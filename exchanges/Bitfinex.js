/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var async = require('async');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
var movement_1 = require("../models/movement");
// My packages
var Bitfinex = require('bitfinex'), logger = require('config-logger'), VError = require('verror'), config = require('config');
var defaultExchangeSettings = {
    name: 'Bitfinex',
    bitfinexAccountType: 'exchange',
    fixedCurrencies: ['BTC', 'LTC', 'DRK'],
    variableCurrencies: ['USD', 'BTC'],
    commissions: { maker: 0.001, taker: 0.002 },
    currencyRounding: {
        BTC: 8,
        LTC: 8,
        DRK: 8,
        USD: 8 },
    priceRounding: {
        BTCUSD: 2,
        LTCUSD: 3,
        DRKUSD: 3,
        LTCBTC: 6
    },
    defaultPriceRounding: 2,
    defaultMinAmount: 0.01,
    publicPollingInterval: 5100,
    privatePollingInterval: 1100,
    maxFailures: 10
};
var BitfinexExchange = (function (_super) {
    __extends(BitfinexExchange, _super);
    function BitfinexExchange(exchangeConfig) {
        var functionName = 'Bitfinex.constructor()';
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        // default to the exchange account instead of the trading account which doesn't have a prefix to the order type
        // see New order in the API doco https://www.bitfinex.com/pages/api
        this.bitfinexAccountType = exchangeSettings.bitfinexAccountType || 'exchange';
        if (this.bitfinexAccountType === 'exchange') {
            this.bitfinexOrderTypePrefix = 'exchange ';
        }
        else if (this.bitfinexAccountType === 'trading') {
            // trader account does not have a prefix to the order type
            this.bitfinexOrderTypePrefix = '';
        }
        this.maxTrades = 10000;
        this.maxMovements = 500;
        logger.trace('%s initialising Bitfinex client', functionName);
        this.client = new Bitfinex(exchangeConfig.APIkey, exchangeConfig.APIsecret);
    }
    BitfinexExchange.prototype.getOrder = function (exchangeId, callback) {
        var functionName = 'Bitfinex.getOrder()', self = this;
        if (!exchangeId || typeof exchangeId != 'string') {
            var error = new VError('%s exchangeId %s is undefined or is not a string', functionName, exchangeId);
            logger.error(error.message);
            return callback(error);
        }
        logger.trace('%s about to get order with id %s', functionName, exchangeId);
        this.client.order_status(exchangeId, function getOrderStatus(err, order) {
            if (err) {
                if (err.name === 'No such order found.') {
                    var error = VError(err, '%s could not find exchangeId %s on the %s exchange', functionName, exchangeId, self.name);
                    error.name = exchange_1.default.ID_NOT_FOUND;
                    logger.error(error.stack);
                    return callback(error);
                }
                else {
                    return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
                }
            }
            self.failures = 0;
            logger.trace('order returned: %s', JSON.stringify(order));
            var state = 'pending';
            var numberPartialFills = 0;
            if (order.is_cancelled) {
                state = 'cancelled';
            }
            else if (!order.is_live) {
                numberPartialFills = 1;
                state = 'filled';
            }
            var price;
            var type = 'limit';
            if (order.type == 'exchange market' || order.type == 'market') {
                price = order.avg_execution_price;
                type = 'market';
            }
            else {
                price = order.price;
            }
            var exchangeOrder = new order_1.default({
                exchangeId: exchangeId,
                exchangeName: self.name,
                symbol: order.symbol.toUpperCase(),
                state: state,
                side: order.side,
                type: type,
                amount: order.original_amount,
                amountTraded: order.executed_amount,
                amountRemaining: order.remaining_amount,
                numberPartialFills: numberPartialFills,
                price: price,
                timestamp: new Date(order.timestamp * 1000)
            });
            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');
            callback(null, exchangeOrder);
        });
    };
    BitfinexExchange.prototype.getTicker = function (symbol, callback) {
        var functionName = 'Bitfinex.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        var pair = symbol.toLowerCase();
        this.client.ticker(pair, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(data));
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(data.bid),
                ask: Number(data.ask),
                last: Number(data.last_price),
                timestamp: new Date(data.timestamp * 1000)
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        });
    };
    BitfinexExchange.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'Bitfinex.getOrderBook()', self = this;
        var pair = symbol.toLowerCase();
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s pair: %s fixed: %s var: %s', functionName, pair, fixedCurrency, variableCurrency);
        this.client.orderbook(pair, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(data));
            logger.debug('%s returned %d bids, %d asks', functionName, data.bids.length, data.asks.length);
            //Convert Bitfinex bid and ask formats to a 2 dimentional array
            var bids = [], asks = [];
            data.bids.forEach(function (order, index) {
                bids[index] = [Number(order.price), Number(order.amount)]; //cast o Number as coming from JSON string
            });
            data.asks.forEach(function (order, index) {
                asks[index] = [Number(order.price), Number(order.amount)]; //cast o Number as coming from JSON string
            });
            // create a new OrderBook object
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            callback(null, newOrderBook);
        });
    };
    BitfinexExchange.prototype.getAccountBalances = function (callback) {
        var functionName = 'Bitfinex.getAccountBalances()', self = this;
        logger.trace('%s getting account balances', functionName);
        this.client.wallet_balances(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s wallets balance data: %s', functionName, JSON.stringify(data));
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            data.forEach(function (balance) {
                if (balance.type === self.bitfinexAccountType) {
                    logger.debug('%s currency %s, amount %s, available %s in %s account type', functionName, balance.currency, balance.amount, balance.available, balance.type);
                    var upperCurrency = balance.currency.toUpperCase();
                    returnedAccount.setBalance(new balance_1.default({
                        exchangeName: self.name,
                        currency: upperCurrency,
                        // need to round as the API returns USD to 10 decimal places - not 8
                        totalBalance: new BigNumber(balance.amount).
                            toFixed(self.currencyRounding[upperCurrency]),
                        availableBalance: new BigNumber(balance.available).
                            toFixed(self.currencyRounding[upperCurrency])
                    }));
                }
                else if (Number(balance.amount) !== 0) {
                    logger.warn('%s not configured to used the %s wallets with currency %s, amount %s, available %s', functionName, balance.type, balance.currency, balance.amount, balance.available);
                }
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    BitfinexExchange.prototype.getPendingOrders = function (callback) {
        var functionName = 'Bitfinex.getPendingOrders()', self = this;
        logger.trace('%s getting pending orders from the exchange', functionName);
        var orders = [];
        this.client.active_orders(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getPendingOrders', self.getPendingOrders, callback);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(data));
            logger.trace('%s %s pending orders returned from the exchange:', functionName, data.length);
            data.forEach(function (order) {
                var fixedCurrency = order.symbol.slice(0, 3).toUpperCase(), variableCurrency = order.symbol.slice(3).toUpperCase(), symbol = fixedCurrency + variableCurrency;
                logger.trace('%s fixedCurrency = %s, variableCurrency = %s', functionName, fixedCurrency, variableCurrency);
                logger.trace('%s %s %s %d @ %d %s id:%s', functionName, order.side, order.symbol, order.original_amount, order.price, moment(order.timestamp * 1000).format('DMMMYY H:mm:ss'), order.id);
                var exchangeOrder = new order_1.default({
                    exchangeId: order.id,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: order.side,
                    amount: order.original_amount,
                    amountTraded: order.executed_amount,
                    amountRemaining: order.remaining_amount,
                    price: BigNumber(order.price).toString(),
                    timestamp: new Date(order.timestamp * 1000)
                });
                logger.debug('%s pending order:', functionName);
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        });
    };
    ;
    BitfinexExchange.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'Bitfinex.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the type parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = VError('%s order failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var symbol = newOrder.symbol.toLowerCase(), bitfinexOrderType = self.bitfinexOrderTypePrefix + newOrder.type;
        var price;
        if (newOrder.type === 'market') {
            price = '1';
        }
        else {
            price = newOrder.price;
        }
        logger.debug('%s symbol %s, bitfinex order type %s, price %s', functionName, symbol, bitfinexOrderType, price);
        // bitfinex.new_order: (symbol, amount, price, exchange, side, type, cb) ->
        // Bitfinex side = buy or sell which is my trade type
        // Bitfinex type = "market" / "limit" / "stop" / "trailing-stop" / "exchange market" / "exchange limit" / "exchange stop" / "exchange trailing-stop
        this.client.new_order(symbol, newOrder.amountRemaining, price, 'bitfinex', newOrder.side, bitfinexOrderType, function (err, data) {
            if (err) {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.name === 'Invalid order: not enough balance') {
                    var error = new VError('%s %s %s is not enough funds on the %s exchange to add %s order with remaining amount %s, sell amount remaining %s %s and tag %s', functionName, self.account.balances[newOrder.sell.currency].availableBalance, newOrder.sell.currency, self.name, newOrder.side, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Invalid order: minimum amount for exchange order is 0.001') {
                    var error = new VError('%s trade amount remaining %s was too small for the %s exchange and %s order with tag %s. Configured min amount for the %s market is %s', functionName, newOrder.amountRemaining, self.name, newOrder.side, newOrder.tag, newOrder.symbol, self.minAmount[newOrder.symbol]);
                    error.name = exchange_1.default.AMOUNT_TOO_SMALL;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }
            logger.trace('%s data: %s', functionName, JSON.stringify(data));
            var exchangeData = {
                exchangeId: data.id.toString(),
                timestamp: new Date(data.timestamp * 1000)
            };
            self.addOrderSuccess(newOrder, callback, exchangeData);
        });
    };
    ;
    BitfinexExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'Bitfinex.cancelOrder()', self = this;
        if (cancelOrder.exchangeName !== this.name) {
            var error = VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }
        logger.trace('%s about to cancel order id %s', functionName, cancelOrder.exchangeId);
        this.client.cancel_order(cancelOrder.exchangeId, function (err, data) {
            if (err) {
                var orderDesc = util_1.format('%s order with id %s, tag %s, price %s and amount remaining %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === "Order could not be cancelled.") {
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            logger.trace('%s data: %s', functionName, JSON.stringify(data));
            self.cancelOrderSuccess(cancelOrder, callback);
        });
    };
    BitfinexExchange.prototype.cancelAllOrders = function (callback) {
        var functionName = 'Bitfinex.cancelAllOrders()', self = this;
        logger.trace('%s about to cancel all orders on the exchange', functionName);
        this.client.cancel_all_orders(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'cancelAllOrders', self.cancelAllOrders, callback);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(data));
            logger.debug('%s all pending orders were cancelled', functionName);
            var exchangeOrders = [];
            // for each market
            _.values(self.markets).forEach(function (market) {
                // get the pending orders from market
                var pendingMarketOrders = _.where(market.orders, { state: 'pending' });
                logger.debug('%s got %s pending orders for the %s market', functionName, pendingMarketOrders.length, market.symbol);
                // concat market orders to all exchange orders
                exchangeOrders = exchangeOrders.concat(pendingMarketOrders);
            });
            var returnedOrders = [];
            exchangeOrders.forEach(function (exchangeOrder) {
                var returnOrder = exchangeOrder.clone();
                returnOrder.cancel();
                self.account.cancelOrder(exchangeOrder);
                // update market orders, emit a new event for listeners of new order events
                self.markets[exchangeOrder.symbol].cancelOrder(returnOrder);
                returnedOrders.push(returnOrder);
            });
            callback(null, returnedOrders.length);
        });
    };
    // replace an old order with a new order
    // TODO commenting out for now as it needs to handle timeouts and failed cancels
    /*
    replaceOrder(oldOrder: Order, newOrder: Order, callback: (error?: Error, exchangeOrder?: Order) => void): void
    {
        const functionName = 'Bitfinex.replaceOrder()',
            self = this;

        logger.trace('%s about to replace order with id %s with the following new order', functionName,
            oldOrder.exchangeId);
        newOrder.log('trace');

        if (oldOrder.exchangeName !== this.name || newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s old order exchange %s or new order exchange %s is not exchange %s',
                functionName, oldOrder.exchangeName, newOrder.exchangeName, this.name);

            logger.error(error.message);
            throw error;
        }

        const symbol = newOrder.symbol.toLowerCase(),
            bitfinexOrderType = self.bitfinexOrderTypePrefix + newOrder.type;

        logger.trace('%s symbol %s, bitfinexOrderType %s', functionName,
            symbol, bitfinexOrderType);

        this.client.replace_order(oldOrder.exchangeId, symbol, newOrder.amountRemaining, newOrder.price, 'bitfinex', newOrder.side, bitfinexOrderType,
            function(err, data)
            {
                if (err)
                {
                    if (err.name === "404" || err.name === 'Order could not be cancelled.')
                    {
                        const error = new VError(err, '%s could not replace order with id %s. Assume the order has already been filled', functionName,
                            oldOrder.exchangeId);
                        error.name = Exchange.ALREADY_FILLED;

                        logger.error(error.stack);
                        return callback(error);
                    }

                    return self.errorHandler(err, true, 'replaceOrder', self.replaceOrder, callback, oldOrder, newOrder);
                }

                self.failures = 0;

                logger.trace('%s data: %s', functionName, JSON.stringify(data) );

                logger.debug('%s order id %s cancelled', functionName, oldOrder.exchangeId);

                const oldExchangeOrder = oldOrder.clone();
                oldExchangeOrder.cancel();

                self.account.cancelOrder(oldExchangeOrder);

                // update market orders and emit a new event for listeners of new order events
                self.markets[newOrder.symbol].cancelOrder(oldExchangeOrder);

                // if there is a latest order book for the market the order was just removed from
                if (self.markets[oldOrder.symbol].latestOrderBook &&
                    _.isFunction(self.markets[oldOrder.symbol].latestOrderBook.removeOrder) )
                {
                    // remove my order I've just cancelled from the latest market order book
                    self.markets[oldOrder.symbol].latestOrderBook.removeOrder(oldOrder);
                }

                const newExchangeOrder = newOrder.clone();
                newExchangeOrder.exchangeId = data.id.toString();
                newExchangeOrder.state = 'pending';
                newExchangeOrder.timestamp = new Date(data.timestamp * 1000);

                self.account.addOrder(newExchangeOrder);

                // update market orders and emit a new event for listeners of new order events
                self.markets[newOrder.symbol].addOrder(newExchangeOrder);

                // if there is a latest order book for the market the new order was just added to
                if (self.markets[newOrder.symbol].latestOrderBook &&
                    _.isFunction(self.markets[newOrder.symbol].latestOrderBook.addOrder) )
                {
                    // add my new order to the latest market order book
                    self.markets[newOrder.symbol].latestOrderBook.addOrder(newExchangeOrder);
                }

                callback(null, newExchangeOrder);
            });
    }
    */
    BitfinexExchange.prototype.getMarketTrades = function (symbol, numberOfTrades, callback) {
        var functionName = 'Bitfinex.getMarketTrades()', self = this;
        // default the number of trades to be retrieved
        var bitfinexParams = {
            limit_trades: numberOfTrades || 10000
        };
        var pair = symbol.toLowerCase();
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s pair: %s fixed: %s var: %s', functionName, pair, fixedCurrency, variableCurrency);
        this.client.trades(pair, bitfinexParams, function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);
            self.failures = 0;
            var trades = [];
            data.forEach(function (trade) {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.timestamp * 1000)
                });
            });
            logger.debug('%s %s historical trades returned', functionName, trades.length);
            callback(null, trades);
        });
    };
    BitfinexExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        var functionName = 'Bitfinex.getAccountTradesBetweenDates()', self = this;
        // TODO remove hard coding of market
        var symbol = 'BTCUSD';
        logger.trace('%s about to get historical account trades between %s (%s) and %s (%s)', functionName, fromDate.toString(), fromDate.getTime() / 1000, toDate.toString(), toDate.getTime() / 1000);
        // TODO need multiple past_trade calls if more than max transactions is returned
        this.client.past_trades(symbol, fromDate.getTime() / 1000, toDate.getTime() / 1000, this.maxTrades, function (err, trades) {
            if (err)
                return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(trades));
            var returnedTrades = [];
            trades.forEach(function (trade) {
                var tradeDate = new Date(trade.timestamp * 1000);
                var historicalAccountTrade = new historicalAccountTrade_1.default({
                    exchangeName: self.name,
                    symbol: symbol,
                    side: trade.type.toLowerCase(),
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: tradeDate,
                    feeAmountBN: new BigNumber(trade.fee_amount).abs(),
                    feeCurrency: trade.fee_currency,
                    orderId: trade.order_id,
                    tradeId: trade.tid
                });
                returnedTrades.push(historicalAccountTrade);
            });
            logger.debug('%s %s historical trades on my account', functionName, returnedTrades.length);
            callback(null, returnedTrades);
        });
    };
    BitfinexExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var functionName = "Bitfinex.getCurrencyMovementsBetweenDates()", self = this, movements = [];
        logger.trace('%s about to get movements between %s (%s) and %s (%s)', functionName, fromDate.toString(), fromDate.getTime(), toDate.toString(), fromDate.getTime());
        var currencies;
        if (currency) {
            currencies = [currency];
        }
        else {
            currencies = _.union(this.fixedCurrencies, this.variableCurrencies);
        }
        if (!toDate) {
            toDate = new Date();
        }
        var returnedMovements = [];
        async.eachSeries(currencies, function (currency, callback) {
            self.getCurrencyMovementsBetweenDatesForCurrency(function (err, movements) {
                if (err)
                    return callback(err);
                returnedMovements = returnedMovements.concat(movements);
                callback(null);
            }, fromDate, toDate, currency, type);
        }, function (err) {
            callback(err, returnedMovements);
        });
    };
    BitfinexExchange.prototype.getCurrencyMovementsBetweenDatesForCurrency = function (callback, fromDate, toDate, currency, type) {
        var functionName = "Bitfinex.getCurrencyMovementsBetweenDatesForCurrency()", self = this, movements = [];
        logger.trace('%s about to get movements between %s (%s) and %s (%s) for currency %s', functionName, fromDate.toString(), fromDate.getTime(), toDate.toString(), fromDate.getTime(), currency);
        this.client.movements(currency, fromDate.getTime() / 1000, toDate.getTime() / 1000, this.maxMovements, function (err, movements) {
            if (err)
                return self.errorHandler(err, false, 'getCurrencyMovementsBetweenDatesForCurrency', self.getCurrencyMovementsBetweenDatesForCurrency, callback, fromDate, toDate, currency, type);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(movements));
            var returnedMovements = [];
            movements.forEach(function (movement) {
                var newMovement = new movement_1.default({
                    exchangeName: self.name,
                    currency: movement.currency,
                    type: movement.side.toLowerCase(),
                    amount: movement.amount,
                    timestamp: new Date(movement.timestamp * 1000)
                });
                // add movement if type not defined or type of returned movement matches what's specified
                if (!type || type == newMovement.type) {
                    returnedMovements.push(newMovement);
                }
            });
            logger.debug('%s %s movements returned for currency %s', functionName, returnedMovements.length, currency);
            callback(null, returnedMovements);
        });
    };
    BitfinexExchange.prototype.setTimeout = function (timeout) {
        this.client.timeout = timeout;
    };
    return BitfinexExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BitfinexExchange;
;
