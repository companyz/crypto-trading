/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import {default as HistoricalAccountTrade, IHistoricalAccountTrade} from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

var	logger = require('config-logger'),
    VError = require('verror'),
	BTCMarkets = require("btc-markets"),
    config = require('config');

const numberConverter = 100000000;    // 100 million
const numberConverterBN = new BigNumber(numberConverter);    // 100 million

const defaultExchangeSettings: IExchangeSettings = {
	name: 'BTCMarkets',
	fixedCurrencies: ['BTC', 'LTC'],
	variableCurrencies: ['AUD'],
	commissions: 0.0011,				// default commission rather than defining for each currency
	currencyRounding:{
	    BTC: 8,
	    LTC: 8,
	    AUD: 2},
    priceRounding:{
        BTCAUD: 2,
        LTCAUD: 2,
        LTCBTC: 8
    },
    defaultMinAmount: 0.01,
	publicPollingInterval: 15000,	// milliseconds
	privatePollingInterval: 5000,    //polling interval of private methods
	maxFailures: 10,
    feeStructure: 'variableCurrency'    // all fees are in AUD currency. eg buy BTC will have a AUD fee
};

declare type BTCMarketOrder = {
    id: number,
    currency: string,
    instrument: string,
    orderSide: string,
    ordertype: string,
    creationTime: number,
    status: string,
    errorMessage: string,
    price: number,
    volume: number,
    openVolume: number,
    clientRequestId: string,
    trades: {
        id: number,
        creationTime: number,
        description: string,
        price: number,
        volume: number,
        fee: number
    }[]
}

export default class BTCMarketsExchange extends Exchange
{
    client;
    server: string;
    limit: number;

    constructor (exchangeConfig: IExchangeSettings)
    {
        const functionName = 'BTCMarketsExchange.constructor()';

        this.limit = 50;

        // override the default exchange settings from the config
        const exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);

        //Call the constructor of inherited Exchange object
        super(exchangeSettings);

        if (exchangeConfig.testInstance)
        {
            this.server = 'http://dev.api.btcmarkets.net';

            logger.info('%s using test server %s', functionName, this.server);
        }

        logger.trace('%s initialising BTC Markets client', functionName);

        this.client = new BTCMarkets(
            exchangeSettings.APIkey,
            exchangeSettings.APIsecret,
            this.server  // undefined defaults to production: https://api.btcmarkets.net
        );
    }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    getTicker(symbol: string, callback: (error?: Error, ticker?: Ticker) => void ): void
    {
        const functionName = 'BTCMarkets.getTicker()',
            self = this;

        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);

        const instrument = Market.getFixedCurrency(symbol),
            currency = Market.getVariableCurrency(symbol);

        logger.trace('%s instrument %s, currency %s', functionName,
            instrument, currency);

        this.client.getTick(instrument, currency, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);

            self.failures = 0;

            const newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.bestBid),
                ask: Number(json.bestAsk),
                last: Number(json.lastPrice),
                timestamp: new Date(json.timestamp * 1000)
            });

            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            callback(null, newTicker);
        });
    }

    getOrderBook(symbol: string, callback: (error: Error, orderBook: OrderBook) => void): void
    {
        const functionName = 'BTCMarkets.getOrderBook()',
            self = this;

        logger.trace('%s about to get order book for market %s', functionName, symbol);

        const instrument = Market.getFixedCurrency(symbol),
            currency = Market.getVariableCurrency(symbol);

        logger.trace('%s instrument %s, currency %s', functionName,
            instrument, currency);

        this.client.getOrderBook(instrument, currency, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(json) );

            let bids = BTCMarketsExchange.consolidateOrders(json.bids);
            let asks = BTCMarketsExchange.consolidateOrders(json.asks);

            const newOrderBook = new OrderBook({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(json.timestamp * 1000),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });

            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0)
            {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName,
                    self.name, symbol,
                    newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length,
                    newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length,
                    moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss') );
            }
            else
            {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks',
                    functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }

            self.markets[symbol].addOrderBook(newOrderBook);

            callback(null, newOrderBook);
        });
    }

    // consolidated orders with the same prices. That is, combines them and adds the amounts together
    static consolidateOrders(exchangeOrders: number[][]): number[][]
    {
        let previousOrder = [0,0],
            returnOrders: number[][] = [];

        exchangeOrders.forEach(function(order, index)
        {
            // if current order price = previous order price
            if (previousOrder[0] === order[0])
            {
                // add order amounts together and cast to Number format
                previousOrder[1] = new BigNumber(previousOrder[1]).
                plus(order[1]).
                toNumber();
            }
            // if current order price != previous order price
            else
            {
                // if not processing the first order in the order book
                if (index > 0)
                {
                    // add previous order to list of bids
                    returnOrders.push(previousOrder);
                }

                previousOrder = order;
            }
        });

        if (exchangeOrders.length > 0)
        {
            // add the last order from the previous loop
            returnOrders.push(previousOrder);
        }

        return returnOrders;
    }

    getMarketTrades(symbol: string, params, callback: HistoricalMarketTradesCallback): void
    {
        const functionName = 'BTCMarkets.getMarketTrades()',
            self = this;

        logger.trace('%s about to get historical trades for market %s', functionName, symbol);

        const instrument = Market.getFixedCurrency(symbol),
            currency = Market.getVariableCurrency(symbol);

        logger.trace('%s instrument %s, currency %s', functionName,
            instrument, currency);

        this.client.getTrades(instrument, currency, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);

            self.failures = 0;

            let trades: IHistoricalMarketTrade[] = [];

            json.forEach(function(trade)
            {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date * 1000)
                });
            });

            logger.debug('%s %s historical trades returned', functionName, trades.length);

            callback(null, trades);
        });
    }

    getAccountBalances(callback: (error: Error, account: Account) => void): void
    {
        const functionName = 'BTCMarkets.getAccountBalances()',
            self = this;

        logger.trace('%s about to get account balances', functionName);

        this.client.getAccountBalances(function(err, balancesArray)
        {
            if (err) return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);

            self.failures = 0;

            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(balancesArray);

            const returnedAccount = new Account([], self.name, self.currencyRounding);

            balancesArray.forEach(function(account: {
                balance: number,
                currency: string,
                pendingFunds: number
            })
            {
                const total = new BigNumber(account.balance).
                    div(numberConverter).
                    round(self.currencyRounding[account.currency], BigNumber.ROUND_DOWN).
                    toString();

                const available = new BigNumber(total).
                    minus(new BigNumber(account.pendingFunds).
                        div(numberConverter)
                    ).
                    round(self.currencyRounding[account.currency], BigNumber.ROUND_DOWN).
                    toString();

                logger.debug('%s %s balances: total %s, available %s = balance %s - pending balance %s', functionName,
                    account.currency, total, available,
                    account.balance, account.pendingFunds);

                returnedAccount.setBalance(
                    new Balance({
                        exchangeName: self.name,
                        currency: account.currency,
                        totalBalance: total,
                        availableBalance: available
                    })
                );
            });

            // set the balances on the exchange
            self.account = returnedAccount;

            callback(null, returnedAccount);
        });
    }

    // gets my pending orders for a market with symbol
    getPendingOrdersForSymbol(symbol: string, callback: OrdersCallback): void
    {
        const functionName = 'BTCMarkets.getPendingOrdersForSymbol()',
            self = this;

        logger.trace('%s about to get pending orders', functionName);

        let orders = [];

        const sinceOrderId = 1;

        const fixedCurrency = Market.getFixedCurrency(symbol);
        const variableCurrency = Market.getVariableCurrency(symbol);

        self.client.getOpenOrders(fixedCurrency, variableCurrency, 20, sinceOrderId, function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);

            self.failures = 0;

            logger.debug('%s %s pending orders returned', self.name, json.orders.length);

            json.orders.forEach(function(order: BTCMarketOrder)
            {
                //convert from BTCMarkets side
                const side = BTCMarketsExchange.convertSide(order.orderSide);
                const symbol = order.instrument + order.currency;

                const amountTraded: string = new BigNumber(order.volume).
                    minus(order.openVolume).
                    div(numberConverter).
                    round(self.currencyRounding[order.instrument as string], BigNumber.ROUND_DOWN).
                    toString();

                let amountLastPartial = '0';
                if (order.trades && order.trades.length > 0 )
                {
                    const lastTrade = _.last(order.trades);
                    amountLastPartial = new BigNumber(lastTrade.volume).div(numberConverter).toString();

                    logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName,
                        order.id, order.trades.length,
                        lastTrade.price  / numberConverter,
                        amountLastPartial,
                        moment(lastTrade.creationTime).format('D MMM YY H:mm:ss'));
                }

                const exchangeOrder = new Order({
                    exchangeId: order.id.toString(),
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    type: order.ordertype.toLowerCase(),
                    amount: new BigNumber(order.volume).div(numberConverter).toString(),
                    amountTraded: amountTraded,
                    amountRemaining: new BigNumber(order.openVolume).div(numberConverter).toString(),
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: order.trades.length,
                    price: new BigNumber(order.price).div(numberConverter).toString(),
                    timestamp: new Date(order.creationTime * 1000),
                    tag: order.clientRequestId
                });

                logger.debug('pending order:');
                exchangeOrder.log('debug');

                orders.push(exchangeOrder);
            });

            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);

            callback(null, orders);
        });
    }

    // creates a buy or sell order on an exchange
    addOrder(newOrder, callback: OrderCallback): void
    {
        const functionName = 'BTCMarkets.addOrder()',
            self = this;

        logger.trace('%s about to add the following new order:', functionName );
        newOrder.log('trace');

        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy')
        {
            const error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }

        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                newOrder.name, this.name);

            logger.error(error.stack);
            return callback(error);
        }

        const orderSide = BTCMarketsExchange.convertSide(newOrder.side),
            orderType = newOrder.type.charAt(0).toUpperCase() + newOrder.type.slice(1);

        let convertedPrice = 0;   // default to 0 for market trades
        if (newOrder.type === 'limit')
        {
            convertedPrice = new BigNumber(newOrder.price).
                times(numberConverter).
                round(0).
                toNumber();

            logger.debug('%s converted price %s = price %s * number converter %s', functionName,
                convertedPrice, newOrder.price, numberConverter);
        }

        const convertedAmount = new BigNumber(newOrder.amountRemaining).
            times(numberConverter).
            round(0).
            toNumber();

        logger.debug('%s converted amount %s = remaining amount %s * number converter %s', functionName,
            convertedAmount, newOrder.amountRemaining, numberConverter);

        this.client.createOrder(
            newOrder.fixedCurrency,
            newOrder.variableCurrency,
            convertedPrice,
            convertedAmount,
            orderSide,
            orderType,
            newOrder.tag,
            function(err, json)
            {
                const functionName = 'BTCMarkets.addOrder.createOrder()#' + newOrder.type;

                if (err)
                {
                    // no need to retry the API call with the following errors.
                    if (err.name === 'Insufficient funds.')
                    {
                        const error = new VError(err, '%s not enough funds on the %s exchange to add %s order @ %s with remaining amount %s, sell amount remaining %s %s, fee %s %s and tag %s', functionName,
                            self.name, newOrder.side, newOrder.price,
                            newOrder.amountRemaining,
                            newOrder.sell.amountRemaining, newOrder.sell.currency,
                            newOrder.fee.amountRemaining, newOrder.fee.currency,
                            newOrder.tag);

                        error.name = Exchange.NOT_ENOUGH_FUNDS;

                        logger.error(error.stack);

                        return callback(error);
                    }
                    else if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                    {
                        return self.addOrderTimeout(err, newOrder, callback);
                    }

                    return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
                }

                logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

                const data = {
                    exchangeId: json.id.toString(),
                    timestamp: new Date()
                };

                self.addOrderSuccess(newOrder, callback, data);
            });
    }

    // gets the order details
    getOrder(exchangeId: string, callback: (error?: Error, trade?: Order) => void): void
    {
        const functionName = 'BTCMarketsExchange.getOrder()',
            self = this;

        if (!exchangeId || typeof exchangeId != 'string' )
        {
            const error = new VError('%s exchangeId %s is undefined or is not a string', functionName,
                exchangeId);

            logger.error(error.message);
            return callback(error);
        }

        logger.trace('%s about to get order with id %s', functionName, exchangeId);

        self.client.getOrderDetail([parseInt(exchangeId)], function getOrderDetail(err, json)
        {
            if (err)
            {
                return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
            }
            else if (json.orders.length == 0)
            {
                const error = new VError('%s could not find order with id %s on the %s exchange', functionName,
                    exchangeId, self.name);
                error.name = Exchange.ID_NOT_FOUND;

                logger.error(error.stack);
                return callback(error);
            }
            else if (json.orders[0].ordertype == 'Market' && json.orders[0].orders == 0)
            {
                const error = new VError('%s market order with id %s has not yet been filled.', functionName,
                    exchangeId);

                return self.errorHandler(error, true, 'getOrder', self.getOrder, callback, exchangeId);
            }

            self.failures = 0;
            logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

            const order: BTCMarketOrder = json.orders[0];

            const symbol = order.instrument + order.currency;

            const price = self.calcPrice(order, symbol);

            //convert from BTCMarkets side
            const side = BTCMarketsExchange.convertSide(order.orderSide);

            const amountTraded = new BigNumber(order.volume).
                minus(order.openVolume).
                div(numberConverter).
                round(self.currencyRounding[order.instrument as string], BigNumber.ROUND_DOWN).
                toString();

            let amountLastPartial = '0';
            if (order.trades && order.trades.length > 0 )
            {
                const lastTrade = _.last(order.trades);
                amountLastPartial = new BigNumber(lastTrade.volume).div(numberConverter).toString();

                logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName,
                    order.id, order.trades.length,
                    lastTrade.price  / numberConverter,
                    amountLastPartial,
                    moment(lastTrade.creationTime).format('D MMM YY H:mm:ss'));
            }

            const state = BTCMarketsExchange.convertStatus(order.status);

            const exchangeOrder = new Order({
                exchangeId: order.id.toString(),
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                type: order.ordertype.toLowerCase(),
                amount: new BigNumber(order.volume).div(numberConverter).toString(),
                amountTraded: amountTraded,
                amountRemaining: new BigNumber(order.openVolume).div(numberConverter).toString(),
                amountLastPartial: amountLastPartial,
                numberPartialFills: order.trades.length,
                price: price,
                timestamp: new Date(order.creationTime * 1000),
                tag: order.clientRequestId
            });

            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');

            callback(null, exchangeOrder);
        });
    }

    private calcPrice(order: BTCMarketOrder, symbol: string): string
    {
        const functionName = 'BTCMarkets.calcPrice()',
            self = this;

        if (order.ordertype == 'Limit')
        {
            return new BigNumber(order.price).
                div(numberConverter).
                toString();
        }
        else // market order
        {
            if (order.trades.length == 0) {return;}

            let sumTradePriceTimesVolumesBN = new BigNumber(0),
                sumTradeVolumesBN = new BigNumber(0);

            order.trades.forEach(function(trade)
            {
                const tradeVolumeBN = new BigNumber(trade.volume).
                div(numberConverter);

                sumTradePriceTimesVolumesBN = sumTradePriceTimesVolumesBN.
                plus(new BigNumber(trade.price).
                div(numberConverter).
                times(tradeVolumeBN));

                sumTradeVolumesBN = sumTradeVolumesBN.
                plus(tradeVolumeBN);
            });

            const vwap = sumTradePriceTimesVolumesBN.
            div(sumTradeVolumesBN).
            round(self.priceRounding[symbol], BigNumber.ROUND_DOWN).
            toString();

            logger.debug('%s volume weighted average price %s = sum trade prices %s / sum trade volumes %s', functionName,
                vwap,
                sumTradePriceTimesVolumesBN.toString(),
                sumTradeVolumesBN.toString() );

            return vwap;
        }
    }

    // cancels an pending order
    cancelOrder(cancelOrder: Order, callback: OrderCallback): void
    {
        const functionName = 'BTCMarkets.cancelOrder()',
            self = this;

        logger.trace('%s about to cancel order with exchange id %s', functionName,
            cancelOrder.exchangeId);

        if (cancelOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }

        this.client.cancelOrders([parseInt(cancelOrder.exchangeId)], function(err, json)
        {
            if (err)
            {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }

                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            // if order was not successfully cancelled
            else if (json && json.responses && json.responses[0] &&
                !json.responses[0].success)
            {
                const orderDesc = format('%s order with id %s, tag %s, price %s and amount remaining %s',
                    cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);

                if (json.responses[0].errorMessage === 'order is already cancelled.')
                {
                    const error = new VError('%s could not cancel %s. BTC Markets error message: %s', functionName,
                        orderDesc, json.responses[0].errorMessage);

                    // check the state of the order that could not be cancelled and then call the callback with the appropriate error. eg ALREADY_FILLED or ALREADY_CANCELLED
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }

                const error = new VError('%s could not cancel %s order with id %s and tag %s, price %s and amount remaining %s. Error message: %s', functionName,
                    cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining,
                    json.responses[0].errorMessage);

                error.name = json.responses[0].errorMessage;

                logger.error(error.stack);
                return callback(error);
            }

            logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

            self.cancelOrderSuccess(cancelOrder, callback);
        });
    }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate: Date = new Date() ): void
    {
        const functionName = 'BTCMarkets.getAccountTradesBetweenDates()',
            self = this;

        const since = moment(fromDate).unix();

        // TODO get both BTCAUD and LTCAUD rather than just BTCUSD
        const symbol = 'BTCAUD';
        const instrument = Market.getFixedCurrency(symbol);
        const currency = Market.getVariableCurrency(symbol);

        logger.trace('%s about to get %s account trades from %s (%s) to %s', functionName,
            symbol,
            moment(fromDate).format('DD MMM YYYY HH:mm:ss'), since,
            moment(toDate).format('DD MMM YYYY HH:mm:ss') );

        // getOrderHistory(instrument, currency, limit, since, callback)
        self.client.getOrderHistory(currency, instrument, this.limit, 1, function(err, json)
        {
            if (err) return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);

            self.failures = 0;

            let trades: HistoricalAccountTrade[] = [];

            json.orders.forEach(function(order)
            {
                if (!order.orders) { return; }

                // is the returned trade type a key in the typeMap
                order.orders.forEach(function(trade)
                {
                    const historicalAccountTrade = new HistoricalAccountTrade({
                        orderId: order.id.toString(),
                        tradeId: trade.id.toString(),
                        priceBN: new BigNumber(trade.price).div(numberConverterBN),
                        quantityBN: new BigNumber(trade.volume).div(numberConverterBN),
                        side: BTCMarketsExchange.convertSide(order.orderSide),
                        exchangeName: self.name,
                        symbol: symbol,
                        timestamp: new Date(trade.creationTime * 1000)
                    });

                    trades.push(historicalAccountTrade);
                });
            });

            logger.debug('%s %s my trades returned', functionName, trades.length);

            callback(null, trades);
        });
    }

    setTimeout(timeout: number)
    {
        this.client.timeout = timeout;
    }

    // converts exchange order status to valid Exchange.state values
    private static convertStatus(status: string): string
    {
        if (status === "Placed" || status === "Partially Matched") return 'pending';
        else if (status === "Fully Matched") return 'filled';
        else if (status === "Cancelled" || status === "Partially Cancelled") return 'cancelled';
        else return 'unknown';
    }

    private static convertSide(side: string): string
    {
        const functionName = 'BTCMarkets.convertSide()';
        let convertedSide: string;

        if (side === 'Bid')
        {
            convertedSide = 'buy';
        }
        else if (side === 'Ask')
        {
            convertedSide = 'sell';
        }
        else if (side === 'buy')
        {
            convertedSide = 'Bid';
        }
        else if (side === 'sell')
        {
            convertedSide = 'Ask';
        }
        else
        {
            const error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
            logger.error(error.stack);
            throw error;
        }

        logger.trace('%s side %s convert to %s', functionName, side, convertedSide);

        return convertedSide;
    }
}
