/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as fs from 'fs';
import * as readline from 'readline';
import * as stream from 'stream';

import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import {default as OrderBook, IOrderBookOrder} from "../models/orderBook";
import Order from "../models/order";
import {default as HistoricalAccountTrade, IHistoricalAccountTrade} from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

var	logger = require('config-logger'),
    VError = require('verror'),
    config = require('config');

export interface ISimulatorExchangeSettings extends IExchangeSettings
{
    exchangeModule: string;
    simulatedExchangeName: string;

    simulatorFiles?: {
        name: string,
        lines: number
    };

    historicalAccountTradesFilename?: string;

    tickerMargin: number;

    numberOfBidAsks: number;
    orderBookStartingAmount: number;
    orderBookStepAmount: number;
    orderBookBidAskGap: number;

    failReplaceTradeRem: number;
    failReplaceTradeDiv: number;
    failCancelTradeRem: number;
    failCancelTradeDiv: number;
    balances: {
        currency: string,
        totalBalance: string,
        availableBalance: string
    }[]
}

export default class Simulator extends Exchange
{
    simulator: boolean;

    // config for simulating trades
    filename: string;
    countInSimulatorFile: number;

    // config for historical account trades
    historicalAccountTradesFilename: string;

    tickerMargin: number;

    // order book generation settings
    numberOfBidAsks: number;
    orderBookStartingAmount: number;
    orderBookStepAmount: number;
    orderBookBidAskGap: number;     // price * (1 - i / orderBookBidAskGap)

    // is the simulated time to look for crossed trades in getFilledTrades()
    // TODO why can't this be set from settings.privatePollingInterval?
    simulatedPublicPollingInterval: number;     // milliseconds

    failReplaceTradeRem: number;
    failReplaceTradeDiv: number;
    failCancelTradeRem: number;
    failCancelTradeDiv: number;

    failedOrder: Order;

    orders: Order[];
    lastOrder: Order;

    historicalTrades: {
        price: string,
        amount: string,
        timestamp: Date
    }[];
    historicalTradesProcessed: number;

    historicalAccountTrades: HistoricalAccountTrade[];

    failures: number;

    requestNumber: number;
    cancelCount: number;

    smallestRateBN: {[symbol: string]: BigNumber};

    constructor(settings: ISimulatorExchangeSettings)
    {
        const functionName = format('%s.constructor()', settings.name),
            self = this;

        const SimulatedExchange = require('../exchanges/' + settings.simulatedExchangeName).default;
        const simulatedExchange: Exchange = new SimulatedExchange(settings);

        const simExchangeSettings: IExchangeSettings = {
            name:                   settings.name,
            minOrderBookAmountToIgnore: settings.minOrderBookAmountToIgnore,
            fixedCurrencies:        simulatedExchange.fixedCurrencies,
            variableCurrencies:     simulatedExchange.variableCurrencies,
            commissions:            simulatedExchange.commissions,
            currencyRounding:       simulatedExchange.currencyRounding,
            priceRounding:          simulatedExchange.priceRounding,
            minAmount:              simulatedExchange.minAmount,

            publicPollingInterval:  settings.publicPollingInterval || 1000,// milliseconds
            privatePollingInterval: settings.privatePollingInterval || 1000,    //polling interval of private methods

            APIkey: 'simulatedAPIkey',
            APIsecret: 'simulatedAPIsecret'
        };

        //Call the constructor of inherited Exchange object
        super(simExchangeSettings);

        this.simulator = true;

        if (settings.simulatorFiles)
        {
            this.filename = settings.simulatorFiles.name;
            this.countInSimulatorFile = settings.simulatorFiles.lines;
            logger.debug('%s Simulator filename %s has %s lines', functionName,
                this.filename, this.countInSimulatorFile);
        }

        this.historicalAccountTradesFilename = settings.historicalAccountTradesFilename;

        this.tickerMargin = settings.tickerMargin || 3;

        // order book generation settings
        this.numberOfBidAsks = settings.numberOfBidAsks || 20;
        this.orderBookStartingAmount = settings.orderBookStartingAmount || 1;
        this.orderBookStepAmount = settings.orderBookStepAmount || 1;
        this.orderBookBidAskGap = settings.orderBookBidAskGap || settings.privatePollingInterval || 1000;     // price * (1 - i / orderBookBidAskGap)

        // is the simulated time to look for crossed trades in getFilledTrades()
        // TODO why can't this be set from settings.privatePollingInterval?
        this.simulatedPublicPollingInterval = 3000;     // milliseconds

        this.failReplaceTradeRem = settings.failReplaceTradeRem || 11 ;
        this.failReplaceTradeDiv = settings.failReplaceTradeDiv || 10;
        this.failCancelTradeRem = settings.failCancelTradeRem || 11;
        this.failCancelTradeDiv = settings.failCancelTradeDiv || 10;

        this.failedOrder = null;

        this.orders = [];
        this.historicalTrades = [];
        this.historicalTradesProcessed = 1;

        this.historicalAccountTrades = [];

        this.failures = 0;
        this.requestNumber = 0;
        this.cancelCount = 0;

        this.smallestRateBN = {};
        // for each symbol
        _.keys(this.markets).forEach(function(symbol)
        {
            self.smallestRateBN[symbol] = new BigNumber(1).div(new BigNumber(10).toPower(self.priceRounding[symbol]));
        });

        var configBalances = [];

        // loop through each balance in the Simulator config
        settings.balances.forEach(function(balance)
        {
            configBalances.push(new Balance({
                exchangeName: self.name,
                currency: balance.currency,
                totalBalance: balance.totalBalance,
                availableBalance: balance.availableBalance
            }))
        });

        // instantiate Simulator balances from balances in the config read in above
        this.account = new Account(configBalances, this.name, this.currencyRounding);

        logger.debug('%s Opening historical trade file %s', functionName, this.filename);

        this.readSimulatedTrades();

        this.readHistoricalAccountTrades();
    }

    readSimulatedTrades(): void
    {
        const functionName = format('%s.readSimulatedTrades()', this.name),
            self = this;

        if (!this.filename)
        {
            const warning = new VError('%s filename not set for the simulated exchange %s', functionName, this.name);
            logger.warn(warning.stack);
            return;
        }

        logger.debug('%s starting readline listener for file %s', functionName, self.filename);

        const inStream = fs.createReadStream(this.filename);

        const rl = readline.createInterface({
            input: inStream,
            output: process.stdout,
            terminal: false
        });

        rl.on('line', function(line)
        {
            const fields = line.split(',');

            const historicalTrade = {price: fields[1],
                amount: fields[2],
                timestamp: new Date(fields[0] * 1000)};

            self.historicalTrades.push(historicalTrade);
        });
    }

    readHistoricalAccountTrades()
    {
        const functionName = format('%s.readHistoricalAccountTrades()', this.name),
            self = this;

        if (!this.historicalAccountTradesFilename) { return; }

        logger.debug('%s about to read historical account trades in file %s', functionName, this.historicalAccountTradesFilename);

        const inStream = fs.createReadStream(this.historicalAccountTradesFilename);

        const rl = readline.createInterface({
            input: inStream,
            output: process.stdout,
            terminal: false
        });

        rl.on('line', function(line)
        {
            const fields = line.split(',');

            const timestamp: Date = new Date(fields[4] * 1000);

            const historicalAccountTradeSettings: IHistoricalAccountTrade = {
                exchangeName: self.name,
                symbol: fields[0],
                side: fields[1],
                priceBN: new BigNumber(fields[2]),
                quantityBN: new BigNumber(fields[3]),
                timestamp: timestamp,
                orderId: fields[5],
                tradeId: fields[6],
                feeCurrency: fields[8],
                rebateCurrency: fields[10]
            };

            if (fields[7])
            {
                historicalAccountTradeSettings.feeAmountBN = new BigNumber(fields[7]);
            }

            if (fields[9])
            {
                historicalAccountTradeSettings.rebateAmountBN = new BigNumber(fields[9]);
            }

            self.historicalAccountTrades.push(new HistoricalAccountTrade(historicalAccountTradeSettings));
        });
    }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate?: Date ): void
    {
        const functionName = 'Simulator.getAccountTradesBetweenDates()',
            self = this;

        if (this.historicalAccountTrades.length == 0)
        {
            setTimeout(function()
            {
                self.getAccountTradesBetweenDates(callback, fromDate, toDate);
            }, 2000);
            return;
        }

        const returnTrades: HistoricalAccountTrade[] = _.filter(this.historicalAccountTrades, trade =>
            fromDate <= trade.timestamp && trade.timestamp <= toDate);

        callback(null, returnTrades);
    }

    getMarketTrades(symbol: string, numberOfTrades: number, callback: HistoricalMarketTradesCallback ): void
    {
        const error = new VError('Simulator.getMarketTrades() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    // adds a new order on the simulated exchange
    addOrder<T extends Order>(newOrder: T, callback: (error: Error, order?: T) => void): void
    {
        const functionName = format('%s.addOrder()', this.name),
            self = this;

        logger.trace('%s about to add %s %s order with tag %s, price %s and amount remaining %s to the %s exchange', functionName,
            newOrder.type, newOrder.side, newOrder.tag, newOrder.price, newOrder.amountRemaining, newOrder.exchangeName );


        // validate the exchangeName on the newOrder
        if (newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                newOrder.exchangeName, this.name);

            logger.error(error.stack);
            throw error;
        }

        // make an asynchronous call to simulate a real exchange call
        setImmediate(function()
        {
            // if remaining amount < exchange min amount for the market
            if (newOrder.amountRemainingBN.lessThan(self.minAmount[newOrder.symbol]))
            {
                const error = new VError('%s order with tag %s has amount remaining %s < min exchange amount %s for market %s on %s', functionName,
                    newOrder.tag,
                    newOrder.amountRemaining, self.minAmount[newOrder.symbol],
                    newOrder.symbol, newOrder.exchangeName);

                logger.error(error.stack);
                error.name = Exchange.AMOUNT_TOO_SMALL;

                return callback(error);
            }

            // if no historical trades have been read from the file
            if (self.historicalTrades.length === 0)
            {
                const err = new VError('no trades have been read from historical file', functionName);
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }

            // Clone the order to be added to the exchange to break the references
            const exchangeOrder = newOrder.clone();
            exchangeOrder.exchangeId = (self.historicalTradesProcessed++).toString();
            exchangeOrder.timestamp = new Date(self.historicalTrades[0].timestamp.getTime() );

            if (newOrder.type === 'limit')
            {
                exchangeOrder.state = 'pending';

                // if original exchange id is not set then set it
                if (!newOrder.originalExchangeId)
                {
                    exchangeOrder.originalExchangeId = exchangeOrder.exchangeId;
                }

                // update exchange balances
                self.account.addOrder(exchangeOrder);
            }
            else if (newOrder.type === 'market')
            {
                exchangeOrder.amountTraded = newOrder.amount;
                exchangeOrder.amountLastPartial = exchangeOrder.amountRemaining;
                exchangeOrder.numberPartialFills = 1;
                exchangeOrder.amountRemaining = '0';
                exchangeOrder.state = 'filled';

                // set the filled price to the last ticker price
                if (exchangeOrder.side === 'sell')
                {
                    exchangeOrder.price = self.markets[exchangeOrder.symbol].latestTicker.bid.toString();
                }
                else if (exchangeOrder.side === 'buy')
                {
                    exchangeOrder.price = self.markets[exchangeOrder.symbol].latestTicker.ask.toString();
                }

                // update exchange balances
                self.account.addMarketTrade(exchangeOrder);
            }

            // validate all the balances are positive
            self.validateBalances('add', exchangeOrder);

            self.orders.push(exchangeOrder);
            logger.trace('%s added exchange trade with id %s to trade list %s in length',
                functionName, exchangeOrder.exchangeId, self.orders.length);

            const returnedTrade: T = exchangeOrder.clone() as T;

            // update market trades and emit event for any listeners
            if (returnedTrade.pending)
            {
                self.markets[returnedTrade.symbol].addOrder(returnedTrade);
            }
            else if (returnedTrade.filled)
            {
                self.markets[returnedTrade.symbol].fillOrder(returnedTrade);
            }

            self.failures = 0;

            // return the cloned trade with the new exchangeId to the callback
            callback(null, returnedTrade);
        });
    };

    cancelOrder<T extends Order>(cancelOrder: T, callback: (error: Error, order?: T) => void): void
    {
        var functionName = format('%s.addOrder()', this.name),
            self = this;

        logger.trace('%s about to cancel %s order with id %s and tag %s', functionName,
            cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag);

        // validate the exchangeName on the newOrder
        if (cancelOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.exchangeName, this.name);

            logger.error(error.stack);
            throw error;
        }

        // make an asynchronous call to simulate a real exchange call
        setImmediate(function()
        {
            // find filled order in the exchange trades
            let exchangeOrder: Order = _.findWhere(self.orders, {
                exchangeId: cancelOrder.exchangeId
                , state: 'pending'
            });

            // if not pending
            if (!exchangeOrder )
            {
                var error;

                // check if the order has already been filled
                exchangeOrder = _.findWhere(self.orders, {
                    exchangeId: cancelOrder.exchangeId
                    , state: 'filled'
                });

                // if filled
                if (exchangeOrder)
                {
                    error = new VError('%s order with id %s and tag %s has already been fully filled on the %s exchange', functionName,
                        cancelOrder.exchangeId, cancelOrder.tag, self.name);
                    error.name = Exchange.ALREADY_FILLED;
                }
                else    // if not cancelled
                {
                    // check if the order has already been cancelled
                    exchangeOrder = _.findWhere(self.orders, {
                        exchangeId: cancelOrder.exchangeId
                        , state: 'cancelled'
                    });

                    // if filled
                    if (exchangeOrder)
                    {
                        error = new VError('%s order with id %s and tag %s has already been cancelled on the %s exchange', functionName,
                            cancelOrder.exchangeId, cancelOrder.tag, self.name);
                        error.name = Exchange.ALREADY_CANCELLED;
                    }
                    else // if not pending, cancelled or filled
                    {
                        error = new VError('%s could not find order with id %s and tag %s on the %s exchange in pending, filled or cancelled state', functionName,
                            cancelOrder.exchangeId, cancelOrder.tag);
                    }
                }

                logger.error(error.stack);
                return callback(error);
            }

            // check the price and remaining amount of cancel trade matches the found pending exchange trade for the same exchange id
            if (exchangeOrder.price !== cancelOrder.price)
            {
                const error = new VError('%s pending exchange order with id %s, tag %s and price %s does not equal price %s of cancel trade parameter', functionName,
                    exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.price,
                    cancelOrder.price);
                logger.error(error);
                throw error;
            }
            else if (exchangeOrder.amountRemaining !== cancelOrder.amountRemaining)
            {
                const error = new VError('%s pending exchange order with id %s, tag %s and remaining amount %s does not equal remaining amount %s of cancel trade parameter', functionName,
                    exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.amountRemaining,
                    cancelOrder.amountRemaining);
                logger.error(error);
                throw error;
            }

            if (++self.cancelCount % self.failCancelTradeDiv === self.failCancelTradeRem)
            {
                logger.warn('%s testing a failed cancel. count = %s', functionName,
                    self.cancelCount);

                // store for next getFilledOrders
                self.failedOrder = exchangeOrder.clone();

                // partially fill the order by 20%
                self.failedOrder.amountBN = self.failedOrder.amountBN.
                    times(0.2);

                var error = new VError('%s could not cancel trade with id %s and tag %s as it is already filled on the %s exchange', functionName,
                    cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.exchangeName);
                error.name = Exchange.ALREADY_FILLED;

                return callback(error);
            }

            // change state to cancelled. Leave the trade in the list
            exchangeOrder.cancel();

            self.account.cancelOrder(exchangeOrder);

            self.validateBalances('cancel', exchangeOrder);

            var returnedOrder: T = exchangeOrder.clone() as T;

            // update market trades and emit event for any listeners
            self.markets[exchangeOrder.symbol].cancelOrder(returnedOrder);

            self.failures = 0;

            // return the cloned trade with the new exchangeId to the callback
            callback(null, returnedOrder);
        });
    }

    // used to set the number of tickers that will be read with a spread before a filled trade has been found
    //var tickerMargin = 3;

    /**
     * Set the bid and ask prices to be
     * @param symbol
     * @param callback
     */
    getTicker(symbol: string, callback: (error: Error, ticker?: Ticker) => void ): void
    {
        var functionName = format('%s.getTicker()', this.name),
            self = this,
            lastTrade, nextBuyTrade, nextSellTrade;

        logger.trace('%s about to get next ticker', functionName);

        // make an asynchronous call to simulate a real exchange call
        setImmediate(function()
        {
            // if no last trade on the Simulator then use the next historical trade
            // this happens when getFilledTrade is still to find a filled trade
            //if (!self.lastTrade)
            //{
            // if no historical trades have been read from the file
            if (self.historicalTrades.length === 0)
            {
                var error = new VError('%s no trades have been read from historical file %s', functionName, self.filename);
                logger.error(error.stack);
                return self.errorHandler(error, true, 'getTicker', self.getTicker, callback, symbol);
            }

            self.failures = 0;

            lastTrade = self.historicalTrades.shift();

            //if (tickerMargin > 0) {
            //    tickerMargin--;
            //}

            nextBuyTrade = {
                price: new BigNumber(lastTrade.price).
                minus(
                    self.smallestRateBN[symbol].
                    times(self.tickerMargin) ).
                toFixed(self.priceRounding[symbol])
            };

            logger.trace('%s next %s buy trade %s = last price %s - smallest price %s * ticker margin %s', functionName,
                symbol, nextBuyTrade.price,
                lastTrade.price,
                self.smallestRateBN[symbol], self.tickerMargin);

            nextSellTrade = {
                price: new BigNumber(lastTrade.price).
                plus(
                    self.smallestRateBN[symbol].
                    times(self.tickerMargin) ).
                toFixed(self.priceRounding[symbol])
            };

            logger.trace('%s next %s sell trade %s = last price %s + smallest price %s * ticker margin %s', functionName,
                symbol, nextSellTrade.price,
                lastTrade.price,
                self.smallestRateBN[symbol], self.tickerMargin);
            /*
             logger.debug('%s no last trade so using last trade with price %s, buy price %s, sell price %s and timestamp %s = %s', functionName,
             lastTrade.price, nextBuyTrade.price, nextSellTrade.price,
             moment(lastTrade.timestamp).unix(),
             moment(lastTrade.timestamp).format('DD MMM YY hh:mm:ss'));
             }
             else
             {
             lastTrade = self.lastTrade;

             // find the first historical trade with an price <= the price of my last trade
             nextBuyTrade = _.find(self.historicalTrades, function (trade) {
             return self.lastTrade.price <= trade.price;
             });

             if (!nextBuyTrade) {
             logger.debug('%s could not find next buy trade in %s historical trades so will use next historical trade', functionName,
             self.historicalTrades.length);

             nextBuyTrade = self.historicalTrades.shift();
             }

             logger.debug('%s next buy trade in %s historical trades has price %s and unix timestamp %s = %s', functionName,
             self.historicalTrades.length, nextBuyTrade.price,
             moment(nextBuyTrade.timestamp).unix(),
             moment(nextBuyTrade.timestamp).format('DD MMM YY hh:mm:ss'));

             // find the next sell trade in the historical trades still to be processed
             nextSellTrade = _.find(self.historicalTrades, function (trade) {
             return self.lastTrade.price >= trade.price;
             });

             if (!nextSellTrade) {
             logger.warn('%s could not find next sell trade in %s historical trades so will use next historical trade', functionName,
             self.historicalTrades.length);

             nextSellTrade = self.historicalTrades.shift();
             }

             logger.debug('%s next sell trade in %s historical trades has price %s and unix timestamp %s = %s', functionName,
             self.historicalTrades.length, nextSellTrade.price,
             moment(nextSellTrade.timestamp).unix(),
             moment(nextSellTrade.timestamp).format('DD MMM YY hh:mm:ss'));
             }*/

            var newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: nextBuyTrade.price,
                ask: nextSellTrade.price,
                last: lastTrade.price,
                timestamp: lastTrade.timestamp
            });

            logger.debug('%s %s bid %d, ask %d, last %s, next historical %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                self.historicalTrades[0].price,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            self.failures = 0;

            callback(null, newTicker);
        });
    }

    getOrderBook(symbol: string, callback: (error: Error, orderBook?: OrderBook) => void ): void
    {
        var functionName = format('%s.getOrderBook()', this.name);
        var self = this;

        logger.trace('%s about to get ticker in order to create an order book', functionName);

        // make an asynchronous call to simulate a real exchange call
        setImmediate(function() {

            self.getTicker(symbol, function(err, ticker)
            {
                logger.debug('%s create new order book from ticker with bid %s and ask %s', functionName, ticker.bid, ticker.ask);

                const testBids: number[][] = [],
                    testAsks: number[][] = [];

                for (var i=0; i<self.numberOfBidAsks; i++)
                {
                    var amount = self.orderBookStartingAmount + i * self.orderBookStepAmount;

                    // rate = bid * (1 - i/orderBookBidAskGap) - minimum rate * 2
                    var bidRate = new BigNumber(ticker.bid).times(
                        new BigNumber(1).
                        minus(new BigNumber(i).
                            div(self.orderBookBidAskGap)
                        )
                    ).minus(self.smallestRateBN[symbol].times(2) ).
                    toFixed(self.priceRounding[symbol]);

                    logger.trace('%s bid rate %s = bid ticker %s * (1 - i %s / orderBookBidAskGap %s) - minimum rate %s * 2', functionName,
                        bidRate, ticker.bid, i, self.orderBookBidAskGap,
                        self.smallestRateBN[symbol].toString() );

                    // rate = ask * (1 + i / orderBookBidAskGap) + minimum rate * 2
                    var askRate = new BigNumber(ticker.ask).times(
                        new BigNumber(1).
                        plus(new BigNumber(i).
                            div(self.orderBookBidAskGap)
                        )
                    ).plus(self.smallestRateBN[symbol].
                    times(2) ).
                    toFixed(self.priceRounding[symbol]);

                    logger.trace('%s bid rate %s = ask ticker %s * (1 + i %s / orderBookBidAskGap %s) + minimum rate %s * 2', functionName,
                        askRate, ticker.ask, i, self.orderBookBidAskGap,
                        self.smallestRateBN[symbol].toString() );

                    logger.trace('%s iteration %s bidRate %s, askRate %s, amount %s', functionName,
                        i, bidRate, askRate, amount);

                    testBids.push([Number(bidRate), amount]);
                    testAsks.push([Number(askRate), amount]);
                }

                var orderBook = new OrderBook ({
                    exchangeName: self.name,
                    symbol: symbol,
                    bids: testBids, //array of arrays [rate, size]
                    asks: testAsks,
                    timestamp: ticker.timestamp,
                    minAmountToIgnore: self.minOrderBookAmountToIgnore
                });

                self.markets[symbol].addOrderBook(orderBook);

                self.failures = 0;

                callback(null, orderBook);
            });
        });
    };

    /**
     * returns a cloned copy of the Simulator's balances in the callback
     * @param callback
     */
    getAccountBalances(callback: (error?: Error, account?: Account) => void): void
    {
        var functionName = format('%s.getAccountBalances()', this.name),
            self = this;

        logger.trace('%s about to get balances', functionName);

        // make an asynchronous call to simulate a real exchange call
        setImmediate(function() {

            logger.debug('%s Simulator balances:' + self.account.log(), functionName );

            self.failures = 0;

            // return the cloned trade with the new exchangeId to the callback
            callback(null, self.account.clone() );
        });
    }

    /**
     * looks for the any filled orders from the next historical trade over the polling period
     * @param orders
     * @param symbol
     * @param callback
     */
    getFilledOrders<T extends Order>(orders: T[], symbol: string, callback: (error?: Error, trades?: T[])=>void): void
    {
        const functionName = format('%s.getFilledOrders()', this.name),
            self = this,
            filledOrders = [];

        let nextHistoricalTrade,
            tradesSinceLastFill = 0;

        // If callback is passed and is not a function
        if (callback && typeof(callback) !== "function")
        {
            const error = new VError('%s callback function is not a function', functionName);
            logger.error(error.stack);
            throw error;
        }

        // check that there are exchangeId to watch
        if (!orders || !Array.isArray(orders))
        {
            const error = new VError('%s trades parameter is null or not an array', functionName);

            logger.error(error.stack);
            throw error;
        }
        else if (orders.length === 0)
        {
            logger.warn('%s trades array is empty so can not check if any have been filled', functionName);

            return callback(null, []);
        }

        // making an asynchronous call to simulate a real call to an exchange
        setImmediate(function()
        {
            // increment the request number for this getFilledTrades request
            self.requestNumber++;

            var pendingOrders = _.where(orders, {state: 'pending'} );

            if (pendingOrders.length === 0)
            {
                logger.debug('%s no pending orders so returning an empty array of filled orders', functionName);

                return callback(null, []);
            }

            if (logger.transports.console.level === 'trace' || logger.transports.console.level === 'debug')
            {
                logger.debug('%s loop to find next filled orders for the %s pending orders with %s historical trades still to be processed', functionName,
                    pendingOrders.length, self.historicalTrades.length);

                pendingOrders.forEach(function(order, i) {
                    logger.debug('%s pending %s order with id %s, tag %s, rate %s and remaining amount %s', functionName,
                        order.side, order.exchangeId, order.tag, order.price, order.amountRemaining);
                });
            }

            // if there has been a previous failed order then set as next historical trade to ensure the failed order is returned as filled
            if (self.failedOrder !== null)
            {
                logger.info('%s set the previous failed trade with id %s, tag %s, rate %s, amount %s and amount remaining %s as next historical trade', functionName,
                    self.failedOrder.exchangeId, self.failedOrder.tag, self.failedOrder.price,
                    self.failedOrder.amount, self.failedOrder.amountRemaining );

                const historicalTrade = {
                    price: self.failedOrder.price,
                    amount: self.failedOrder.amount,
                    timestamp: self.getTime().toDate()};

                self.historicalTrades.unshift(historicalTrade);

                self.failedOrder = null;
            }

            const startTime = self.getTime(),
            // start time plus simulated public polling period in milliseconds
            // note moment objects are mutable so we need to clone the startTime
                endTime = startTime.clone().add(self.simulatedPublicPollingInterval, 'ms');

            logger.debug('%s simulated start time %s = %s, end time %s = %s, next trade time %s = %s', functionName,
                startTime.unix(), startTime.format('D-MMM-YY h:mm:ss'),
                endTime.unix(), endTime.format('D-MMM-YY h:mm:ss'),
                self.historicalTrades[0].timestamp.getTime() / 1000,
                moment(self.historicalTrades[0].timestamp).format('D-MMM-YY h:mm:ss') );

            // loop through the historical trades for the public polling period or until a buy or sell trade is crossed
            while ( endTime.isAfter(self.historicalTrades[0].timestamp)
            && self.historicalTrades.length > 0)
            {
                //get next historical trade
                nextHistoricalTrade = self.historicalTrades.shift();
                self.historicalTradesProcessed++;
                tradesSinceLastFill++;

                logger.debug('%s %d rate %s, %s, remain %s', functionName,
                    tradesSinceLastFill, nextHistoricalTrade.price,
                    moment(nextHistoricalTrade.timestamp).format('D-MM-YY h:mm:ss'),
                    self.historicalTrades.length);

                // look through the pending orders to see if the historical trade has been crossed
                pendingOrders.forEach(function(pendingOrder, i)
                {
                    let filled = false,
                        filledAmount = 0;

                    // if pending order was crossed with historical trade
                    if (pendingOrder.side === 'sell' && pendingOrder.priceBN.lessThanOrEqualTo(nextHistoricalTrade.price) )
                    {
                        logger.info('%s sell order at index %d, id %s, tag %s, rate %s, amount %s, amount remaining %s, amount last partial %s crossed with historical trade %s with rate %s and amount %s', functionName,
                            i, pendingOrder.exchangeId, pendingOrder.tag, pendingOrder.price,
                            pendingOrder.amount, pendingOrder.amountRemaining, pendingOrder.amountLastPartial,
                            self.historicalTradesProcessed,
                            nextHistoricalTrade.price, nextHistoricalTrade.amount);

                        filled = true;
                    }
                    else if (pendingOrder.side === 'buy' && pendingOrder.priceBN.greaterThanOrEqualTo(nextHistoricalTrade.price) )
                    {
                        logger.info('%s buy order at index %d, id %s, tag %s, rate %s and amount %s, amount remaining %s, amount last partial %s crossed with historical trade %s with rate %s and amount %s', functionName,
                            i, pendingOrder.exchangeId, pendingOrder.tag, pendingOrder.price,
                            pendingOrder.amount, pendingOrder.amountRemaining, pendingOrder.amountLastPartial,
                            self.historicalTradesProcessed,
                            nextHistoricalTrade.price, nextHistoricalTrade.amount);

                        filled = true;
                    }

                    if (filled)
                    {
                        var previouslyFilledInThisRequest = true;

                        // see if the filled trade has already been filled in this request
                        let exchangeOrder = _.find(filledOrders, function(filledOrder) {
                            return filledOrder.exchangeId == pendingOrder.exchangeId;
                        });

                        // if not previously filled then clone from pending order and add to list of filled trades
                        if (!exchangeOrder)
                        {
                            previouslyFilledInThisRequest = false;

                            exchangeOrder = pendingOrder.clone();

                            // push new filled order into list of filled orders to be returned
                            filledOrders.push(exchangeOrder);
                        }

                        // if full fill of the remaining amount
                        // amount remaining <= historical amount
                        if (BigNumber(exchangeOrder.amountRemaining).lessThanOrEqualTo(nextHistoricalTrade.amount) )
                        {
                            filledAmount = exchangeOrder.amountRemaining;

                            logger.debug('%s full fill amount = remaining order amount %s', functionName,
                                exchangeOrder.amountRemaining);

                            // remove the filled trade from the list of pending orders
                            pendingOrders = _.reject(pendingOrders, function(filledTrade) {
                                return filledTrade.exchangeId == pendingOrder.exchangeId;
                            });
                        }
                        else  // if partial fill
                        {
                            filledAmount = nextHistoricalTrade.amount;

                            logger.debug('%s partial fill amount %s = historical trade amount %s', functionName,
                                filledAmount, nextHistoricalTrade.amount);
                        }

                        // crossed so partially fill the order
                        // if previously filled in this request
                        if (previouslyFilledInThisRequest)
                        {
                            exchangeOrder.appendPartialFill(filledAmount);
                        }
                        else
                        {
                            exchangeOrder.partialFill(filledAmount);
                        }

                        exchangeOrder.timestamp = nextHistoricalTrade.timestamp;
                    }
                });

                if (self.historicalTradesProcessed > self.countInSimulatorFile)
                {
                    const error = new VError('%s processed all %s trades in %s simulator file %s', functionName,
                        self.countInSimulatorFile, self.name, self.filename);
                    error.name = 'noMoreTradesOnSimulator';

                    logger.error(error.stack);
                    return callback(error);
                }
            }

            logger.debug('%s %s filled orders to be passed to the callback', functionName, filledOrders.length);

            filledOrders.forEach(function(filledOrder, i)
            {
                logger.info('%s #%s %s %s order with id %s, tag %s, rate %s, amount %s, amount traded %s, amount remaining %s, amount last partial %s', functionName,
                    i, filledOrder.side, filledOrder.state,
                    filledOrder.exchangeId, filledOrder.tag, filledOrder.price,
                    filledOrder.amount, filledOrder.amountTraded,
                    filledOrder.amountRemaining, filledOrder.amountLastPartial);

                // update exchange balance now that we know what the last partial amount will be
                self.account.filledOrder(filledOrder);

                self.validateBalances('fill', filledOrder);

                // find filled order in the exchange trades
                const exchangeOrder = _.findWhere(self.orders, {
                    exchangeId: filledOrder.exchangeId
                    , state: 'pending'
                });

                exchangeOrder.timestamp = nextHistoricalTrade.timestamp;
                exchangeOrder.partialFill(filledOrder.amountLastPartial);

                if (filledOrder.state === 'partiallyFilled')
                {
                    // push the remaining pending order of the partially filled trade into the trades list
                    self.orders.push(filledOrder.remainingPartial() );
                }

                // update market trades, emit a new event for listeners
                self.markets[filledOrder.symbol].partialFillOrder(filledOrder);

                self.lastOrder = filledOrder;
            });

            //pass filled orders back to the callback
            callback(null, filledOrders);
        });
    }

    getPendingOrders<T extends Order>(callback: (error: Error, pendingOrders?: T[]) => void): void
    {
        const functionName = format('%s.getPendingOrders()', this.name),
            self = this;

        logger.trace('%s about to get pending orders', functionName);

        const pendingOrders: T[] = [];

        this.orders.forEach(function(trade)
        {
            if (trade.state === 'pending')
            {
                pendingOrders.push(trade.clone);
            }
        });

        logger.debug('%s %s pending orders where found in %s exchange orders', functionName,
            pendingOrders.length, this.orders.length);

        self.failures = 0;

        callback(null, pendingOrders);
    }

    getOrder(exchangeId: string, callback: (error: Error, trade?: Order) => void): void
    {
        var functionName = format('%s.getOrder()', this.name),
            self = this;

        logger.trace('%s about to get order with id %s', functionName, exchangeId);

        var exchangeOrder;

        this.orders.forEach(function(trade)
        {
            if (trade.exchangeId === exchangeId)
            {
                exchangeOrder = trade.clone();
            }
        });

        if (exchangeOrder)
        {
            logger.debug('%s found order with id %s', functionName, exchangeId);

            self.failures = 0;

            callback(null, exchangeOrder);
        }
        else
        {
            var error = new VError('%s could no find a pending trade with id %s', functionName, exchangeId);
            logger.error(error.stack);
            callback(error);
        }
    }

    /**
     * Validates that the exchanges balances are all positive after an action is performed on a trade.
     * @param action against the trade on the exchange. eg add, cancel, fill
     * @param trade that the action was performed against
     */
    validateBalances(action: string, trade: Order): void
    {
        var functionName = format('%s.validateBalances()', this.name),
            self = this,
            error;

        if ( BigNumber(self.account.balances[trade.fixedCurrency].availableBalance).lessThan(0) )
        {
            error = new VError('%s available fixed currency %s is negative %s from %s %s order with id %s, tag %s, rate %s, amount remaining %s, sell amount remaining %s, last partial amount %s and sell last partial amount %s', functionName,
                trade.fixedCurrency, self.account.balances[trade.fixedCurrency].availableBalance,
                action, trade.side,
                trade.exchangeId, trade.tag,
                trade.price,
                trade.amountRemaining, trade.sell.amountRemaining,
                trade.amountLastPartial, trade.sell.amountLastPartial);
        }
        else if ( BigNumber(self.account.balances[trade.fixedCurrency].totalBalance).lessThan(0) )
        {
            error = new VError('%s total fixed currency %s is negative %s from %s %s order with id %s, tag %s, rate %s, amount remaining %s, sell amount remaining %s, last partial amount %s and sell last partial amount %s', functionName,
                trade.fixedCurrency, self.account.balances[trade.fixedCurrency].totalBalance,
                action, trade.side,
                trade.exchangeId, trade.tag,
                trade.price,
                trade.amountRemaining, trade.sell.amountRemaining,
                trade.amountLastPartial, trade.sell.amountLastPartial);
        }
        else if ( BigNumber(self.account.balances[trade.variableCurrency].availableBalance).lessThan(0) )
        {
            error = new VError('%s available variable currency %s is negative %s from %s %s order with id %s, tag %s, rate %s, amount remaining %s, sell amount remaining %s, last partial amount %s and sell last partial amount %s', functionName,
                trade.variableCurrency, self.account.balances[trade.variableCurrency].availableBalance,
                action, trade.side,
                trade.exchangeId, trade.tag,
                trade.price,
                trade.amountRemaining, trade.sell.amountRemaining,
                trade.amountLastPartial, trade.sell.amountLastPartial);
        }
        else if ( BigNumber(self.account.balances[trade.variableCurrency].totalBalance).lessThan(0) )
        {
            error = new VError('%s total variable currency %s is negative %s from %s %s order with id %s, tag %s, rate %s, amount remaining %s, sell amount remaining %s, last partial amount %s and sell last partial amount %s', functionName,
                trade.variableCurrency, self.account.balances[trade.variableCurrency].totalBalance,
                action, trade.side,
                trade.exchangeId, trade.tag,
                trade.price,
                trade.amountRemaining, trade.sell.amountRemaining,
                trade.amountLastPartial, trade.sell.amountLastPartial);
        }
        else
        {
            // balances are fine so no errors
            return;
        }

        // log and return the error
        error.name = Exchange.NOT_ENOUGH_FUNDS;
        logger.error(error.stack);
        throw error;
    }

    /**
     * This can be overridden to synchronise with times in other exchange
     * @returns {Moment} timestamp of next historical trade to be processed
     */
    getTime(): moment.Moment
    {
        var functionName = format('%s.getTime()', this.name);

        logger.trace('%s getting timestamp of next historical trade to be processed', functionName);

        return moment(this.historicalTrades[0].timestamp);
    }

    // nothing to set on the Simulator
    setTimeout(timeout: number): void {}

    log(debugMessage: string): void
    {
        var functionName = format('%s.log()', this.name);

        var pendingOrders = _.where(this.orders, {state: 'pending'});
        var sortedPendingOrders = _.sortBy(pendingOrders, order => order.exchangeId);

        var filledOrders = _.where(this.orders, {state: 'filled'});
        var sortedFilledOrders = _.sortBy(filledOrders, trade => trade.exchangeId);

        var cancelledTrades = _.where(this.orders, {state: 'cancelled'});
        var sortedCancelledTrades = _.sortBy(cancelledTrades, trade => trade.exchangeId);

        console.log('%s %s pending, %s filled, %s cancelled trades %s', functionName,
            sortedPendingOrders.length, sortedFilledOrders.length, cancelledTrades.length, debugMessage );

        sortedPendingOrders.forEach(function(trade, i) {
            console.log('pending #%s id %s %s rate %s size %s, %s',
                i+1, trade.exchangeId, trade.side, trade.price, trade.amount,
                moment(trade.timestamp).format('DD-MM-YY hh:mm:ss'));
        });

        sortedFilledOrders.forEach(function(trade) {
            console.log('filled order id %s %s rate %s size %s, %s',
                trade.exchangeId, trade.side, trade.price, trade.amount,
                moment(trade.timestamp).format('DD-MM-YY hh:mm:ss'));
        });

        sortedCancelledTrades.forEach(function(trade, i) {
            console.log('cancelled order #%s id %s %s rate %s size %s, %s',
                i+1, trade.exchangeId, trade.side, trade.price, trade.amount,
                moment(trade.timestamp).format('DD-MM-YY hh:mm:ss'));
        });
    }
}
