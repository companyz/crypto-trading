/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// load third party packages
var _ = require('underscore');
var exchange_1 = require("../models/exchange");
var orderBook_1 = require("../models/orderBook");
// My packages
var VError = require('verror');
/**
 * A test implementation of the abstract Exchange class.
 * All abstract methods are implemented to return an error in the callback
 */
var TestExchange = (function (_super) {
    __extends(TestExchange, _super);
    function TestExchange(settings) {
        settings = _.extend(settings, {
            APIkey: 'testAPPKey',
            APIsecret: 'testAPISecret'
        });
        _super.call(this, settings);
    }
    TestExchange.prototype.initOrderBook = function (symbol, bids, asks) {
        this.markets[symbol].latestOrderBook = new orderBook_1.default({
            exchangeName: this.name,
            symbol: symbol,
            bids: bids,
            asks: asks
        });
    };
    ;
    // get latest ticker data from the exchange for a specified market
    TestExchange.prototype.getTicker = function (symbol, callback) {
        callback(new VError('Exchange.getTicker() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getOrderBook = function (symbol, callback) {
        callback(new VError('Exchange.getOrderBook() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getMarketTrades = function (symbol, numberOfTrades, callback) {
        callback(new VError('Exchange.getMarketTrades() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        callback(new VError('Exchange.getAccountTradesBetweenDates() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getPendingOrders = function (callback) {
        callback(new VError('Exchange.getPendingOrders() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getOrderState = function (exchangeId, callback) {
        callback(new VError('Exchange.getOrderState() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getOrder = function (exchangeId, callback) {
        callback(new VError('Exchange.getOrder() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getAccountBalances = function (callback) {
        callback(new VError('Exchange.getAccountBalances() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.addOrder = function (newOrder, callback) {
        callback(new VError('Exchange.addOrder() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        callback(new VError('Exchange.cancelOrder() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        callback(new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name));
    };
    TestExchange.prototype.setTimeout = function (timeout) { };
    return TestExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TestExchange;
