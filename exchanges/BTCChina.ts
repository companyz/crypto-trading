/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import {default as HistoricalAccountTrade, IHistoricalAccountTrade} from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

// My packages
const BTCChina = require("btc-china"),
    logger = require('config-logger'),
    VError = require('verror'),
    config = require('config');

const defaultExchangeSettings: IExchangeSettings = {
	name: 'BTCChina',
	fixedCurrencies: ['BTC', 'LTC'],
	variableCurrencies: ['CNY'],
	commissions: 0,				// default commission rather than defining for each currency
	currencyRounding:{
	    BTC:4,
	    LTC:3,
	    CNY:2},
    priceRounding:{
        BTCCNY: 2,
        LTCCNY: 2,
        LTCBTC: 4
    },
    defaultMinAmount: 0.001,
	publicPollingInterval: 3000,	// milliseconds. Also used for the retry time between failures - both public and private
	privatePollingInterval: 3000,    // polling interval of private methods
	maxFailures: 10,
    feeStructure: 'buyCurrency'    // all fees are in the fixed currency. eg sell BTC will have a BTC fee
};

declare type BTCChinaOrder = {
    id:	number,
    type: string,
    price: string,
    avg_price?: string,
    currency: string,
    amount: string,
    amount_original: string,
    date: number,
    status: string,
    details: {
        price: string,
        amount: number,
        dateline: string
    }[]
}

export default class BTCChinaExchange extends Exchange
{
    client;

    getTransactionLimit;

    constructor(exchangeConfig: IExchangeSettings)
    {
        const functionName = 'BTCChina.constructor()';

        // override the default exchange settings from the config
        const exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);

        //Call the constructor of inherited Exchange object
        super(exchangeSettings);

        this.getTransactionLimit = 1000;

        logger.trace('%s initialising BTC China client', functionName);

        this.client = new BTCChina(
            exchangeConfig.APIkey,
            exchangeConfig.APIsecret
        );
    }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    }

    getOrder(exchangeId: string, callback: (error?: Error, order?: Order) => void): void
    {
        const functionName = 'BTCChinaExchange.getOrder()',
            self = this;

        // TODO remove hard coding of symbol
        const symbol = 'BTCCNY';

        logger.trace('%s about to get order for market trade with id %s', functionName, exchangeId);

        self.client.getOrder(function getOrderDetail(err, json)
        {
            if (err)
            {
                if (err.name == 'Order not found')
                {
                    const error = VError('%s could not find order for id %s in the %s market', functionName,
                        exchangeId, symbol);
                    error.name = Exchange.ID_NOT_FOUND;

                    logger.error(error.stack);
                    return callback(error);
                }

                return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
            }

            self.failures = 0;

            logger.debug('%s successfully got order with id %s and symbol %s', functionName,
                exchangeId, symbol);

            logger.debug('%s json data: %s', functionName, JSON.stringify(json) );

            const order: BTCChinaOrder = json.result.order;

            //convert from BTCChina type
            const side = convertSide(order.type);

            const amountTraded = new BigNumber(order.amount_original).
                minus(order.amount).
                toString();

            const priceBN = BTCChinaExchange.getPriceBN(order);

            let amountLastPartial = '0';
            let numberPartialFills = 0;
            if (order.details && order.details.length > 0 )
            {
                numberPartialFills = order.details.length;

                const lastTrade = _.last(order.details);
                amountLastPartial = lastTrade.amount.toString();

                logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName,
                    order.id, order.details.length,
                    lastTrade.price, lastTrade.amount,
                    moment(new Date(lastTrade.dateline + '000')).format('D MMM YY H:mm:ss'));
            }

            let state = 'unknown';
            if (order.status === 'closed') state = 'filled';
            else if (order.status === 'open' || order.status === 'pending') state = 'pending';
            else if (order.status === 'cancelled') state = 'cancelled';
            else if (order.status === 'error') state = 'error';
            else
            {
                const error = new VError('%s could not convert %s order status %s for id %s', functionName,
                    self.name, order.status, exchangeId);

                logger.error(error.stack);
                return callback(error, null);
            }

            const exchangeOrder = new Order({
                exchangeId: order.id.toString(),
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                amount: order.amount_original.toString(),
                amountTraded: amountTraded,
                amountRemaining: order.amount.toString(),
                amountLastPartial: amountLastPartial,
                numberPartialFills: numberPartialFills,
                price: priceBN,
                timestamp: new Date(order.date * 1000)
            });

            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');

            callback(null, exchangeOrder );

            // id, market, trade details included
        }, parseInt(exchangeId), symbol, true );
    }

    static getPriceBN(order: BTCChinaOrder): BigNumber
    {
        if (order.avg_price)
        {
            return new BigNumber(order.avg_price);
        }
        else
        {
            return new BigNumber(order.price);
        }
    }

    // override abstract function with implementation for this exchange
    getTicker(symbol: string, callback: (error: Error, ticker: Ticker) => void): void
    {
        const functionName = 'BTCChina.getTicker()',
            self = this;

        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);

        this.client.getTicker(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);

            self.failures = 0;

            const newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.ticker.buy),
                ask: Number(json.ticker.sell),
                last: Number(json.ticker.last),
                timestamp: new Date()
            });

            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            callback(null, newTicker);

        }, symbol);
    };

    // override abstract function with implementation for this exchange
    getOrderBook(symbol: string, callback: (error: Error, orderBook: OrderBook) => void): void
    {
        const functionName = 'BTCChina.getOrderBook()',
            self = this;

        logger.trace('%s about to get order book for market %s', functionName, symbol);

        this.client.getOrderBook(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(json) );

            //Convert bid and ask formats to a 2 dimentional array
            const bids = [], asks = [];
            json.bids.forEach(function(order) {
                bids.push([Number(order[0]),Number(order[1])]);	//cast to Number as coming from JSON string
            });
            json.asks.forEach(function(order) {
                asks.unshift([Number(order[0]),Number(order[1])]);	//cast to Number as coming from JSON string
            });

            const newOrderBook = new OrderBook({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(Number(json.date) * 1000),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });

            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0)
            {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName,
                    self.name, symbol,
                    newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length,
                    newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length,
                    moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss') );
            }
            else
            {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks',
                    functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }

            self.markets[symbol].addOrderBook(newOrderBook);

            callback(null, newOrderBook);

        }, symbol);
    }

    // Implementation of abstract function to get account balances on the exchange
    getAccountBalances(callback: (error: Error, account: Account) => void): void
    {
        const functionName = 'BTCChina.getAccountBalances()',
            self = this;

        logger.trace('%s about to get account balances', functionName);

        this.client.getAccountInfo(function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);

            self.failures = 0;

            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(data);

            const returnedAccount = new Account([], self.name, self.currencyRounding);

            const currencies = _.keys(data.result.balance);

            currencies.forEach(function(currency)
            {
                const totalBN = new BigNumber(data.result.balance[currency].amount);

                const availableBN = totalBN.
                minus(data.result.frozen[currency].amount);

                logger.debug('%s %s balances: total %s, available %s = balance amount %s - frozen amount %s', functionName,
                    currency, totalBN.toString(), availableBN.toString(),
                    data.result.balance[currency].amount,
                    data.result.frozen[currency].amount);

                returnedAccount.setBalance(
                    new Balance({
                        exchangeName: self.name,
                        currency: currency.toUpperCase(),
                        totalBalance: totalBN.toString(),
                        availableBalance: availableBN.toString()
                    })
                );
            });

            // set the balances on the exchange
            self.account = returnedAccount;

            callback(null, returnedAccount);
        });
    };

    // gets my pending orders
    getPendingOrdersForSymbol(symbol: string, callback: OrdersCallback): void
    {
        const functionName = 'BTCChina.getPendingOrdersForSymbol()',
            self = this;

        logger.trace('%s about to get pending orders from the %s exchange', functionName, this.name);

        const fixedCurrency = Market.getFixedCurrency(symbol);

        self.client.getOrders(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);

            self.failures = 0;

            logger.debug('%s %s pending orders returned', self.name, json.result.order.length);

            let orders: Order[] = [];

            json.result.order.forEach(function(order: BTCChinaOrder)
            {
                //convert from BTCChina type
                const side = convertSide(order.type);

                const amountBN = new BigNumber(order.amount_original).
                    round(self.currencyRounding[fixedCurrency], BigNumber.ROUND_DOWN);

                const amountRemainingBN = new BigNumber(order.amount).
                    round(self.currencyRounding[fixedCurrency], BigNumber.ROUND_DOWN);

                const amountTradedBN = amountBN.
                    minus(amountRemainingBN);

                const priceBN = new BigNumber(order.price).
                    round(self.priceRounding[symbol], BigNumber.ROUND_DOWN);

                let amountLastPartial = '0';
                let numberPartialFills = 0;
                if (order.details && order.details.length > 0 )
                {
                    numberPartialFills = order.details.length;

                    const lastTrade = _.last(order.details);
                    amountLastPartial = lastTrade.amount.toString();

                    logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName,
                        order.id, order.details.length,
                        lastTrade.price, lastTrade.amount,
                        moment(lastTrade.dateline).format('D MMM YY H:mm:ss'));
                }

                const exchangeOrder = new Order({
                    exchangeId: order.id.toString(),
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    amount: amountBN.toString(),
                    amountTraded: amountTradedBN.toString(),
                    amountRemaining: amountRemainingBN.toString(),
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: numberPartialFills,
                    price: priceBN.toString(),
                    timestamp: new Date(order.date * 1000)
                });

                logger.debug('pending order:');
                exchangeOrder.log('debug');

                orders.push(exchangeOrder);
            });

            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);

            callback(null, orders);

            //openOnly, market, limit, offset, since, withDetail
            // withDetails returns an array of trades filled against the order
        }, true, symbol, 100, 0, 0, true);
    }

    // creates a buy or sell order on an exchange
    addOrder(newOrder: Order, callback: OrderCallback): void
    {
        const functionName = 'BTCChina.addOrder()',
            self = this;

        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');

        // validate the type parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            const error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }

        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                newOrder.exchange.name, this.name);

            logger.error(error.stack);
            return callback(error);
        }

        let price = newOrder.price;
        if (newOrder.type === 'market') { price = null; }

        this.client.createOrder2(
            function(err, json)
            {
                const functionName = 'BTCChina.addOrder.createOrder()#' + newOrder.type;

                if (err)
                {
                    const tradeDesc = format('%s order with price %s, remaining amount %s, sell amount remaining %s %s and tag %s',
                        newOrder.side, newOrder.price, newOrder.amountRemaining,
                        newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);

                    // no need to retry the API call with the following errors.
                    if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                    {
                        return self.addOrderTimeout(err, newOrder, callback);
                    }
                    else if (err.name === 'Insufficient balance')
                    {
                        const expectedAvailableBalanceBN = self.account.balances[newOrder.sell.currency].availableBalanceBN.
                        minus(newOrder.sellAmountRemainingBN);

                        const error = new VError('%s not enough funds on the %s exchange to add %s. Client %s total balance %s, available balance %s, expected available balance %s', functionName,
                            self.name, tradeDesc,
                            newOrder.sell.currency, self.account.balances[newOrder.sell.currency].totalBalance,
                            self.account.balances[newOrder.sell.currency].availableBalance,
                            expectedAvailableBalanceBN.toString() );

                        error.name = Exchange.NOT_ENOUGH_FUNDS;

                        logger.error(error.stack);
                        return callback(error);
                    }
                    else if (err.name === 'Invalid amount')
                    {
                        const error = new VError(err, '%s trade amount remaining %s was too small for the %s exchange to add %s. Configured min amount for the %s market is %s', functionName,
                            newOrder.amountRemaining, self.name, tradeDesc,
                            newOrder.symbol, self.minAmount[newOrder.symbol]);

                        error.name = Exchange.AMOUNT_TOO_SMALL;

                        logger.error(error.stack);
                        return callback(error);
                    }

                    return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
                }

                logger.trace('%s json data: %s', functionName, JSON.stringify(json) );

                const data = {
                    exchangeId: json.result,
                    timestamp: new Date()
                };

                self.addOrderSuccess(newOrder, callback, data);
            },
            newOrder.side, price, newOrder.amountRemaining, newOrder.symbol
        );
    }

    // cancels an pending order
    cancelOrder(cancelOrder: Order, callback: OrderCallback): void
    {
        const functionName = 'BTCChina.cancelOrder()',
            self = this;

        logger.trace('%s about to cancel order with exchange id %s', functionName,
            cancelOrder.exchangeId);

        if (cancelOrder.exchangeName !== this.name)
        {
            const error = new VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }

        this.client.cancelOrder(function(err, json)
        {
            if (err)
            {
                const errorTrade = format('%s order with id %s, tag %s, price %s and amount remaining %s',
                    cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);

                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === 'Order already cancelled')
                {
                    const error = new VError(err, '%s could not cancel %s as it has already been cancelled. Error message: %s', functionName,
                        errorTrade, err.message);
                    error.name = Exchange.ALREADY_CANCELLED;

                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Order already completed')
                {
                    const error = new VError(err, '%s could not cancel %s as it has already been filled', functionName, errorTrade);
                    error.name = Exchange.ALREADY_FILLED;

                    logger.error(error.stack);
                    return callback(error);
                }

                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }

            logger.trace(json);

            self.cancelOrderSuccess(cancelOrder, callback);

        }, parseInt(cancelOrder.exchangeId));
    }

    getMarketTrades(symbol: string, params, callback: HistoricalMarketTradesCallback): void
    {
        const functionName = 'BTCChina.getMarketTrades()',
            self = this;

        if (!params) params = {};

        logger.trace('%s about to get historical trades for the %s market', functionName, symbol);

        const trades: IHistoricalMarketTrade[] = [];

        self.client.getTrades(function(err, json)
        {
            if (err) return self.errorHandler(err, true, 'getTrades', self.getMarketTrades, callback, symbol, params);

            self.failures = 0;

            json.forEach(function(trade)
            {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date * 1000)
                });
            });

            logger.debug('%s %s historical trades returned', functionName, trades.length);

            callback(null, trades);

        }, symbol, params);
    };

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate: Date = new Date() ): void
    {
        const functionName = 'BTCChina.getAccountTradesBetweenDates()',
            self = this;

        // TODO need to get both LTCBNY and BTCCNY with an async.parallel
        const symbol = 'BTCCNY';

        const since = moment(fromDate).unix();

        logger.trace('%s about to get account trades from %s (%s) to %s', functionName,
            moment(fromDate).format('DD MMM YYYY HH:mm:ss'), since,
            moment(toDate).format('DD MMM YYYY HH:mm:ss') );

        self.client.getOrders(function(err, json)
        {
            if (err) return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);

            self.failures = 0;

            let trades: HistoricalAccountTrade[] = [];

            json.result.order.forEach(function(order)
            {
                if (!order.details) { return; }

                // is the returned trade type a key in the typeMap
                order.details.forEach(function(trade)
                {
                    const tradeDate = new Date(trade.dateline * 1000);

                    if (fromDate <= tradeDate &&
                        tradeDate <= toDate )
                    {
                        const historicalAccountTrade = new HistoricalAccountTrade({
                            exchangeName: self.name,
                            symbol: symbol,
                            side: convertSide(order.type),
                            priceBN: new BigNumber(trade.price),
                            quantityBN: new BigNumber(trade.amount),
                            timestamp: tradeDate,
                            orderId: order.id,
                            feeAmountBN: BigNumber(0),
                            feeCurrency: 'AUD'  // it doesn't matter what the currency is as there is no fee
                        });

                        trades.push(historicalAccountTrade);
                    }
                });
            });

            logger.debug('%s %s my trades returned', functionName, trades.length);

            callback(null, trades);

        }, false, symbol, this.getTransactionLimit, 0, since, true);
        // openOnly, market, limit, offset, since, withDetail
    }

    setTimeout(timeout: number)
    {
        this.client.timeout = timeout;
    }
}

function convertSide(side: string): string
{
    const functionName = 'BTCChina.convertSide()';

    let convertedSide: string;

    if (side === 'bid')
    {
        convertedSide = 'buy';
    }
    else if (side === 'ask')
    {
        convertedSide = 'sell';
    }
    else if (side === 'buy')
    {
        convertedSide = 'bid';
    }
    else if (side === 'sell')
    {
        convertedSide = 'ask';
    }
    else
    {
        const error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
        logger.error(error.stack);
        throw error;
    }

    logger.trace('%s side %s convert to %s', functionName, side, convertedSide);

    return convertedSide;
}