/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var async = require('async');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
var logger = require('config-logger'), VError = require('verror'), OkCoin = require("okcoin"), config = require('config');
var defaultExchangeSettings = {
    name: 'OKCoin',
    fixedCurrencies: ['BTC', 'LTC'],
    variableCurrencies: ['USD'],
    commissions: 0.002,
    currencyRounding: {
        BTC: 8,
        LTC: 8,
        AUD: 2 },
    priceRounding: {
        BTCUSD: 2,
        LTCUSD: 4,
        LTCBTC: 8
    },
    defaultMinAmount: 0.01,
    publicPollingInterval: 15000,
    privatePollingInterval: 5000,
    maxFailures: 10,
    feeStructure: 'buyCurrency'
};
var OKCoinExchange = (function (_super) {
    __extends(OKCoinExchange, _super);
    function OKCoinExchange(exchangeConfig) {
        var functionName = 'OKCoin.constructor()';
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        this.getTransactionLimit = 10000;
        var server;
        if (exchangeSettings.testInstance) {
            server = 'http://dev.okcoin.net';
            logger.info('%s using test server %s', functionName, server);
        }
        logger.trace('%s initialising OKCoin client', functionName);
        this.client = new OkCoin(exchangeSettings.APIkey, exchangeSettings.APIsecret, server);
    }
    // TODO need to implement
    OKCoinExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    // TODO need to implement
    OKCoinExchange.prototype.getOrder = function (exchangeId, callback) {
        var error = new VError('Exchange.getOrder() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    OKCoinExchange.prototype.getTicker = function (symbol, callback) {
        var functionName = 'OKCoin.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        var okCoinSymbol = OKCoinExchange.getSymbol(symbol);
        this.client.getTicker(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.ticker.buy),
                ask: Number(json.ticker.sell),
                last: Number(json.ticker.last),
                timestamp: new Date(json.date * 1000)
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        }, okCoinSymbol);
    };
    OKCoinExchange.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'OKCoin.getOrderBook()', self = this;
        logger.trace('%s about to get order book for market %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol), okCoinSymbol = OKCoinExchange.getSymbol(symbol);
        logger.trace('%s fixed currency %s, variable currency %s, symbol %s', functionName, fixedCurrency, variableCurrency, okCoinSymbol);
        this.client.getDepth(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(json));
            //Convert bid and ask formats to a 2 dimensional array
            var bids = [], asks = [];
            json.bids.forEach(function (order) {
                bids.push([Number(order[0]), Number(order[1])]); //cast to Number as coming from JSON string
            });
            json.asks.forEach(function (order) {
                asks.unshift([Number(order[0]), Number(order[1])]); //cast to Number as coming from JSON string
            });
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            if (_.isFunction(callback))
                callback(null, newOrderBook);
        }, okCoinSymbol);
    };
    ;
    OKCoinExchange.prototype.getAccountBalances = function (callback) {
        var functionName = 'OKCoin.getAccountBalances()', self = this;
        logger.trace('%s about to get account balances', functionName);
        this.client.getUserInfo(function (err, data) {
            if (err)
                return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s balances returned from the %s exchange:', functionName, self.name);
            logger.trace(data);
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            ['btc', 'usd', 'ltc'].forEach(function (currency) {
                var totalBN = new BigNumber(data.info.funds.free[currency]).
                    plus(data.info.funds.freezed[currency]).
                    round(self.currencyRounding[currency.toUpperCase()], BigNumber.ROUND_DOWN);
                logger.debug('%s %s total %s = free %s + freezed %s', functionName, currency, totalBN.toString(), data.info.funds.free[currency], data.info.funds.freezed[currency]);
                returnedAccount.setBalance(new balance_1.default({
                    exchangeName: self.name,
                    currency: currency.toUpperCase(),
                    totalBalance: totalBN.toString(),
                    availableBalance: data.info.funds.free[currency]
                }));
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    ;
    // gets my pending orders - not the market orders/depth
    OKCoinExchange.prototype.getPendingOrdersForSymbol = function (symbol, callback) {
        var functionName = 'OKCoin.getPendingOrdersForSymbol()', self = this;
        logger.trace('%s about to get pending orders on the %s exchange for the %s market', functionName, this.name, symbol);
        var orders = [];
        var okCoinSymbol = OKCoinExchange.getSymbol(symbol);
        self.client.getOrderInfo(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);
            self.failures = 0;
            logger.debug('%s %s pending orders returned', self.name, json.orders.length);
            json.orders.forEach(function (order) {
                var amountRemainingBN = new BigNumber(order.amount).
                    minus(order.deal_amount);
                var exchangeOrder = new order_1.default({
                    exchangeId: order.order_id,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: OKCoinExchange.getState(order.status),
                    side: order.side,
                    amount: order.amount,
                    amountTraded: order.deal_amount,
                    amountRemaining: amountRemainingBN.toString(),
                    price: order.priceBN.toString(),
                    timestamp: new Date()
                });
                logger.debug('pending order:');
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        }, okCoinSymbol, '-1');
    };
    ;
    // creates a buy or sell order on an exchange
    OKCoinExchange.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'OKCoin.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var symbol = OKCoinExchange.getSymbol(newOrder.symbol);
        this.client.addOrder(function (err, json) {
            if (err) {
                var orderDesc = util_1.format('%s trade with price %s, remaining amount %s, sell amount remaining %s %s and tag %s on %s[%s]', functionName, newOrder.side, newOrder.priceBN, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag, newOrder.exchangeName, newOrder.symbol);
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.name === 10010 || err.name === 10016 || err.name === 10035) {
                    var error = new VError(err, '%s not enough funds on add %s', functionName, self.name, orderDesc);
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 10011) {
                    var error = new VError(err, '%s trade amount %s too small to add %s', functionName, newOrder.amountRemaining, orderDesc);
                    error.name = exchange_1.default.AMOUNT_TOO_SMALL;
                    logger.error(error.stack);
                    return callback(error);
                }
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            var data = {
                exchangeId: json.order_id,
                timestamp: new Date()
            };
            self.addOrderSuccess(newOrder, callback, data);
        }, symbol, newOrder.side, newOrder.amountRemaining, newOrder.price);
    };
    // cancels an pending order
    OKCoinExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'OKCoin.cancelOrder()', self = this;
        logger.trace('%s about to cancel order with exchange id %s', functionName, cancelOrder.exchangeId);
        if (cancelOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }
        var symbol = OKCoinExchange.getSymbol(cancelOrder.symbol);
        this.client.cancelOrder(function (err, json) {
            if (err) {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === 10009) {
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            self.cancelOrderSuccess(cancelOrder, callback);
        }, symbol, cancelOrder.exchangeId);
    };
    OKCoinExchange.prototype.getMarketTrades = function (symbol, numberOfTrades, callback) {
        var functionName = 'OKCoin.getMarketTrades()', self = this;
        logger.trace('%s about to get historical trades for market %s', functionName, symbol);
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol), okCoinSymbol = fixedCurrency + '_' + variableCurrency;
        logger.trace('%s fixed currency %s, variable currency %s, symbol %s', functionName, fixedCurrency, variableCurrency, okCoinSymbol);
        this.client.getTrades(function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);
            self.failures = 0;
            var trades = [];
            json.forEach(function (trade) {
                trades.push({
                    exchangeName: this.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date_ms)
                });
            });
            logger.debug('%s %s historical trades returned', functionName, trades.length);
            callback(null, trades);
        }, okCoinSymbol);
    };
    OKCoinExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        var functionName = 'OKCoin.getAccountTradesBetweenDates()', self = this, maxTradesPerCall = 600, returnedTrades = [];
        var sinceTid = 1, tradesInCall, lastTradeTimestamp;
        logger.trace('%s about to get historical account trades from %s (%s) to %s (%s)', functionName, fromDate.toString(), fromDate.getTime(), toDate.toString(), toDate.getTime());
        async.doUntil(function (callback) {
            self.getAccountTradesSinceTid(sinceTid, function (err, trades) {
                if (err)
                    return callback(err);
                trades.forEach(function (trade) {
                    if (fromDate <= trade.timestamp &&
                        trade.timestamp <= toDate) {
                        returnedTrades.push(trade);
                    }
                });
                var lastTradeId = _.last(trades).tradeId;
                sinceTid = Number(lastTradeId) + 1;
                logger.trace('%s last trade id = %s. Next since tid = %s', functionName, lastTradeId, sinceTid);
                tradesInCall = trades.length;
                callback(null);
            });
        }, 
        // do until this function returns true
        function () {
            // stop if the last page did not equal to the max number of trades in a page
            return tradesInCall != maxTradesPerCall || lastTradeTimestamp > toDate;
        }, function (err) {
            callback(err, returnedTrades);
        });
    };
    OKCoinExchange.prototype.getAccountTradesSinceTid = function (sinceTid, callback) {
        var functionName = 'OKCoin.getAccountTradesSinceTid()', self = this;
        // hard coding for now. other option is LTCUSD
        var symbol = 'BTCUSD';
        logger.trace('%s about to get historical account trades since trade id %s for market ', functionName, sinceTid, symbol);
        var okCoinSymbol = OKCoinExchange.getSymbol(symbol);
        this.client.getTradeHistory(function (err, trades) {
            if (err)
                return self.errorHandler(err, true, 'getSingleAccountTradesSinceDate', self.getAccountTradesSinceTid, callback, sinceTid);
            self.failures = 0;
            logger.trace('%s data returned: %s', functionName, JSON.stringify(trades));
            var returnedTrades = [];
            trades.forEach(function (trade) {
                var tradeDate = new Date(trade.date_ms);
                var historicalAccountTrade = new historicalAccountTrade_1.default({
                    exchangeName: self.name,
                    symbol: symbol,
                    side: trade.side,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: tradeDate,
                    tradeId: trade.tid
                });
                returnedTrades.push(historicalAccountTrade);
                logger.debug('%s, %s', functionName, trade.tid, tradeDate.toDateString());
            });
            logger.debug('%s %s historical trades on my account', functionName, returnedTrades.length);
            callback(null, returnedTrades);
        }, okCoinSymbol, sinceTid);
    };
    OKCoinExchange.prototype.setTimeout = function (timeout) {
        this.client.timeout = timeout;
    };
    OKCoinExchange.getState = function (status) {
        if (status == -1)
            return "cancelled";
        else if (status == 0)
            return "pending";
        else if (status == 1)
            return "pendingPartial";
        else if (status == 2)
            return "filled";
        else if (status == 4)
            return "cancelled";
        else
            return "unknown";
    };
    OKCoinExchange.getSymbol = function (symbol) {
        var fixedCurrency = market_1.default.getFixedCurrency(symbol), variableCurrency = market_1.default.getVariableCurrency(symbol);
        return fixedCurrency.toLowerCase() + '_' + variableCurrency.toLowerCase();
    };
    return OKCoinExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = OKCoinExchange;
