/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />

import * as moment from 'moment';
import * as _ from 'underscore';
import * as async from 'async';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

// import my Interfaces
import {default as Exchange, IExchangeSettings, OrderCallback, OrdersCallback,
    HistoricalTradesCallback,
    IHistoricalMarketTrade, HistoricalMarketTradesCallback}
    from "../models/exchange";
import Market from "../models/market";
import Ticker from "../models/ticker";
import Balance from "../models/balance";
import Account from "../models/account";
import OrderBook from "../models/orderBook";
import Order from "../models/order";
import HistoricalAccountTrade from "../models/historicalAccountTrade";
import {default as Movement, MovementType} from "../models/movement";

// My packages
const Bitfinex = require('bitfinex'),
    logger = require('config-logger'),
    VError = require('verror'),
    config = require('config');

interface IBitfinexExchangeSettings extends IExchangeSettings {
    bitfinexAccountType: string
}

const defaultExchangeSettings: IBitfinexExchangeSettings =
{
	name: 'Bitfinex',
    bitfinexAccountType: 'exchange',
	fixedCurrencies: ['BTC','LTC','DRK'], //,'NMC','NVC','USD','TRC','PPC','FTC','XPM'],
	variableCurrencies: ['USD','BTC'], //,'LTC','RUR','EUR'],
	commissions: {maker: 0.001, taker: 0.002},
	currencyRounding:{
	    BTC: 8,
	    LTC: 8,
	    DRK: 8,
	    USD: 8},		// decimal places to be used in rounding currency amounts
    priceRounding:{
        BTCUSD: 2,
        LTCUSD: 3,
        DRKUSD: 3,
        LTCBTC: 6
    },
    defaultPriceRounding: 2,
	defaultMinAmount: 0.01,
	publicPollingInterval: 5100,	// milliseconds
	privatePollingInterval: 1100,    //polling interval of private methods
	maxFailures: 10
};

export default class BitfinexExchange extends Exchange
{
    client;

    bitfinexAccountType: string;
    bitfinexOrderTypePrefix: string;

    maxTrades: number;
    maxMovements: number;

    constructor(exchangeConfig: IBitfinexExchangeSettings)
    {
        const functionName = 'Bitfinex.constructor()';

        // override the default exchange settings from the config
        const exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);

        //Call the constructor of inherited Exchange object
        super(exchangeSettings);

        // default to the exchange account instead of the trading account which doesn't have a prefix to the order type
        // see New order in the API doco https://www.bitfinex.com/pages/api
        this.bitfinexAccountType = exchangeSettings.bitfinexAccountType || 'exchange';

        if (this.bitfinexAccountType === 'exchange')
        {
            this.bitfinexOrderTypePrefix = 'exchange ';
        }
        else if (this.bitfinexAccountType === 'trading')
        {
            // trader account does not have a prefix to the order type
            this.bitfinexOrderTypePrefix = '';
        }

        this.maxTrades = 10000;
        this.maxMovements = 500;

        logger.trace('%s initialising Bitfinex client', functionName);

        this.client = new Bitfinex(
            exchangeConfig.APIkey,
            exchangeConfig.APIsecret
        );
    }

    getOrder(exchangeId: string, callback: (error: Error, order?: Order) => void): void
    {
        const functionName = 'Bitfinex.getOrder()',
            self = this;

        if (!exchangeId || typeof exchangeId != 'string')
        {
            const error = new VError('%s exchangeId %s is undefined or is not a string', functionName,
                exchangeId);
            logger.error(error.message);
            return callback(error);
        }

        logger.trace('%s about to get order with id %s', functionName, exchangeId);

        this.client.order_status(exchangeId, function getOrderStatus(err, order)
        {
            if (err)
            {
                if (err.name === 'No such order found.')
                {
                    const error = VError(err, '%s could not find exchangeId %s on the %s exchange', functionName,
                        exchangeId, self.name);

                    error.name = Exchange.ID_NOT_FOUND;
                    logger.error(error.stack);
                    return callback(error);
                }
                else
                {
                    return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
                }
            }

            self.failures = 0;

            logger.trace('order returned: %s', JSON.stringify(order));

            let state = 'pending';
            let numberPartialFills = 0;
            if (order.is_cancelled)
            {
                state = 'cancelled';
            }
            else if (!order.is_live)
            {
                numberPartialFills = 1;
                state = 'filled';
            }

            let price: string;
            let type = 'limit';
            if (order.type == 'exchange market' || order.type == 'market')
            {
                price = order.avg_execution_price;
                type = 'market';
            }
            else    // limit, stop or trailing-stop order type
            {
                price = order.price;
            }

            const exchangeOrder = new Order({
                exchangeId: exchangeId,
                exchangeName: self.name,
                symbol: order.symbol.toUpperCase(),
                state: state,
                side: order.side,
                type: type,
                amount: order.original_amount,
                amountTraded: order.executed_amount,
                amountRemaining: order.remaining_amount,
                numberPartialFills: numberPartialFills,
                price: price,
                timestamp: new Date(order.timestamp * 1000)
            });

            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');

            callback(null, exchangeOrder);
        });
    }

    getTicker(symbol: string, callback: (error: Error, ticker?: Ticker) => void): void
    {
        const functionName = 'Bitfinex.getTicker()',
            self = this;

        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);

        const pair = symbol.toLowerCase();

        this.client.ticker(pair, function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);

            self.failures = 0;

            logger.trace('%s data returned: %s', functionName, JSON.stringify(data) );

            const newTicker = new Ticker({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(data.bid),		// cast to number as coming from JSON string
                ask: Number(data.ask),
                last: Number(data.last_price),
                timestamp: new Date(data.timestamp * 1000)
            });

            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName,
                newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last,
                moment(newTicker.timestamp).format('D MMM YY H:mm:ss') );

            self.markets[symbol].addTicker(newTicker);

            callback(null, newTicker);
        });
    }

    getOrderBook(symbol: string, callback: (error: Error, orderBook?: OrderBook) => void): void
    {
        const functionName = 'Bitfinex.getOrderBook()',
            self = this;

        const pair = symbol.toLowerCase();

        const fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        logger.trace('%s pair: %s fixed: %s var: %s', functionName, pair, fixedCurrency, variableCurrency);

        this.client.orderbook(pair, function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);

            self.failures = 0;

            logger.trace('%s data returned: %s', functionName, JSON.stringify(data) );

            logger.debug('%s returned %d bids, %d asks', functionName, data.bids.length, data.asks.length);

            //Convert Bitfinex bid and ask formats to a 2 dimentional array
            const bids = [], asks = [];
            data.bids.forEach(function(order, index) {
                bids[index] = [Number(order.price),Number(order.amount)];	//cast o Number as coming from JSON string
            });
            data.asks.forEach(function(order, index) {
                asks[index] = [Number(order.price),Number(order.amount)];	//cast o Number as coming from JSON string
            });

            // create a new OrderBook object
            const newOrderBook = new OrderBook({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });

            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0)
            {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName,
                    self.name, symbol,
                    newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length,
                    newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length,
                    moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss') );
            }
            else
            {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName,
                    self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }

            self.markets[symbol].addOrderBook(newOrderBook);

            callback(null, newOrderBook);
        });
    }

    getAccountBalances(callback: (error: Error, account?: Account) => void): void
    {
        const functionName = 'Bitfinex.getAccountBalances()',
            self = this;

        logger.trace('%s getting account balances', functionName);

        this.client.wallet_balances(function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);

            self.failures = 0;

            logger.trace('%s wallets balance data: %s', functionName, JSON.stringify(data));

            const returnedAccount = new Account([], self.name, self.currencyRounding);

            data.forEach(function(balance)
            {
                if (balance.type === self.bitfinexAccountType)
                {
                    logger.debug('%s currency %s, amount %s, available %s in %s account type', functionName,
                        balance.currency, balance.amount, balance.available, balance.type);

                    const upperCurrency = balance.currency.toUpperCase();

                    returnedAccount.setBalance(
                        new Balance({
                            exchangeName: self.name,
                            currency: upperCurrency,
                            // need to round as the API returns USD to 10 decimal places - not 8
                            totalBalance: new BigNumber(balance.amount).
                                toFixed(self.currencyRounding[upperCurrency]),
                            availableBalance: new BigNumber(balance.available).
                                toFixed(self.currencyRounding[upperCurrency])
                        })
                    );
                }
                else if (Number(balance.amount) !== 0)
                {
                    logger.warn('%s not configured to used the %s wallets with currency %s, amount %s, available %s', functionName,
                        balance.type, balance.currency, balance.amount, balance.available);
                }
            });

            // set the balances on the exchange
            self.account = returnedAccount;

            callback(null, returnedAccount);
        });
    }

    getPendingOrders(callback: OrdersCallback): void
    {
        const functionName = 'Bitfinex.getPendingOrders()',
            self = this;

        logger.trace('%s getting pending orders from the exchange', functionName);

        let orders: Order[] = [];

        this.client.active_orders(function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getPendingOrders', self.getPendingOrders, callback);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(data) );

            logger.trace('%s %s pending orders returned from the exchange:', functionName, data.length);

            data.forEach(function(order)
            {
                const fixedCurrency = order.symbol.slice(0,3).toUpperCase(),
                    variableCurrency = order.symbol.slice(3).toUpperCase(),
                    symbol = fixedCurrency + variableCurrency;
                logger.trace('%s fixedCurrency = %s, variableCurrency = %s', functionName, fixedCurrency, variableCurrency);

                logger.trace('%s %s %s %d @ %d %s id:%s', functionName,
                    order.side, order.symbol, order.original_amount, order.price,
                    moment(order.timestamp * 1000).format('DMMMYY H:mm:ss'), order.id);

                const exchangeOrder = new Order({
                    exchangeId: order.id,
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: order.side,
                    amount: order.original_amount,
                    amountTraded: order.executed_amount,
                    amountRemaining: order.remaining_amount,
                    price: BigNumber(order.price).toString(),
                    timestamp: new Date(order.timestamp * 1000)
                });

                logger.debug('%s pending order:', functionName);
                exchangeOrder.log('debug');

                orders.push(exchangeOrder);
            });

            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        });
    };

    addOrder(newOrder, callback: OrderCallback): void
    {
        const functionName = 'Bitfinex.addOrder()',
            self = this;

        logger.trace('%s about to add the following new order:', functionName );
        newOrder.log('trace');

        // validate the type parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy')
        {
            const error = VError('%s order failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }

        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            const error = VError('%s order is for exchange %s and not this exchange %s',
                functionName, newOrder.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }

        const symbol = newOrder.symbol.toLowerCase(),
            bitfinexOrderType = self.bitfinexOrderTypePrefix + newOrder.type;

        let price;

        if (newOrder.type === 'market')
        {
            price = '1';
        }
        else
        {
            price = newOrder.price;
        }

        logger.debug('%s symbol %s, bitfinex order type %s, price %s', functionName,
            symbol, bitfinexOrderType, price );

        // bitfinex.new_order: (symbol, amount, price, exchange, side, type, cb) ->
        // Bitfinex side = buy or sell which is my trade type
        // Bitfinex type = "market" / "limit" / "stop" / "trailing-stop" / "exchange market" / "exchange limit" / "exchange stop" / "exchange trailing-stop
        this.client.new_order(symbol, newOrder.amountRemaining, price, 'bitfinex', newOrder.side, bitfinexOrderType, function(err, data)
        {
            if (err)
            {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                else if (err.name === 'Invalid order: not enough balance')
                {
                    const error = new VError('%s %s %s is not enough funds on the %s exchange to add %s order with remaining amount %s, sell amount remaining %s %s and tag %s', functionName,
                        self.account.balances[newOrder.sell.currency].availableBalance, newOrder.sell.currency,
                        self.name, newOrder.side, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.tag);

                    error.name = Exchange.NOT_ENOUGH_FUNDS;

                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'Invalid order: minimum amount for exchange order is 0.001')
                {
                    const error = new VError('%s trade amount remaining %s was too small for the %s exchange and %s order with tag %s. Configured min amount for the %s market is %s', functionName,
                        newOrder.amountRemaining, self.name, newOrder.side, newOrder.tag,
                        newOrder.symbol, self.minAmount[newOrder.symbol]);

                    error.name = Exchange.AMOUNT_TOO_SMALL;

                    logger.error(error.stack);
                    return callback(error);
                }

                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }

            logger.trace('%s data: %s', functionName, JSON.stringify(data) );

            const exchangeData = {
                exchangeId: data.id.toString(),
                timestamp: new Date(data.timestamp * 1000)
            };

            self.addOrderSuccess(newOrder, callback, exchangeData);
        });

    };

    cancelOrder(cancelOrder: Order, callback: OrderCallback): void
    {
        const functionName = 'Bitfinex.cancelOrder()',
            self = this;

        if (cancelOrder.exchangeName !== this.name)
        {
            const error = VError('%s order is for exchange %s and not this exchange %s', functionName,
                cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }

        logger.trace('%s about to cancel order id %s', functionName, cancelOrder.exchangeId);

        this.client.cancel_order(cancelOrder.exchangeId, function(err, data)
        {
            if (err)
            {
                const orderDesc = format('%s order with id %s, tag %s, price %s and amount remaining %s',
                    cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);

                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT')
                {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                else if (err.name === "Order could not be cancelled.")
                {
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }

                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }

            logger.trace('%s data: %s', functionName, JSON.stringify(data) );

            self.cancelOrderSuccess(cancelOrder, callback);
        });
    }

    cancelAllOrders(callback: (error: Error, cancelOrderCount?: number) => void): void
    {
        const functionName = 'Bitfinex.cancelAllOrders()',
            self = this;

        logger.trace('%s about to cancel all orders on the exchange', functionName);

        this.client.cancel_all_orders(function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'cancelAllOrders', self.cancelAllOrders, callback);

            self.failures = 0;

            logger.trace('%s data: %s', functionName, JSON.stringify(data) );

            logger.debug('%s all pending orders were cancelled', functionName);

            let exchangeOrders: Order[] = [];

            // for each market
            _.values(self.markets).forEach(function(market)
            {
                // get the pending orders from market
                const pendingMarketOrders: Order[] = _.where(market.orders, {state: 'pending'});

                logger.debug('%s got %s pending orders for the %s market', functionName,
                    pendingMarketOrders.length, market.symbol);

                // concat market orders to all exchange orders
                exchangeOrders = exchangeOrders.concat(pendingMarketOrders);
            });

            const returnedOrders: Order[] = [];

            exchangeOrders.forEach(function(exchangeOrder)
            {
                const returnOrder = exchangeOrder.clone();

                returnOrder.cancel();

                self.account.cancelOrder(exchangeOrder);

                // update market orders, emit a new event for listeners of new order events
                self.markets[exchangeOrder.symbol].cancelOrder(returnOrder);

                returnedOrders.push(returnOrder);
            });

            callback(null, returnedOrders.length);
        });
    }

    // replace an old order with a new order
    // TODO commenting out for now as it needs to handle timeouts and failed cancels
    /*
    replaceOrder(oldOrder: Order, newOrder: Order, callback: (error?: Error, exchangeOrder?: Order) => void): void
    {
        const functionName = 'Bitfinex.replaceOrder()',
            self = this;

        logger.trace('%s about to replace order with id %s with the following new order', functionName,
            oldOrder.exchangeId);
        newOrder.log('trace');

        if (oldOrder.exchangeName !== this.name || newOrder.exchangeName !== this.name)
        {
            const error = new VError('%s old order exchange %s or new order exchange %s is not exchange %s',
                functionName, oldOrder.exchangeName, newOrder.exchangeName, this.name);

            logger.error(error.message);
            throw error;
        }

        const symbol = newOrder.symbol.toLowerCase(),
            bitfinexOrderType = self.bitfinexOrderTypePrefix + newOrder.type;

        logger.trace('%s symbol %s, bitfinexOrderType %s', functionName,
            symbol, bitfinexOrderType);

        this.client.replace_order(oldOrder.exchangeId, symbol, newOrder.amountRemaining, newOrder.price, 'bitfinex', newOrder.side, bitfinexOrderType,
            function(err, data)
            {
                if (err)
                {
                    if (err.name === "404" || err.name === 'Order could not be cancelled.')
                    {
                        const error = new VError(err, '%s could not replace order with id %s. Assume the order has already been filled', functionName,
                            oldOrder.exchangeId);
                        error.name = Exchange.ALREADY_FILLED;

                        logger.error(error.stack);
                        return callback(error);
                    }

                    return self.errorHandler(err, true, 'replaceOrder', self.replaceOrder, callback, oldOrder, newOrder);
                }

                self.failures = 0;

                logger.trace('%s data: %s', functionName, JSON.stringify(data) );

                logger.debug('%s order id %s cancelled', functionName, oldOrder.exchangeId);

                const oldExchangeOrder = oldOrder.clone();
                oldExchangeOrder.cancel();

                self.account.cancelOrder(oldExchangeOrder);

                // update market orders and emit a new event for listeners of new order events
                self.markets[newOrder.symbol].cancelOrder(oldExchangeOrder);

                // if there is a latest order book for the market the order was just removed from
                if (self.markets[oldOrder.symbol].latestOrderBook &&
                    _.isFunction(self.markets[oldOrder.symbol].latestOrderBook.removeOrder) )
                {
                    // remove my order I've just cancelled from the latest market order book
                    self.markets[oldOrder.symbol].latestOrderBook.removeOrder(oldOrder);
                }

                const newExchangeOrder = newOrder.clone();
                newExchangeOrder.exchangeId = data.id.toString();
                newExchangeOrder.state = 'pending';
                newExchangeOrder.timestamp = new Date(data.timestamp * 1000);

                self.account.addOrder(newExchangeOrder);

                // update market orders and emit a new event for listeners of new order events
                self.markets[newOrder.symbol].addOrder(newExchangeOrder);

                // if there is a latest order book for the market the new order was just added to
                if (self.markets[newOrder.symbol].latestOrderBook &&
                    _.isFunction(self.markets[newOrder.symbol].latestOrderBook.addOrder) )
                {
                    // add my new order to the latest market order book
                    self.markets[newOrder.symbol].latestOrderBook.addOrder(newExchangeOrder);
                }

                callback(null, newExchangeOrder);
            });
    }
    */

    getMarketTrades(symbol: string, numberOfTrades: number, callback: HistoricalMarketTradesCallback): void
    {
        const functionName = 'Bitfinex.getMarketTrades()',
            self = this;

        // default the number of trades to be retrieved
        const bitfinexParams = {
            limit_trades: numberOfTrades || 10000
            //, timestamp: params.since * 1000
        };

        const pair = symbol.toLowerCase();

        const fixedCurrency = Market.getFixedCurrency(symbol),
            variableCurrency = Market.getVariableCurrency(symbol);

        logger.trace('%s pair: %s fixed: %s var: %s', functionName, pair, fixedCurrency, variableCurrency);

        this.client.trades(pair, bitfinexParams, function(err, data)
        {
            if (err) return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);

            self.failures = 0;

            let trades: IHistoricalMarketTrade[] = [];

            data.forEach(function(trade)
            {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.timestamp * 1000)
                });
            });

            logger.debug('%s %s historical trades returned', functionName, trades.length);

            callback(null, trades);
        });
    }

    getAccountTradesBetweenDates(callback: HistoricalTradesCallback, fromDate: Date, toDate: Date = new Date() ): void
    {
        const functionName = 'Bitfinex.getAccountTradesBetweenDates()',
            self = this;

        // TODO remove hard coding of market
        const symbol = 'BTCUSD';

        logger.trace('%s about to get historical account trades between %s (%s) and %s (%s)', functionName,
            fromDate.toString(), fromDate.getTime() / 1000,
            toDate.toString(), toDate.getTime() / 1000 );

        // TODO need multiple past_trade calls if more than max transactions is returned
        this.client.past_trades(symbol, fromDate.getTime() / 1000, toDate.getTime() / 1000, this.maxTrades, function(err, trades)
        {
            if (err) return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);

            self.failures = 0;

            logger.trace('%s data returned: %s', functionName, JSON.stringify(trades) );

            let returnedTrades: HistoricalAccountTrade[] = [];

            trades.forEach(function(trade)
            {
                const tradeDate = new Date(trade.timestamp * 1000);

                const historicalAccountTrade = new HistoricalAccountTrade({
                    exchangeName: self.name,
                    symbol: symbol,
                    side: trade.type.toLowerCase(),
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: tradeDate,
                    feeAmountBN: new BigNumber(trade.fee_amount).abs(),
                    feeCurrency: trade.fee_currency,
                    orderId: trade.order_id,
                    tradeId: trade.tid
                });

                returnedTrades.push(historicalAccountTrade);
            });

            logger.debug('%s %s historical trades on my account', functionName, returnedTrades.length );

            callback(null, returnedTrades);
        });
    }

    getCurrencyMovementsBetweenDates(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate?: Date, currency?: string, type?: MovementType): void
    {
        const functionName = "Bitfinex.getCurrencyMovementsBetweenDates()",
            self = this,
            movements:Movement[] = [];

        logger.trace('%s about to get movements between %s (%s) and %s (%s)', functionName,
            fromDate.toString(), fromDate.getTime(),
            toDate.toString(), fromDate.getTime());

        let currencies: string[];
        if (currency) {
            currencies = [currency];
        }
        else {
            currencies = _.union(this.fixedCurrencies, this.variableCurrencies);
        }

        if (!toDate) { toDate = new Date(); }

        let returnedMovements: Movement[] = [];

        async.eachSeries(
            currencies,

            function(currency, callback)
            {
                self.getCurrencyMovementsBetweenDatesForCurrency(function(err, movements)
                {
                    if (err) return callback(err);

                    returnedMovements = returnedMovements.concat(movements);

                    callback(null);

                }, fromDate, toDate, currency, type)
            },
            function(err)
            {
                callback(err, returnedMovements)
            }
        );
    }

    getCurrencyMovementsBetweenDatesForCurrency(callback: (err: Error, movements?: Movement[])=> void, fromDate: Date, toDate: Date, currency: string, type?: MovementType): void
    {
        const functionName = "Bitfinex.getCurrencyMovementsBetweenDatesForCurrency()",
            self = this,
            movements:Movement[] = [];

        logger.trace('%s about to get movements between %s (%s) and %s (%s) for currency %s', functionName,
            fromDate.toString(), fromDate.getTime(),
            toDate.toString(), fromDate.getTime(),
            currency);

        this.client.movements(currency, fromDate.getTime() / 1000, toDate.getTime() / 1000, this.maxMovements, function(err, movements)
        {
            if (err) return self.errorHandler(err, false, 'getCurrencyMovementsBetweenDatesForCurrency', self.getCurrencyMovementsBetweenDatesForCurrency, callback, fromDate, toDate, currency, type);

            self.failures = 0;

            logger.trace('%s data returned: %s', functionName, JSON.stringify(movements) );

            let returnedMovements: Movement[] = [];

            movements.forEach(function(movement)
            {
                const newMovement = new Movement({
                    exchangeName: self.name,
                    currency: movement.currency,
                    type: movement.side.toLowerCase(),
                    amount: movement.amount,
                    timestamp: new Date(movement.timestamp * 1000)
                });

                // add movement if type not defined or type of returned movement matches what's specified
                if (!type || type == newMovement.type)
                {
                    returnedMovements.push(newMovement);
                }

            });

            logger.debug('%s %s movements returned for currency %s', functionName,
                returnedMovements.length, currency);

            callback(null, returnedMovements);
        });
    }

    setTimeout(timeout: number)
    {
        this.client.timeout = timeout;
    }
};


