/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var moment = require('moment');
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
// import my Interfaces
var exchange_1 = require("../models/exchange");
var market_1 = require("../models/market");
var ticker_1 = require("../models/ticker");
var balance_1 = require("../models/balance");
var account_1 = require("../models/account");
var orderBook_1 = require("../models/orderBook");
var order_1 = require("../models/order");
var historicalAccountTrade_1 = require("../models/historicalAccountTrade");
var logger = require('config-logger'), VError = require('verror'), BTCMarkets = require("btc-markets"), config = require('config');
var numberConverter = 100000000; // 100 million
var numberConverterBN = new BigNumber(numberConverter); // 100 million
var defaultExchangeSettings = {
    name: 'BTCMarkets',
    fixedCurrencies: ['BTC', 'LTC'],
    variableCurrencies: ['AUD'],
    commissions: 0.0011,
    currencyRounding: {
        BTC: 8,
        LTC: 8,
        AUD: 2 },
    priceRounding: {
        BTCAUD: 2,
        LTCAUD: 2,
        LTCBTC: 8
    },
    defaultMinAmount: 0.01,
    publicPollingInterval: 15000,
    privatePollingInterval: 5000,
    maxFailures: 10,
    feeStructure: 'variableCurrency' // all fees are in AUD currency. eg buy BTC will have a AUD fee
};
var BTCMarketsExchange = (function (_super) {
    __extends(BTCMarketsExchange, _super);
    function BTCMarketsExchange(exchangeConfig) {
        var functionName = 'BTCMarketsExchange.constructor()';
        this.limit = 50;
        // override the default exchange settings from the config
        var exchangeSettings = _.extend(defaultExchangeSettings, exchangeConfig);
        //Call the constructor of inherited Exchange object
        _super.call(this, exchangeSettings);
        if (exchangeConfig.testInstance) {
            this.server = 'http://dev.api.btcmarkets.net';
            logger.info('%s using test server %s', functionName, this.server);
        }
        logger.trace('%s initialising BTC Markets client', functionName);
        this.client = new BTCMarkets(exchangeSettings.APIkey, exchangeSettings.APIsecret, this.server // undefined defaults to production: https://api.btcmarkets.net
        );
    }
    BTCMarketsExchange.prototype.getCurrencyMovementsBetweenDates = function (callback, fromDate, toDate, currency, type) {
        var error = new VError('Exchange.getCurrencyMovementsBetweenDates() on the %s exchange has not been implemented', this.name);
        logger.error(error.stack);
        callback(error);
    };
    BTCMarketsExchange.prototype.getTicker = function (symbol, callback) {
        var functionName = 'BTCMarkets.getTicker()', self = this;
        logger.trace('%s about to get ticker for symbol %s', functionName, symbol);
        var instrument = market_1.default.getFixedCurrency(symbol), currency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s instrument %s, currency %s', functionName, instrument, currency);
        this.client.getTick(instrument, currency, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getTicker', self.getTicker, callback, symbol);
            self.failures = 0;
            var newTicker = new ticker_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bid: Number(json.bestBid),
                ask: Number(json.bestAsk),
                last: Number(json.lastPrice),
                timestamp: new Date(json.timestamp * 1000)
            });
            logger.debug('%s %s bid %d, ask %d, last %s, timestamp %s', functionName, newTicker.symbol, newTicker.bid, newTicker.ask, newTicker.last, moment(newTicker.timestamp).format('D MMM YY H:mm:ss'));
            self.markets[symbol].addTicker(newTicker);
            callback(null, newTicker);
        });
    };
    BTCMarketsExchange.prototype.getOrderBook = function (symbol, callback) {
        var functionName = 'BTCMarkets.getOrderBook()', self = this;
        logger.trace('%s about to get order book for market %s', functionName, symbol);
        var instrument = market_1.default.getFixedCurrency(symbol), currency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s instrument %s, currency %s', functionName, instrument, currency);
        this.client.getOrderBook(instrument, currency, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getOrderBook', self.getOrderBook, callback, symbol);
            self.failures = 0;
            logger.trace('%s data: %s', functionName, JSON.stringify(json));
            var bids = BTCMarketsExchange.consolidateOrders(json.bids);
            var asks = BTCMarketsExchange.consolidateOrders(json.asks);
            var newOrderBook = new orderBook_1.default({
                exchangeName: self.name,
                symbol: symbol,
                bids: bids,
                asks: asks,
                timestamp: new Date(json.timestamp * 1000),
                minAmountToIgnore: self.minOrderBookAmountToIgnore
            });
            // check that there are bids and asks
            if (newOrderBook.bids.length > 0 && newOrderBook.asks.length > 0) {
                logger.debug('%s %s[%s] best bid price %d, amount %d of %s bids. best ask price %d, amount %d of %s asks. %s', functionName, self.name, symbol, newOrderBook.bids[0].price, newOrderBook.bids[0].amount, newOrderBook.bids.length, newOrderBook.asks[0].price, newOrderBook.asks[0].amount, newOrderBook.asks.length, moment(newOrderBook.timestamp).format('D MMM YY hh:mm:ss'));
            }
            else {
                logger.info('%s %s[%s] did not have bids or asks. %s bids, %s asks', functionName, self.name, symbol, newOrderBook.bids.length, newOrderBook.asks.length);
            }
            self.markets[symbol].addOrderBook(newOrderBook);
            callback(null, newOrderBook);
        });
    };
    // consolidated orders with the same prices. That is, combines them and adds the amounts together
    BTCMarketsExchange.consolidateOrders = function (exchangeOrders) {
        var previousOrder = [0, 0], returnOrders = [];
        exchangeOrders.forEach(function (order, index) {
            // if current order price = previous order price
            if (previousOrder[0] === order[0]) {
                // add order amounts together and cast to Number format
                previousOrder[1] = new BigNumber(previousOrder[1]).
                    plus(order[1]).
                    toNumber();
            }
            else {
                // if not processing the first order in the order book
                if (index > 0) {
                    // add previous order to list of bids
                    returnOrders.push(previousOrder);
                }
                previousOrder = order;
            }
        });
        if (exchangeOrders.length > 0) {
            // add the last order from the previous loop
            returnOrders.push(previousOrder);
        }
        return returnOrders;
    };
    BTCMarketsExchange.prototype.getMarketTrades = function (symbol, params, callback) {
        var functionName = 'BTCMarkets.getMarketTrades()', self = this;
        logger.trace('%s about to get historical trades for market %s', functionName, symbol);
        var instrument = market_1.default.getFixedCurrency(symbol), currency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s instrument %s, currency %s', functionName, instrument, currency);
        this.client.getTrades(instrument, currency, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getMarketTrades', self.getMarketTrades, callback, symbol);
            self.failures = 0;
            var trades = [];
            json.forEach(function (trade) {
                trades.push({
                    exchangeName: self.name,
                    symbol: symbol,
                    priceBN: new BigNumber(trade.price),
                    quantityBN: new BigNumber(trade.amount),
                    timestamp: new Date(trade.date * 1000)
                });
            });
            logger.debug('%s %s historical trades returned', functionName, trades.length);
            callback(null, trades);
        });
    };
    BTCMarketsExchange.prototype.getAccountBalances = function (callback) {
        var functionName = 'BTCMarkets.getAccountBalances()', self = this;
        logger.trace('%s about to get account balances', functionName);
        this.client.getAccountBalances(function (err, balancesArray) {
            if (err)
                return self.errorHandler(err, true, 'getAccountBalances', self.getAccountBalances, callback);
            self.failures = 0;
            logger.trace('%s accounts returned from the %s exchange:', functionName, self.name);
            logger.trace(balancesArray);
            var returnedAccount = new account_1.default([], self.name, self.currencyRounding);
            balancesArray.forEach(function (account) {
                var total = new BigNumber(account.balance).
                    div(numberConverter).
                    round(self.currencyRounding[account.currency], BigNumber.ROUND_DOWN).
                    toString();
                var available = new BigNumber(total).
                    minus(new BigNumber(account.pendingFunds).
                    div(numberConverter)).
                    round(self.currencyRounding[account.currency], BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s %s balances: total %s, available %s = balance %s - pending balance %s', functionName, account.currency, total, available, account.balance, account.pendingFunds);
                returnedAccount.setBalance(new balance_1.default({
                    exchangeName: self.name,
                    currency: account.currency,
                    totalBalance: total,
                    availableBalance: available
                }));
            });
            // set the balances on the exchange
            self.account = returnedAccount;
            callback(null, returnedAccount);
        });
    };
    // gets my pending orders for a market with symbol
    BTCMarketsExchange.prototype.getPendingOrdersForSymbol = function (symbol, callback) {
        var functionName = 'BTCMarkets.getPendingOrdersForSymbol()', self = this;
        logger.trace('%s about to get pending orders', functionName);
        var orders = [];
        var sinceOrderId = 1;
        var fixedCurrency = market_1.default.getFixedCurrency(symbol);
        var variableCurrency = market_1.default.getVariableCurrency(symbol);
        self.client.getOpenOrders(fixedCurrency, variableCurrency, 20, sinceOrderId, function (err, json) {
            if (err)
                return self.errorHandler(err, true, 'getPendingOrdersForSymbol', self.getPendingOrdersForSymbol, callback, symbol);
            self.failures = 0;
            logger.debug('%s %s pending orders returned', self.name, json.orders.length);
            json.orders.forEach(function (order) {
                //convert from BTCMarkets side
                var side = BTCMarketsExchange.convertSide(order.orderSide);
                var symbol = order.instrument + order.currency;
                var amountTraded = new BigNumber(order.volume).
                    minus(order.openVolume).
                    div(numberConverter).
                    round(self.currencyRounding[order.instrument], BigNumber.ROUND_DOWN).
                    toString();
                var amountLastPartial = '0';
                if (order.trades && order.trades.length > 0) {
                    var lastTrade = _.last(order.trades);
                    amountLastPartial = new BigNumber(lastTrade.volume).div(numberConverter).toString();
                    logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName, order.id, order.trades.length, lastTrade.price / numberConverter, amountLastPartial, moment(lastTrade.creationTime).format('D MMM YY H:mm:ss'));
                }
                var exchangeOrder = new order_1.default({
                    exchangeId: order.id.toString(),
                    exchangeName: self.name,
                    symbol: symbol,
                    state: 'pending',
                    side: side,
                    type: order.ordertype.toLowerCase(),
                    amount: new BigNumber(order.volume).div(numberConverter).toString(),
                    amountTraded: amountTraded,
                    amountRemaining: new BigNumber(order.openVolume).div(numberConverter).toString(),
                    amountLastPartial: amountLastPartial,
                    numberPartialFills: order.trades.length,
                    price: new BigNumber(order.price).div(numberConverter).toString(),
                    timestamp: new Date(order.creationTime * 1000),
                    tag: order.clientRequestId
                });
                logger.debug('pending order:');
                exchangeOrder.log('debug');
                orders.push(exchangeOrder);
            });
            logger.trace('%s returning %s pending orders via callback', functionName, orders.length);
            callback(null, orders);
        });
    };
    // creates a buy or sell order on an exchange
    BTCMarketsExchange.prototype.addOrder = function (newOrder, callback) {
        var functionName = 'BTCMarkets.addOrder()', self = this;
        logger.trace('%s about to add the following new order:', functionName);
        newOrder.log('trace');
        // validate the side parameter
        if (newOrder.side !== 'sell' && newOrder.side !== 'buy') {
            var error = new VError('%s add trade failed as side %s is not sell or buy', functionName, newOrder.side);
            logger.error(error.stack);
            return callback(error);
        }
        // validate the exchangeName and symbol on the newTrade
        if (newOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, newOrder.name, this.name);
            logger.error(error.stack);
            return callback(error);
        }
        var orderSide = BTCMarketsExchange.convertSide(newOrder.side), orderType = newOrder.type.charAt(0).toUpperCase() + newOrder.type.slice(1);
        var convertedPrice = 0; // default to 0 for market trades
        if (newOrder.type === 'limit') {
            convertedPrice = new BigNumber(newOrder.price).
                times(numberConverter).
                round(0).
                toNumber();
            logger.debug('%s converted price %s = price %s * number converter %s', functionName, convertedPrice, newOrder.price, numberConverter);
        }
        var convertedAmount = new BigNumber(newOrder.amountRemaining).
            times(numberConverter).
            round(0).
            toNumber();
        logger.debug('%s converted amount %s = remaining amount %s * number converter %s', functionName, convertedAmount, newOrder.amountRemaining, numberConverter);
        this.client.createOrder(newOrder.fixedCurrency, newOrder.variableCurrency, convertedPrice, convertedAmount, orderSide, orderType, newOrder.tag, function (err, json) {
            var functionName = 'BTCMarkets.addOrder.createOrder()#' + newOrder.type;
            if (err) {
                // no need to retry the API call with the following errors.
                if (err.name === 'Insufficient funds.') {
                    var error = new VError(err, '%s not enough funds on the %s exchange to add %s order @ %s with remaining amount %s, sell amount remaining %s %s, fee %s %s and tag %s', functionName, self.name, newOrder.side, newOrder.price, newOrder.amountRemaining, newOrder.sell.amountRemaining, newOrder.sell.currency, newOrder.fee.amountRemaining, newOrder.fee.currency, newOrder.tag);
                    error.name = exchange_1.default.NOT_ENOUGH_FUNDS;
                    logger.error(error.stack);
                    return callback(error);
                }
                else if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.addOrderTimeout(err, newOrder, callback);
                }
                return self.errorHandler(err, true, 'addOrder', self.addOrder, callback, newOrder);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            var data = {
                exchangeId: json.id.toString(),
                timestamp: new Date()
            };
            self.addOrderSuccess(newOrder, callback, data);
        });
    };
    // gets the order details
    BTCMarketsExchange.prototype.getOrder = function (exchangeId, callback) {
        var functionName = 'BTCMarketsExchange.getOrder()', self = this;
        if (!exchangeId || typeof exchangeId != 'string') {
            var error = new VError('%s exchangeId %s is undefined or is not a string', functionName, exchangeId);
            logger.error(error.message);
            return callback(error);
        }
        logger.trace('%s about to get order with id %s', functionName, exchangeId);
        self.client.getOrderDetail([parseInt(exchangeId)], function getOrderDetail(err, json) {
            if (err) {
                return self.errorHandler(err, true, 'getOrder', self.getOrder, callback, exchangeId);
            }
            else if (json.orders.length == 0) {
                var error = new VError('%s could not find order with id %s on the %s exchange', functionName, exchangeId, self.name);
                error.name = exchange_1.default.ID_NOT_FOUND;
                logger.error(error.stack);
                return callback(error);
            }
            else if (json.orders[0].ordertype == 'Market' && json.orders[0].orders == 0) {
                var error = new VError('%s market order with id %s has not yet been filled.', functionName, exchangeId);
                return self.errorHandler(error, true, 'getOrder', self.getOrder, callback, exchangeId);
            }
            self.failures = 0;
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            var order = json.orders[0];
            var symbol = order.instrument + order.currency;
            var price = self.calcPrice(order, symbol);
            //convert from BTCMarkets side
            var side = BTCMarketsExchange.convertSide(order.orderSide);
            var amountTraded = new BigNumber(order.volume).
                minus(order.openVolume).
                div(numberConverter).
                round(self.currencyRounding[order.instrument], BigNumber.ROUND_DOWN).
                toString();
            var amountLastPartial = '0';
            if (order.trades && order.trades.length > 0) {
                var lastTrade = _.last(order.trades);
                amountLastPartial = new BigNumber(lastTrade.volume).div(numberConverter).toString();
                logger.debug("%s order with id %s has been partially filled by %s trades. Last trade had price %s, amount %s at %s", functionName, order.id, order.trades.length, lastTrade.price / numberConverter, amountLastPartial, moment(lastTrade.creationTime).format('D MMM YY H:mm:ss'));
            }
            var state = BTCMarketsExchange.convertStatus(order.status);
            var exchangeOrder = new order_1.default({
                exchangeId: order.id.toString(),
                exchangeName: self.name,
                symbol: symbol,
                state: state,
                side: side,
                type: order.ordertype.toLowerCase(),
                amount: new BigNumber(order.volume).div(numberConverter).toString(),
                amountTraded: amountTraded,
                amountRemaining: new BigNumber(order.openVolume).div(numberConverter).toString(),
                amountLastPartial: amountLastPartial,
                numberPartialFills: order.trades.length,
                price: price,
                timestamp: new Date(order.creationTime * 1000),
                tag: order.clientRequestId
            });
            logger.info('%s successfully got order:', functionName);
            exchangeOrder.log('info');
            callback(null, exchangeOrder);
        });
    };
    BTCMarketsExchange.prototype.calcPrice = function (order, symbol) {
        var functionName = 'BTCMarkets.calcPrice()', self = this;
        if (order.ordertype == 'Limit') {
            return new BigNumber(order.price).
                div(numberConverter).
                toString();
        }
        else {
            if (order.trades.length == 0) {
                return;
            }
            var sumTradePriceTimesVolumesBN = new BigNumber(0), sumTradeVolumesBN = new BigNumber(0);
            order.trades.forEach(function (trade) {
                var tradeVolumeBN = new BigNumber(trade.volume).
                    div(numberConverter);
                sumTradePriceTimesVolumesBN = sumTradePriceTimesVolumesBN.
                    plus(new BigNumber(trade.price).
                    div(numberConverter).
                    times(tradeVolumeBN));
                sumTradeVolumesBN = sumTradeVolumesBN.
                    plus(tradeVolumeBN);
            });
            var vwap = sumTradePriceTimesVolumesBN.
                div(sumTradeVolumesBN).
                round(self.priceRounding[symbol], BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s volume weighted average price %s = sum trade prices %s / sum trade volumes %s', functionName, vwap, sumTradePriceTimesVolumesBN.toString(), sumTradeVolumesBN.toString());
            return vwap;
        }
    };
    // cancels an pending order
    BTCMarketsExchange.prototype.cancelOrder = function (cancelOrder, callback) {
        var functionName = 'BTCMarkets.cancelOrder()', self = this;
        logger.trace('%s about to cancel order with exchange id %s', functionName, cancelOrder.exchangeId);
        if (cancelOrder.exchangeName !== this.name) {
            var error = new VError('%s order is for exchange %s and not this exchange %s', functionName, cancelOrder.exchangeName, this.name);
            logger.error(error.stack);
            throw error;
        }
        this.client.cancelOrders([parseInt(cancelOrder.exchangeId)], function (err, json) {
            if (err) {
                if (err.name === 'ETIMEDOUT' || err.name === 'ESOCKETTIMEDOUT') {
                    return self.cancelOrderTimeout(err, cancelOrder, callback);
                }
                return self.errorHandler(err, true, 'cancelOrder', self.cancelOrder, callback, cancelOrder);
            }
            else if (json && json.responses && json.responses[0] &&
                !json.responses[0].success) {
                var orderDesc = util_1.format('%s order with id %s, tag %s, price %s and amount remaining %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining);
                if (json.responses[0].errorMessage === 'order is already cancelled.') {
                    var error_1 = new VError('%s could not cancel %s. BTC Markets error message: %s', functionName, orderDesc, json.responses[0].errorMessage);
                    // check the state of the order that could not be cancelled and then call the callback with the appropriate error. eg ALREADY_FILLED or ALREADY_CANCELLED
                    return self.cancelOrderInvalidState(err, cancelOrder, callback);
                }
                var error = new VError('%s could not cancel %s order with id %s and tag %s, price %s and amount remaining %s. Error message: %s', functionName, cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag, cancelOrder.price, cancelOrder.amountRemaining, json.responses[0].errorMessage);
                error.name = json.responses[0].errorMessage;
                logger.error(error.stack);
                return callback(error);
            }
            logger.trace('%s json data: %s', functionName, JSON.stringify(json));
            self.cancelOrderSuccess(cancelOrder, callback);
        });
    };
    BTCMarketsExchange.prototype.getAccountTradesBetweenDates = function (callback, fromDate, toDate) {
        if (toDate === void 0) { toDate = new Date(); }
        var functionName = 'BTCMarkets.getAccountTradesBetweenDates()', self = this;
        var since = moment(fromDate).unix();
        // TODO get both BTCAUD and LTCAUD rather than just BTCUSD
        var symbol = 'BTCAUD';
        var instrument = market_1.default.getFixedCurrency(symbol);
        var currency = market_1.default.getVariableCurrency(symbol);
        logger.trace('%s about to get %s account trades from %s (%s) to %s', functionName, symbol, moment(fromDate).format('DD MMM YYYY HH:mm:ss'), since, moment(toDate).format('DD MMM YYYY HH:mm:ss'));
        // getOrderHistory(instrument, currency, limit, since, callback)
        self.client.getOrderHistory(currency, instrument, this.limit, 1, function (err, json) {
            if (err)
                return self.errorHandler(err, false, 'getAccountTradesBetweenDates', self.getAccountTradesBetweenDates, callback, fromDate, toDate);
            self.failures = 0;
            var trades = [];
            json.orders.forEach(function (order) {
                if (!order.orders) {
                    return;
                }
                // is the returned trade type a key in the typeMap
                order.orders.forEach(function (trade) {
                    var historicalAccountTrade = new historicalAccountTrade_1.default({
                        orderId: order.id.toString(),
                        tradeId: trade.id.toString(),
                        priceBN: new BigNumber(trade.price).div(numberConverterBN),
                        quantityBN: new BigNumber(trade.volume).div(numberConverterBN),
                        side: BTCMarketsExchange.convertSide(order.orderSide),
                        exchangeName: self.name,
                        symbol: symbol,
                        timestamp: new Date(trade.creationTime * 1000)
                    });
                    trades.push(historicalAccountTrade);
                });
            });
            logger.debug('%s %s my trades returned', functionName, trades.length);
            callback(null, trades);
        });
    };
    BTCMarketsExchange.prototype.setTimeout = function (timeout) {
        this.client.timeout = timeout;
    };
    // converts exchange order status to valid Exchange.state values
    BTCMarketsExchange.convertStatus = function (status) {
        if (status === "Placed" || status === "Partially Matched")
            return 'pending';
        else if (status === "Fully Matched")
            return 'filled';
        else if (status === "Cancelled" || status === "Partially Cancelled")
            return 'cancelled';
        else
            return 'unknown';
    };
    BTCMarketsExchange.convertSide = function (side) {
        var functionName = 'BTCMarkets.convertSide()';
        var convertedSide;
        if (side === 'Bid') {
            convertedSide = 'buy';
        }
        else if (side === 'Ask') {
            convertedSide = 'sell';
        }
        else if (side === 'buy') {
            convertedSide = 'Bid';
        }
        else if (side === 'sell') {
            convertedSide = 'Ask';
        }
        else {
            var error = new VError('%s side %s is not buy, sell, bid or ask', functionName, side);
            logger.error(error.stack);
            throw error;
        }
        logger.trace('%s side %s convert to %s', functionName, side, convertedSide);
        return convertedSide;
    };
    return BTCMarketsExchange;
})(exchange_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BTCMarketsExchange;
