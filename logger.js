var winston = require('winston'),
    os = require('os');

var CONFIG = require('config');

// default the log level if it's not set in the config file or NODE_CONFIG environment variable. eg NODE_CONFIG='{"logLevel":"debug"}'
var logLevel = CONFIG.logLevel || "warn";

var myCustomLevels = {
    levels: {
        trace: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4
    },
    colors: {
        trace: 'white',
        debug: 'blue',
        info: 'yellow',
        warn: 'orange',
        error: 'red'
    }
};

winston.addColors(myCustomLevels.colors);

var logger = new (winston.Logger)({
    levels: myCustomLevels.levels,
    transports: [
      new (winston.transports.Console)({level: logLevel, colorize: true})
    ]
});

module.exports=logger;