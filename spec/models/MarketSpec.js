/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
var market_1 = require('../../models/market');
var order_1 = require('../../models/order');
var ticker_1 = require('../../models/ticker');
var BigNumber = require('bignumber.js');
describe("Market:", function () {
    var testExchange1, testExchange2, testMarket;
    beforeEach(function () {
        // Define test exchange data rather than using live exchange config that can change
        testExchange1 = new TestExchange_1.default({
            name: 'testExchange1',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: {
                LTC: { maker: 0.1, taker: 0.01 },
                BTC: { maker: 0.02, taker: 0.2 },
                AUD: { maker: 0.01, taker: 0.1 },
                USD: { maker: 0.06, taker: 0.6 },
                EUR: { maker: 0.03, taker: 0.06 }
            },
            defaultPriceRounding: 2,
            defaultMinAmount: 0.1,
            confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
        });
        testExchange2 = new TestExchange_1.default({
            name: 'testExchange2',
            fixedCurrencies: ["BTC"],
            variableCurrencies: ["EUR", "USD"],
            commissions: {
                "BTC": { maker: 0.01, taker: 0.1 },
                "EUR": { maker: 0.03, taker: 0.3 },
                "USD": { maker: 0.04, taker: 0.4 },
            },
            defaultPriceRounding: 3,
            defaultMinAmount: 0.001,
            confirmationTime: 20
        });
        // override the global exchanges variable for testing purposes
        exchange_1.default.exchanges = {
            'testExchange1': testExchange1,
            'testExchange2': testExchange2 };
    });
    describe("Instantiate a new market object with no historical tickers or trades", function () {
        beforeEach(function () {
            testMarket = new market_1.default({
                exchangeName: 'testExchange2',
                symbol: 'BTCEUR'
            });
        });
        it('properties are set', function () {
            expect(testMarket).toBeDefined();
            expect(testMarket.exchangeName).toEqual('testExchange2');
            expect(testMarket.fixedCurrency).toEqual('BTC');
            expect(testMarket.variableCurrency).toEqual('EUR');
        });
        it('Tickers initiated', function () {
            expect(testMarket.latestTicker).toBeUndefined();
            expect(testMarket.tickers).toBeDefined();
            expect(testMarket.tickers.length).toEqual(0);
        });
        it('Trades initiated', function () {
            expect(testMarket.orders).toBeDefined();
            expect(testMarket.orders.length).toEqual(0);
        });
        it('Helper properties are set', function () {
            //expect(testMarket.exchange).toBeDefined();
            //expect(testMarket.exchange.name).toEqual('testExchange2');
            expect(testMarket.symbol).toEqual('BTCEUR');
        });
    });
    describe("get commission matrix", function () {
        describe("for buyCurrency exchange", function () {
            beforeEach(function () {
                testExchange1.feeStructure = "buyCurrency";
                testMarket = new market_1.default({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD'
                });
            });
            it("maker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('maker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(-0.02));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(-0.01));
            });
            it("taker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('taker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(-0.2));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(-0.1));
            });
        });
        describe("for sellCurrency exchange", function () {
            beforeEach(function () {
                testExchange1.feeStructure = "sellCurrency";
                testMarket = new market_1.default({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD'
                });
            });
            it("maker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('maker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0.02));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0.01));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(0));
            });
            it("taker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('taker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0.2));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0.1));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(0));
            });
        });
        describe("for fixedCurrency exchange", function () {
            beforeEach(function () {
                testExchange1.feeStructure = "fixedCurrency";
                testMarket = new market_1.default({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD'
                });
            });
            it("maker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('maker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0.02));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(-0.02));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(0));
            });
            it("taker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('taker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0.2));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(-0.2));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(0));
            });
        });
        describe("for variableCurrency exchange", function () {
            beforeEach(function () {
                testExchange1.feeStructure = "variableCurrency";
                testMarket = new market_1.default({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCAUD'
                });
            });
            it("maker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('maker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0.01));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(-0.01));
            });
            it("taker fees", function () {
                var commissionMatrix = testMarket.getCommissionMatrix('taker');
                expect(commissionMatrix.sell.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.sell.AUD).toEqual(BigNumber(0.1));
                expect(commissionMatrix.buy.BTC).toEqual(BigNumber(0));
                expect(commissionMatrix.buy.AUD).toEqual(BigNumber(-0.1));
            });
        });
    });
    describe("Market listeners:", function () {
        var testMarket = new market_1.default({
            exchangeName: 'testExchange1',
            symbol: 'BTCEUR'
        });
        describe("Add price to a market to emit a new price event", function () {
            var testTicker = new ticker_1.default({
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                bid: 123.4567,
                ask: 987.6543,
                last: 345.43
            });
            // function to test the price event was fired with the test price
            var assertFunction = function (ticker) {
                expect(ticker).toEqual(testTicker);
            };
            // Create a listener for ticker events that are saved to the database
            //Ticker.saveTickerListener(testMarket, assertFunction);
            // Create a generic listener for ticker events
            ticker_1.default.tickerListener(testMarket, assertFunction);
            it("listener receives the new price from the emitted event", function () {
                // Add a price to the market being listened to to fire the event
                testMarket.addTicker(testTicker);
                // expect is in the assertFunction above
            });
        });
        describe("Add order to a market to emit a new order event", function () {
            var OrderListener = function (market, callback) {
                market.on('order', function (order) {
                    console.log('order event fired with sell amount %d and buy amount %d on the %s market on the %s exchange', order.sell.amount, order.buy.amount, order.symbol, order.exchangeName);
                    callback(order);
                });
            };
            var testOrder = new order_1.default({ exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'buy',
                state: 'pending',
                amount: '0.2',
                amountRemaining: '0.2',
                price: '2000'
            });
            // function to test the price event was fired with the test price
            var assertFunction = function (trade) {
                expect(trade).toEqual(testOrder);
            };
            // Create an object listening for price events 
            var tradeListener = OrderListener(testMarket, assertFunction);
            it("listener receives the new price from the emitted event", function () {
                // Add a price to the market being listened to to fire the event
                testMarket.addOrder(testOrder);
                // expect is in the assertFunction above
            });
        });
    });
});
