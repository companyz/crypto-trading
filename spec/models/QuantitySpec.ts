/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />

import Quantity from '../../models/quantity';

describe("Quantity: ", function() {

	it("Initialising", function()
    {
		var testQuantity = new Quantity({currency: 'AUD', amount:100});

		expect(testQuantity.currency).toEqual('AUD');
		expect(testQuantity.amount).toEqual(100);
	});
});