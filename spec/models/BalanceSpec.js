/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
var balance_1 = require('../../models/balance');
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
// Third party libraries
var BigNumber = require('bignumber.js');
describe("Balance: ", function () {
    // Define test exchange data rather than using live exchange config that can change
    var testExchange1 = new TestExchange_1.default({
        name: 'testExchange1',
        fixedCurrencies: ["LTC", "BTC"],
        variableCurrencies: ["AUD", "EUR", "USD"],
        commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
        defaultPriceRounding: 2,
        defaultMinAmount: 0.1,
        confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
    });
    var testExchange2 = new TestExchange_1.default({
        name: 'testExchange2',
        fixedCurrencies: ["BTC"],
        variableCurrencies: ["EUR", "USD"],
        commissions: { BTC: 0.01, AUD: 0.03, USD: 0.04 },
        defaultPriceRounding: 3,
        defaultMinAmount: 0.001,
        confirmationTime: 20
    });
    // override the static exchanges on the Exchange class
    exchange_1.default.exchanges = {
        'testExchange1': testExchange1,
        'testExchange2': testExchange2 };
    var testBTCBalance = new balance_1.default({
        exchangeName: 'testExchange1',
        currency: 'BTC',
        totalBalance: '5.12345678',
        availableBalance: '4.12345678'
    });
    var testUSDBalance = new balance_1.default({
        exchangeName: 'testExchange1',
        currency: 'USD',
        totalBalance: '55.12',
        availableBalance: '40.99'
    });
    it("Initialising", function () {
        expect(testBTCBalance.exchangeName).toEqual('testExchange1');
        expect(testBTCBalance.currency).toEqual('BTC');
        expect(testBTCBalance.totalBalance).toEqual('5.12345678');
        expect(testBTCBalance.availableBalance).toEqual('4.12345678');
        expect(testBTCBalance.totalBalanceBN).toEqual(BigNumber('5.12345678'));
        expect(testBTCBalance.availableBalanceBN).toEqual(BigNumber('4.12345678'));
        expect(testBTCBalance.exchange.name).toEqual('testExchange1');
    });
    it("Clone", function () {
        var clonedBalance = testBTCBalance.clone;
        clonedBalance.totalBalance = '10.12345678';
        clonedBalance.availableBalance = '6.12345678';
        expect(clonedBalance.exchangeName).toEqual('testExchange1');
        expect(clonedBalance.currency).toEqual('BTC');
        expect(clonedBalance.totalBalance).toEqual('10.12345678');
        expect(clonedBalance.availableBalance).toEqual('6.12345678');
        expect(clonedBalance.totalBalanceBN).toEqual(BigNumber('10.12345678'));
        expect(clonedBalance.availableBalanceBN).toEqual(BigNumber('6.12345678'));
        expect(clonedBalance.exchange.name).toEqual('testExchange1');
        // check that the original values did not get changes
        expect(testBTCBalance.totalBalance).toEqual('5.12345678');
        expect(testBTCBalance.availableBalance).toEqual('4.12345678');
    });
});
