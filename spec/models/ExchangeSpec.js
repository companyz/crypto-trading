/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
var TestExchange_1 = require('../../exchanges/TestExchange');
describe("Exchange: ", function () {
    it("Initialising: ", function () {
        var testExchange = new TestExchange_1.default({
            name: 'testExchange1',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: { LTC: 0.1, BTC: 0.02, AUD: 0.01, USD: 0.06, EUR: 0.03 },
            currencyRounding: { BTC: 5, AUD: 2, LTC: 7 },
            defaultPriceRounding: 3,
            defaultMinAmount: 0.001,
            confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
        });
        expect(testExchange instanceof TestExchange_1.default).toBeTruthy();
        expect(testExchange.name).toEqual('testExchange1');
        expect(testExchange.fixedCurrencies[0]).toEqual('LTC');
        expect(testExchange.fixedCurrencies[1]).toEqual('BTC');
        expect(testExchange.variableCurrencies[0]).toEqual('AUD');
        expect(testExchange.variableCurrencies[1]).toEqual('EUR');
        expect(testExchange.variableCurrencies[2]).toEqual('USD');
        expect(testExchange.commissions["LTC"].maker).toEqual(0.1);
        expect(testExchange.commissions["LTC"].taker).toEqual(0.1);
        expect(testExchange.commissions["BTC"].maker).toEqual(0.02);
        expect(testExchange.commissions["BTC"].taker).toEqual(0.02);
        expect(testExchange.currencyRounding["BTC"]).toEqual(5);
        expect(testExchange.currencyRounding["AUD"]).toEqual(2);
        expect(testExchange.currencyRounding["LTC"]).toEqual(7);
        expect(testExchange.confirmationTime).toEqual(1);
        // balances initialised to zero
        expect(testExchange.account).toBeDefined();
        expect(testExchange.account.balances['BTC']).toBeDefined();
        expect(testExchange.account.balances['BTC'].exchangeName).toEqual('testExchange1');
        expect(testExchange.account.balances['BTC'].currency).toEqual('BTC');
        expect(testExchange.account.balances['BTC'].totalBalance).toEqual('0');
        expect(testExchange.account.balances['BTC'].availableBalance).toEqual('0');
    });
    describe("Initialising with commissions with type", function () {
        it("number", function () {
            var testExchange = new TestExchange_1.default({
                name: 'testExchange1',
                fixedCurrencies: ["LTC", "BTC"],
                variableCurrencies: ["BTC", "USD"],
                commissions: 0.0015,
                defaultPriceRounding: 4,
                defaultMinAmount: 0.01,
                confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
            });
            expect(testExchange.commissions["LTC"].maker).toEqual(0.0015);
            expect(testExchange.commissions["LTC"].taker).toEqual(0.0015);
            expect(testExchange.commissions["BTC"].maker).toEqual(0.0015);
            expect(testExchange.commissions["BTC"].taker).toEqual(0.0015);
            expect(testExchange.commissions["USD"].maker).toEqual(0.0015);
            expect(testExchange.commissions["USD"].taker).toEqual(0.0015);
            expect(testExchange.confirmationTime).toEqual(1);
        });
        it("MarkerTaker", function () {
            var testExchange = new TestExchange_1.default({
                name: 'testExchange1',
                fixedCurrencies: ["LTC", "BTC"],
                variableCurrencies: ["BTC", "USD"],
                commissions: {
                    maker: 0.0015,
                    taker: 0.0022
                },
                defaultPriceRounding: 4,
                defaultMinAmount: 0.01,
                confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
            });
            expect(testExchange.commissions["LTC"].maker).toEqual(0.0015);
            expect(testExchange.commissions["LTC"].taker).toEqual(0.0022);
            expect(testExchange.commissions["BTC"].maker).toEqual(0.0015);
            expect(testExchange.commissions["BTC"].taker).toEqual(0.0022);
            expect(testExchange.commissions["USD"].maker).toEqual(0.0015);
            expect(testExchange.commissions["USD"].taker).toEqual(0.0022);
            expect(testExchange.confirmationTime).toEqual(1);
        });
        it("Commissions", function () {
            var testExchange = new TestExchange_1.default({
                name: 'testExchange1',
                fixedCurrencies: ["LTC", "BTC"],
                variableCurrencies: ["BTC", "USD"],
                commissions: {
                    LTC: {
                        maker: 0.0015,
                        taker: 0.0022
                    },
                    BTC: {
                        maker: 0.0033,
                        taker: 0.0044
                    }
                },
                defaultPriceRounding: 4,
                defaultMinAmount: 0.01,
                confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
            });
            expect(testExchange.commissions["LTC"].maker).toEqual(0.0015);
            expect(testExchange.commissions["LTC"].taker).toEqual(0.0022);
            expect(testExchange.commissions["BTC"].maker).toEqual(0.0033);
            expect(testExchange.commissions["BTC"].taker).toEqual(0.0044);
            expect(testExchange.commissions["USD"].maker).toEqual(0);
            expect(testExchange.commissions["USD"].taker).toEqual(0);
            expect(testExchange.confirmationTime).toEqual(1);
        });
        it("{ [currency: string]: number}", function () {
            var testExchange = new TestExchange_1.default({
                name: 'testExchange1',
                fixedCurrencies: ["LTC", "BTC"],
                variableCurrencies: ["BTC", "USD"],
                commissions: {
                    LTC: 0.0011,
                    BTC: 0.0022
                },
                defaultPriceRounding: 4,
                defaultMinAmount: 0.01,
                confirmationTime: 1 //not a fixed rate exchange so zero confirmation time
            });
            expect(testExchange.commissions["LTC"].maker).toEqual(0.0011);
            expect(testExchange.commissions["LTC"].taker).toEqual(0.0011);
            expect(testExchange.commissions["BTC"].maker).toEqual(0.0022);
            expect(testExchange.commissions["BTC"].taker).toEqual(0.0022);
            expect(testExchange.commissions["USD"].maker).toEqual(0);
            expect(testExchange.commissions["USD"].taker).toEqual(0);
            expect(testExchange.confirmationTime).toEqual(1);
        });
    });
    it("initialise with balances some", function () {
        var testExchange = new TestExchange_1.default({
            name: 'testExchange3',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["BTC", "USD"],
            commissions: 0.0015,
            defaultPriceRounding: 4,
            defaultMinAmount: 0.01,
            confirmationTime: 1,
            balances: [
                { currency: 'BTC', totalBalance: '7', availableBalance: '5' },
                { currency: 'USD', totalBalance: '444', availableBalance: '333' }]
        });
        // balances initialised to zero
        expect(testExchange.account).toBeDefined();
        expect(testExchange.account.balances['BTC']).toBeDefined();
        expect(testExchange.account.balances['BTC'].exchangeName).toEqual('testExchange3');
        expect(testExchange.account.balances['BTC'].currency).toEqual('BTC');
        expect(testExchange.account.balances['BTC'].totalBalance).toEqual('7');
        expect(testExchange.account.balances['BTC'].availableBalance).toEqual('5');
        expect(testExchange.account.balances['USD'].exchangeName).toEqual('testExchange3');
        expect(testExchange.account.balances['USD'].currency).toEqual('USD');
        expect(testExchange.account.balances['USD'].totalBalance).toEqual('444');
        expect(testExchange.account.balances['USD'].availableBalance).toEqual('333');
        expect(testExchange.account.balances['LTC'].exchangeName).toEqual('testExchange3');
        expect(testExchange.account.balances['LTC'].currency).toEqual('LTC');
        expect(testExchange.account.balances['LTC'].totalBalance).toEqual('0');
        expect(testExchange.account.balances['LTC'].availableBalance).toEqual('0');
    });
});
