/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />

import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import OrderBook from '../../models/orderBook';
import Order from '../../models/order';
import Exchange from '../../models/exchange';
import TestExchange from '../../exchanges/TestExchange';

import SellAmountCalculator from '../../models/vwapCalculators/sellAmountCalculator';
import BuyAmountCalculator from '../../models/vwapCalculators/buyAmountCalculator';
import TradeAmountCalculator from '../../models/vwapCalculators/tradeAmountCalculator';

const logger = require('config-logger');

describe("orderBook: ", function()
{
    let testExchange1, testOrderBook;

    beforeEach(function()
    {
        // Define test exchange data rather than using live exchange config that can change
        testExchange1 = new TestExchange({
            name: 'testExchange1',
            fixedCurrencies: ["LTC", "BTC"],
            variableCurrencies: ["AUD", "EUR", "USD"],
            commissions: {
                LTC: {maker: 0.01, taker: 0.1},
                BTC: {maker: 0.002, taker: 0.02},
                AUD: {maker: 0.001, taker: 0.01},
                USD: {maker: 0.006, taker: 0.06},
                EUR: {maker: 0.003, taker: 0.03}
            },
            confirmationTime: 1,	//not a fixed rate exchange so zero confirmation time
            currencyRounding: {BTC: 8, LTC: 8, USD: 8},		// decimal places to be used in rounding currency amounts
            defaultCurrencyRounding: 2,
            defaultMinAmount: 0.1,
            priceRounding: {BTCUSD: 3},
            minAmount:{
                BTCUSD: 0.0001
            }
        });

        Exchange.exchanges = {
            'testExchange1': testExchange1
        };
    });

    describe("add and remove order functions with", function()
    {
        describe("no orders in the order book", function()
        {
            beforeEach(function()
            {
                testOrderBook = new OrderBook({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCUSD',
                    bids: [],
                    asks: []
                });
            });

            it("add new buy order", function()
            {
                const newOrder = new Order({
                    exchangeId: '12345',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCUSD',
                    side:'buy',
                    amount: '2',        // the fixed currency amount before fess are deducted
                    amountRemaining: '0.1',
                    price:'500'
                });

                testOrderBook.addOrder(newOrder);

                expect(testOrderBook.bids.length).toEqual(1);
                expect(testOrderBook.asks.length).toEqual(0);

                expect(testOrderBook.bids[0].price).toEqual(500);
                expect(testOrderBook.bids[0].amount).toEqual(0.1);

                // check updating the new order will not update the order book
                newOrder.price = '444';
                newOrder.amountRemaining = '3';
                expect(testOrderBook.bids[0].price).toEqual(500);
                expect(testOrderBook.bids[0].amount).toEqual(0.1);
            });

            it("add new sell order", function()
            {
                const newOrder = new Order({
                    exchangeId: '12345',
                    exchangeName: 'testExchange1',
                    symbol: 'BTCUSD',
                    side:'sell',
                    amount: '88',        // the fixed currency amount before fess are deducted
                    amountRemaining: '77',
                    price:'333'
                });

                testOrderBook.addOrder(newOrder);

                expect(testOrderBook.bids.length).toEqual(0);
                expect(testOrderBook.asks.length).toEqual(1);

                expect(testOrderBook.asks[0].price).toEqual(333);
                expect(testOrderBook.asks[0].amount).toEqual(77);

                // check updating the new order will not update the order book
                newOrder.price = '222';
                newOrder.amountRemaining = '4';
                expect(testOrderBook.asks[0].price).toEqual(333);
                expect(testOrderBook.asks[0].amount).toEqual(77);
            });

            it("remove old order", function()
            {

            });
        });

        describe("one order in the order book", function()
        {
            beforeEach(function()
            {
                testOrderBook = new OrderBook({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCUSD',
                    bids: [
                        [432, 0.22]
                    ],
                    asks: [
                        [467, 0.333]
                    ]
                });
            });

            describe("add new buy order", function()
            {
                it("before existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '7',        // the fixed currency amount before fess are deducted
                        amountRemaining: '0.4444',
                        price:'450'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(2);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.bids[0].price).toEqual(450);
                    expect(testOrderBook.bids[0].amount).toEqual(0.4444);
                    expect(testOrderBook.bids[1].price).toEqual(432);
                    expect(testOrderBook.bids[1].amount).toEqual(0.22);
                });

                it("with same rate as existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '7',        // the fixed currency amount before fess are deducted
                        amountRemaining: '0.11',
                        price:'432'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(0.33);
                });

                it("after existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '321',        // the fixed currency amount before fess are deducted
                        amountRemaining: '123',
                        price:'300'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(2);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(0.22);
                    expect(testOrderBook.bids[1].price).toEqual(300);
                    expect(testOrderBook.bids[1].amount).toEqual(123);
                });
            });

            describe("add new sell order", function()
            {
                it("before existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '999',        // the fixed currency amount before fess are deducted
                        amountRemaining: '100',
                        price:'460'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(2);

                    expect(testOrderBook.asks[0].price).toEqual(460);
                    expect(testOrderBook.asks[0].amount).toEqual(100);
                    expect(testOrderBook.asks[1].price).toEqual(467);
                    expect(testOrderBook.asks[1].amount).toEqual(0.333);
                });

                it("with same rate as existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '4',        // the fixed currency amount before fess are deducted
                        amountRemaining: '0.222',
                        price:'467'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.555);
                });

                it("after existing order", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '432',        // the fixed currency amount before fess are deducted
                        amountRemaining: '321',
                        price:'666'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(2);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.333);
                    expect(testOrderBook.asks[1].price).toEqual(666);
                    expect(testOrderBook.asks[1].amount).toEqual(321);
                });
            });

            describe("remove buy order", function()
            {
                it("leaving no bid orders", function()
                {
                    const testOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '0.33',
                        amountRemaining: '0.22',
                        amountLastPartial: '0.1',
                        price:'432'
                    });

                    testOrderBook.removeOrder(testOrder);

                    expect(testOrderBook.bids.length).toEqual(0);
                    expect(testOrderBook.asks.length).toEqual(1);
                });

                it("leaving a bid order with some amount", function()
                {
                    const testOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '0.33',
                        amountRemaining: '0.2',
                        amountLastPartial: '0.1',
                        price:'432'
                    });

                    testOrderBook.removeOrder(testOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(0.02);
                });
            });

            describe("remove sell order", function()
            {
                it("leaving no ask orders", function()
                {
                    const testOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '0.444',
                        amountRemaining: '0.333',
                        amountLastPartial: '0.2',
                        price:'467'
                    });

                    testOrderBook.removeOrder(testOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(0);
                });

                it("leaving a ask order with some amount", function()
                {
                    const testOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '0.444',
                        amountRemaining: '0.111',
                        amountLastPartial: '0.2',
                        price:'467'
                    });

                    testOrderBook.removeOrder(testOrder);

                    expect(testOrderBook.bids.length).toEqual(1);
                    expect(testOrderBook.asks.length).toEqual(1);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.222);
                });
            });
        });

        describe("many orders", function()
        {
            beforeEach(function()
            {
                testOrderBook = new OrderBook({
                    exchangeName: 'testExchange1',
                    symbol: 'BTCUSD',
                    bids: [
                        [432, 0.11],
                        [431, 0.22],
                        [430.123, 0.33],
                        [420.5, 0.33]
                    ],
                    asks: [
                        [467, 0.1],
                        [468.8, 2],
                        [468.999, 30],
                        [489.111, 400]
                    ]
                });
            });

            describe("add new buy order", function()
            {
                it("before first bid", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '7',        // the fixed currency amount before fess are deducted
                        amountRemaining: '0.4444',
                        price:'450'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(5);
                    expect(testOrderBook.asks.length).toEqual(4);

                    expect(testOrderBook.bids[0].price).toEqual(450);
                    expect(testOrderBook.bids[0].amount).toEqual(0.4444);
                    expect(testOrderBook.bids[1].price).toEqual(432);
                    expect(testOrderBook.bids[1].amount).toEqual(0.11);
                });

                it("with same rate as first bid", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '7',        // the fixed currency amount before fess are deducted
                        amountRemaining: '1',
                        price:'432'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(4);
                    expect(testOrderBook.asks.length).toEqual(4);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(1.11);
                    expect(testOrderBook.bids[1].price).toEqual(431);
                    expect(testOrderBook.bids[1].amount).toEqual(0.22);
                });

                it("after first bid but before second bid", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '321',        // the fixed currency amount before fess are deducted
                        amountRemaining: '123',
                        price:'431.5'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(5);
                    expect(testOrderBook.asks.length).toEqual(4);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(0.11);
                    expect(testOrderBook.bids[1].price).toEqual(431.5);
                    expect(testOrderBook.bids[1].amount).toEqual(123);
                    expect(testOrderBook.bids[2].price).toEqual(431);
                    expect(testOrderBook.bids[2].amount).toEqual(0.22);
                });

                it("after all bids", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '3',        // the fixed currency amount before fess are deducted
                        amountRemaining: '2',
                        price:'100'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(5);
                    expect(testOrderBook.asks.length).toEqual(4);

                    expect(testOrderBook.bids[0].price).toEqual(432);
                    expect(testOrderBook.bids[0].amount).toEqual(0.11);
                    expect(testOrderBook.bids[1].price).toEqual(431);
                    expect(testOrderBook.bids[1].amount).toEqual(0.22);
                    expect(testOrderBook.bids[3].price).toEqual(420.5);
                    expect(testOrderBook.bids[3].amount).toEqual(0.33);
                    expect(testOrderBook.bids[4].price).toEqual(100);
                    expect(testOrderBook.bids[4].amount).toEqual(2);
                });
            });

            describe("add new sell order", function()
            {
                it("before first ask", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '999',        // the fixed currency amount before fess are deducted
                        amountRemaining: '100',
                        price:'460'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(4);
                    expect(testOrderBook.asks.length).toEqual(5);

                    expect(testOrderBook.asks[0].price).toEqual(460);
                    expect(testOrderBook.asks[0].amount).toEqual(100);
                    expect(testOrderBook.asks[1].price).toEqual(467);
                    expect(testOrderBook.asks[1].amount).toEqual(0.1);
                });

                it("with same rate as first ask", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '4',        // the fixed currency amount before fess are deducted
                        amountRemaining: '0.222',
                        price:'467'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(4);
                    expect(testOrderBook.asks.length).toEqual(4);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.322);
                });

                it("after second ask but before third ask", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '7',        // the fixed currency amount before fess are deducted
                        amountRemaining: '1',
                        price:'468.9'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(4);
                    expect(testOrderBook.asks.length).toEqual(5);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.1);
                    expect(testOrderBook.asks[1].price).toEqual(468.8);
                    expect(testOrderBook.asks[1].amount).toEqual(2);
                    expect(testOrderBook.asks[2].price).toEqual(468.9);
                    expect(testOrderBook.asks[2].amount).toEqual(1);
                    expect(testOrderBook.asks[3].price).toEqual(468.999);
                    expect(testOrderBook.asks[3].amount).toEqual(30);
                    expect(testOrderBook.asks[4].price).toEqual(489.111);
                    expect(testOrderBook.asks[4].amount).toEqual(400);
                });

                it("after all existing asks", function()
                {
                    const newOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'sell',
                        amount: '432',        // the fixed currency amount before fess are deducted
                        amountRemaining: '321',
                        price:'666'
                    });

                    testOrderBook.addOrder(newOrder);

                    expect(testOrderBook.bids.length).toEqual(4);
                    expect(testOrderBook.asks.length).toEqual(5);

                    expect(testOrderBook.asks[0].price).toEqual(467);
                    expect(testOrderBook.asks[0].amount).toEqual(0.1);
                    expect(testOrderBook.asks[4].price).toEqual(666);
                    expect(testOrderBook.asks[4].amount).toEqual(321);
                });
            });

            describe("remove buy order", function()
            {
                let testOrder;
                beforeEach(function()
                {
                    testOrder = new Order({
                        exchangeId: '12345',
                        exchangeName: 'testExchange1',
                        symbol: 'BTCUSD',
                        side:'buy',
                        amount: '0.33',
                        amountRemaining: '0',
                        amountTraded: '0',
                        amountLastPartial: '0.1',
                        price:'0'
                    });
                });

                describe("matching first bid", function()
                {
                    it("leaving no bid order", function()
                    {
                        testOrder.price = '432';
                        testOrder.amountRemaining = '0.11';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(3);
                        expect(testOrderBook.asks.length).toEqual(4);
                    });

                    it("leaving a bid order with some amount", function()
                    {
                        testOrder.price = '432';
                        testOrder.amountRemaining = '0.1';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(4);
                        expect(testOrderBook.asks.length).toEqual(4);

                        expect(testOrderBook.bids[0].price).toEqual(432);
                        expect(testOrderBook.bids[0].amount).toEqual(0.01);
                    });
                });

                describe("matching second bid", function()
                {
                    it("leaving no bid order", function()
                    {
                        testOrder.price = '431';
                        testOrder.amountRemaining = '0.22';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(3);
                        expect(testOrderBook.asks.length).toEqual(4);
                    });

                    it("leaving a bid order with some amount", function()
                    {
                        testOrder.price = '431';
                        testOrder.amountRemaining = '0.1';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(4);
                        expect(testOrderBook.asks.length).toEqual(4);

                        expect(testOrderBook.bids[1].price).toEqual(431);
                        expect(testOrderBook.bids[1].amount).toEqual(0.12);
                    });
                });

                describe("matching last bid", function()
                {
                    it("leaving no bid order", function()
                    {
                        testOrder.price = '420.5';
                        testOrder.amountRemaining = '0.33';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(3);
                        expect(testOrderBook.asks.length).toEqual(4);
                    });

                    it("leaving a bid order with some amount", function()
                    {
                        testOrder.price = '420.5';
                        testOrder.amountRemaining = '0.11';

                        testOrderBook.removeOrder(testOrder);

                        expect(testOrderBook.bids.length).toEqual(4);
                        expect(testOrderBook.asks.length).toEqual(4);

                        expect(testOrderBook.bids[3].price).toEqual(420.5);
                        expect(testOrderBook.bids[3].amount).toEqual(0.22);
                    });
                });
            });
        });
    });

    describe("calculate bid and asks for a spread", function ()
    {
        beforeEach(function ()
        {
            testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [
                    [999, 0.01],
                    [995, 0.03],
                    [990, 1.05],
                    [980, 20.12]
                ],
                asks: [
                    [1001, 0.01],
                    [1002, 0.01],
                    [1003, 0.03],
                    [1005, 0.1],
                    [1010, 10]
                ]
            });
        });

        it("of 0.1%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.1);

            expect(bidAsk.bid).toEqual('999.501');
            expect(bidAsk.ask).toEqual('1000.499');
        });

        it("of 0.2%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.2);

            expect(bidAsk.bid).toEqual('999.002');
            expect(bidAsk.ask).toEqual('1000.998');
        });

        it("of 0.3%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.3);

            expect(bidAsk.bid).toEqual('997.005');
            expect(bidAsk.ask).toEqual('999.995');
        });

        it("of 0.5%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.5);

            expect(bidAsk.bid).toEqual('996.011');
            expect(bidAsk.ask).toEqual('1000.989');
        });

        it("of 0.75%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.75);

            expect(bidAsk.bid).toEqual('995.001');
            expect(bidAsk.ask).toEqual('1002.463');
        });

        it("of 0.85%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(0.85);

            expect(bidAsk.bid).toEqual('994.546');
            expect(bidAsk.ask).toEqual('1002.999');
        });

        it("of 1%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(1);

            expect(bidAsk.bid).toEqual('993.069');
            expect(bidAsk.ask).toEqual('1002.999');
        });


        it("of 1.3%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(1.3);

            expect(bidAsk.bid).toEqual('990.128');
            expect(bidAsk.ask).toEqual('1002.999');
        });

        it("of 1.35%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(1.35);

            expect(bidAsk.bid).toEqual('990.001');
            expect(bidAsk.ask).toEqual('1003.365');
        });

        it("of 2%", function () {
            const bidAsk = testOrderBook.calcBidAskForSpread(2);

            expect(bidAsk.bid).toEqual('990.001');
            expect(bidAsk.ask).toEqual('1009.800');
        });

    });

    describe("calculate spread with no bids or asks", function ()
    {
        it("should throw an exception", function ()
        {
            testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [],
                asks: []
            });

            const testFunction = function () {
                const bidAsk = testOrderBook.calcBidAskForSpread(0.2);
            };

            expect(testFunction).toThrow();
        });

    });

    describe("calculate spread with only 1 bid 0 asks", function () {
        it("spread = 1%", function () {
            testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [
                    [1000, 0.01]
                ],
                asks: []
            });

            const bidAsk = testOrderBook.calcBidAskForSpread(1);

            expect(bidAsk.bid).toEqual('1000.001');
            expect(bidAsk.ask).toEqual('1010.000');
        });
    });

    describe("calculate spread with only 0 bids 1 ask", function ()
    {
        it("spread = 1%", function ()
        {
            const testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [],
                asks: [
                    [1000, 0.01]
                ]
            });

            const bidAsk = testOrderBook.calcBidAskForSpread('1');

            expect(bidAsk.bid).toEqual('990.099');
            expect(bidAsk.ask).toEqual('999.999');
        });
    });

    describe("calculate spread within a wide initial bid and ask spread", function ()
    {
        it(" = 1%", function ()
        {
            const testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [
                    [950, 0.02],
                    [945, 0.1]
                ],
                asks: [
                    [1050, 0.01],
                    [1060, 1]
                ]
            });

            const bidAsk = testOrderBook.calcBidAskForSpread('1');

            expect(bidAsk.bid).toEqual('995.026');
            expect(bidAsk.ask).toEqual('1004.974');
        });
    });

    describe("With no pending orders, calculate best", function ()
    {
        beforeEach(function ()
        {
            testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [
                    [999, 0.01],
                    [995, 0.03],
                    [990, 1.05],
                    [980, 20.12]
                ],
                asks: [
                    [1001, 0.01],
                    [1002, 0.01],
                    [1003, 0.03],
                    [1005, 0.1],
                    [1010, 10]
                ]
            });
        });

        describe("sell price where", function ()
        {
            it("no min sell rate", function () {
                const bestRate = testOrderBook.getBestSell();

                expect(bestRate).toEqual('1000.999');
            });

            it("min sell rate is zero", function () {
                const bestRate = testOrderBook.getBestSell(0);

                expect(bestRate).toEqual('1000.999');
            });

            it("min sell rate equals with first bid", function () {
                const bestRate = testOrderBook.getBestSell(999);

                expect(bestRate).toEqual('999');
            });

            it("min sell rate crosses with first bid", function () {
                const bestRate = testOrderBook.getBestSell(997.5);

                expect(bestRate).toEqual('999');
            });

            it("min sell rate is less than the first ask but does not cross with any bids", function () {
                const bestRate = testOrderBook.getBestSell(1000.5);

                expect(bestRate).toEqual('1000.999');
            });

            it("min sell rate is 2 min price less than the first ask", function () {
                const bestRate = testOrderBook.getBestSell(1000.998);

                expect(bestRate).toEqual('1000.999');
            });

            it("min sell rate is 1 min price less than the first ask", function () {
                const bestRate = testOrderBook.getBestSell(1000.999);

                expect(bestRate).toEqual('1000.999');
            });

            it("min sell rate equals first ask", function () {
                const bestRate = testOrderBook.getBestSell(1001);

                expect(bestRate).toEqual('1001');
            });

            it("min sell rate is 1 min price above the first ask", function () {
                const bestRate = testOrderBook.getBestSell(1001.001);

                expect(bestRate).toEqual('1001.999');
            });

            it("min sell rate is between first and second ask", function () {
                const bestRate = testOrderBook.getBestSell(1001.5);

                expect(bestRate).toEqual('1001.999');
            });

            it("min sell rate equals second ask", function () {
                const bestRate = testOrderBook.getBestSell(1002);

                expect(bestRate).toEqual('1002');
            });

            it("min sell rate equals last ask", function () {
                const bestRate = testOrderBook.getBestSell(1010);

                expect(bestRate).toEqual('1010');
            });

            it("min sell rate is 1 min price greater than the last ask", function () {
                const bestRate = testOrderBook.getBestSell(1010.001);

                expect(bestRate).toEqual('1010.001');
            });

            it("min sell rate is a lot greater than the last ask", function () {
                const bestRate = testOrderBook.getBestSell(1150);

                expect(bestRate).toEqual('1150');
            });

            it("min and max sell rates less than the inside bid rate", function () {
                const bestRate = testOrderBook.getBestSell(901, 996);

                // equal inside bid which means this is a market taker trade
                expect(bestRate).toEqual('999');
            });

            it("min sell rate < inside bid rate but max sell rate > inside bid", function () {
                const bestRate = testOrderBook.getBestSell(901, 999.7);

                expect(bestRate).toEqual('999');
            });
        });

        describe("buy price where with no max buy rate or min amount to ignore", function ()
        {
            it("no max buy rate", function () {
                const bestRate = testOrderBook.getBestBuy();

                expect(bestRate).toEqual('999.001');
            });

            it("max buy rate = Infinity", function () {
                const bestRate = testOrderBook.getBestBuy(Infinity);

                expect(bestRate).toEqual('999.001');
            });

            it("max buy rate equals with first ask", function () {
                const bestRate = testOrderBook.getBestBuy(1001);

                expect(bestRate).toEqual('1001');
            });

            it("max buy rate crosses with first ask", function () {
                const bestRate = testOrderBook.getBestBuy(1001.5);

                expect(bestRate).toEqual('1001');
            });

            it("max buy rate is greater than the first bid but does not cross with any asks", function () {
                const bestRate = testOrderBook.getBestBuy(999.5);

                expect(bestRate).toEqual('999.001');
            });

            it("max buy rate is 2 min price greater than the first bid", function () {
                const bestRate = testOrderBook.getBestBuy(999.002);

                expect(bestRate).toEqual('999.001');
            });

            it("max buy rate is 1 min price greater than the first bid", function () {
                const bestRate = testOrderBook.getBestBuy(999.001);

                expect(bestRate).toEqual('999.001');
            });

            it("max buy rate equals first bid", function () {
                const bestRate = testOrderBook.getBestBuy(999);

                expect(bestRate).toEqual('999');
            });

            it("max buy rate is 1 min price below the first bid", function () {
                const bestRate = testOrderBook.getBestBuy(998.999);

                expect(bestRate).toEqual('995.001');
            });

            it("max buy rate is between first and second bid", function () {
                const bestRate = testOrderBook.getBestBuy(997.123);

                expect(bestRate).toEqual('995.001');
            });

            it("max buy rate equals second bid", function () {
                const bestRate = testOrderBook.getBestBuy(995);

                expect(bestRate).toEqual('995');
            });

            it("max buy rate equals last bid", function () {
                const bestRate = testOrderBook.getBestBuy(980);

                expect(bestRate).toEqual('980');
            });

            it("max buy rate is 1 min price less than the last bid", function () {
                const bestRate = testOrderBook.getBestBuy(979.999);

                expect(bestRate).toEqual('979.999');
            });

            it("max buy rate is a lot less than the last bid", function () {
                const bestRate = testOrderBook.getBestBuy(900);

                expect(bestRate).toEqual('900');
            });

            it("max and min buy rates > inside ask rate", function () {
                const bestRate = testOrderBook.getBestBuy(1010, 1005);

                // equal inside ask which means this is a market taker trade
                expect(bestRate).toEqual('1001');
            });

            it("max buy rate > than the inside ask rate but min buy rate < inside ask", function () {
                const bestRate = testOrderBook.getBestBuy(1010, 1000.5);

                expect(bestRate).toEqual('1001');
            });
        });
    });

    describe("with pending orders, calculate best", function ()
    {
        let testExchange2, orderBookWithMyOrders;

        beforeEach(function ()
        {
            // Define test exhange data rather than using live exchange config that can change
            testExchange2 = new TestExchange({
                name: 'testExchange2',
                fixedCurrencies: ["LTC", "BTC"],
                variableCurrencies: ["AUD", "EUR", "USD"],
                commissions: {LTC: 0.001, BTC: 0.001, AUD: 0.001, USD: 0.001, EUR: 0.001},		//commission of currency being bought
                confirmationTime: 1,	//not a fixed rate exchange so zero confirmation time
                currencyRounding: {BTC: 8, LTC: 8, USD: 8, EUR: 8},		// decimal places to be used in rounding currency amounts
                defaultPriceRounding: 5,
                defaultMinAmount: 0.00000001,
                priceRounding: {BTCEUR: 4}
            });

            Exchange.exchanges = {};
            Exchange.exchanges['testExchange2'] = testExchange2;

            logger.debug('added %s test orders', testExchange2.markets['BTCEUR'].orders.length);

            orderBookWithMyOrders = new OrderBook({
                exchangeName: 'testExchange2',
                symbol: 'BTCEUR',
                bids: [
                    [999, 0.01],
                    [995, 0.03],
                    [990, 1.05],
                    [980, 20.12]
                ],
                asks: [
                    [1001, 0.01],
                    [1002, 0.01],
                    [1003, 0.03],
                    [1005, 0.1],
                    [1010, 10]
                ],
                minAmountToIgnore: 0
            });
        });

        describe("sell rate where", function ()
        {
            let testAsk1, testAsk2;

            beforeEach(function()
            {
                testAsk1 = new Order(
                    {exchangeId: 'QWERT',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'sell',
                        state: 'pending',
                        amount: '0.07',        // the fixed currency amount before fess are deducted
                        amountTraded: '0.05',
                        amountRemaining: '0.02',
                        price: '1003'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testAsk1);

                testAsk2 = new Order(
                    {exchangeId: '123',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'sell',
                        state: 'pending',
                        amount: '1',        // the fixed currency amount before fess are deducted
                        amountTraded: '0.9',
                        amountRemaining: '0.1',
                        price: '1005'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testAsk2);
            });

            describe("no min sell rate", function ()
            {
                it("and 0 min bid amount to ignore", function ()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell();

                    expect(bestRate).toEqual('1000.9999');
                });

                it("and min amount to ignore > first ask amount", function ()
                {
                    orderBookWithMyOrders.minAmountToIgnore = '0.2';

                    const bestRate = orderBookWithMyOrders.getBestSell();

                    expect(bestRate).toEqual('1009.9999');
                });
            });

            describe("min sell less than first ask and greater than my ask", function ()
            {
                describe("and 0 min bid amount to ignore", function ()
                {
                    it("and no max sell rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5);

                        expect(bestRate).toEqual('1000.9999');
                    });

                    it("and max sell rate > first ask", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5, 1001.22);

                        expect(bestRate).toEqual('1000.9999');
                    });

                    it("and max sell rate < first ask", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5, 1000.9);

                        expect(bestRate).toEqual('1000.9');
                    });
                });

                describe("and min amount to ignore > first ask amount", function ()
                {
                    beforeEach(function ()
                    {
                        orderBookWithMyOrders.minAmountToIgnore = '0.2';
                    });

                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5);

                        expect(bestRate).toEqual('1009.9999');
                    });

                    it("and max sell rate > last ask", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5, 1101.22);

                        expect(bestRate).toEqual('1009.9999');
                    });

                    it("and max sell rate < first ask", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5, 1000.9);

                        expect(bestRate).toEqual('1000.9');
                    });

                    it("and max sell rate < last ask", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1000.5, 1008.888);

                        expect(bestRate).toEqual('1008.888');
                    });
                });
            });

            it("min sell between first and second ask and greater than my ask", function () {
                const bestRate = orderBookWithMyOrders.getBestSell(1001.6);

                expect(bestRate).toEqual('1001.9999');
            });

            it("min sell between second and third ask and greater than my ask", function () {
                const bestRate = orderBookWithMyOrders.getBestSell(1002.7);

                expect(bestRate).toEqual('1002.9999');
            });

            describe("min sell equal my ask and someone else's bid", function ()
            {
                describe("and 0 min bid amount to ignore", function ()
                {
                    it("and no max sell rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1003);

                        expect(bestRate).toEqual('1003');
                    });
                });

                describe("and min amount to ignore equals first, second and third ask amounts less my amount of 0.2 on the third ask", function ()
                {
                    beforeEach(function ()
                    {
                        orderBookWithMyOrders.minAmountToIgnore = '0.1';
                    });

                    it("and no max sell rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1003);

                        expect(bestRate).toEqual('1009.9999');
                    });

                    it("and max ask rate < last ask rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1003, 1004.4444);

                        expect(bestRate).toEqual('1004.4444');
                    });

                    it("and max ask rate > last ask rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestSell(1003, 2000);

                        expect(bestRate).toEqual('1009.9999');
                    });
                });
            });

            it("min sell greater than my first ask but less than my second ask", function () {
                const bestRate = orderBookWithMyOrders.getBestSell(1003.8);

                expect(bestRate).toEqual('1009.9999');
            });

            it("min sell greater than my first ask but equal to my second ask", function () {
                const bestRate = orderBookWithMyOrders.getBestSell(1005);

                expect(bestRate).toEqual('1009.9999');
            });

            describe("min sell greater than my first and second asks", function () {
                it("and no max sell rate", function () {
                    const bestRate = orderBookWithMyOrders.getBestSell(1006.1);

                    expect(bestRate).toEqual('1009.9999');
                });

                it("and max sell rate > last ask", function () {
                    const bestRate = orderBookWithMyOrders.getBestSell(1111.111);

                    expect(bestRate).toEqual('1111.111');
                });
            });

        });

        describe("sell rate with two pending sell orders at the same price", function()
        {
            let testAsk1, testAsk2;

            beforeEach(function()
            {
                testAsk1 = new Order(
                    {exchangeId: 'QWERT',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'sell',
                        state: 'pending',
                        amount: '0.07',        // the fixed currency amount before fess are deducted
                        amountTraded: '0.05',
                        amountRemaining: '0.02',
                        price: '1003'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testAsk1);

                testAsk2 = new Order(
                    {exchangeId: '123',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'sell',
                        state: 'pending',
                        amount: '1',
                        amountTraded: '0.98',
                        amountRemaining: '0.02',
                        price: '1005'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testAsk2);
            });

            describe("with no other orders at the same price as my orders where", function()
            {
                beforeEach(function()
                {
                    const testAsk3 = new Order(
                        {exchangeId: '333',
                            exchangeName: 'testExchange2',
                            symbol: 'BTCEUR',
                            side: 'sell',
                            state: 'pending',
                            amount: '0.98',
                            amountTraded: '0',
                            amountRemaining: '0.98',
                            price: '1005'
                        });

                    testExchange2.markets['BTCEUR'].addOrder(testAsk3);
                });

                it("min sell rate less than rate of all of asks", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1003.5);

                    expect(bestRate).toEqual('1009.9999');
                });

                it("min sell rate less than rate of my two of asks with the same rate but greater than the first ask", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1004.5555);

                    expect(bestRate).toEqual('1009.9999');
                });

                it("max sell rate equals rate of my two asks", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1005);

                    expect(bestRate).toEqual('1009.9999');
                });

                it("max sell rate greater than rate of my two asks with the same price", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1007.7777);

                    expect(bestRate).toEqual('1009.9999');
                });
            });

            describe("with another third party order at the same price as my orders where", function()
            {
                beforeEach(function()
                {
                    const testAsk3 = new Order(
                        {exchangeId: '333',
                            exchangeName: 'testExchange2',
                            symbol: 'BTCEUR',
                            side: 'sell',
                            state: 'pending',
                            amount: '0.2',
                            amountTraded: '0.15',
                            amountRemaining: '0.05',
                            price: '1005'
                        });

                    testExchange2.markets['BTCEUR'].addOrder(testAsk3);
                });

                it("min sell rate less than rate of all of asks", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1003.5);

                    expect(bestRate).toEqual('1004.9999');
                });

                it("min sell rate less than rate of my two of asks with the same rate but greater than the first ask", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1004.5555);

                    expect(bestRate).toEqual('1004.9999');
                });

                it("max sell rate equals rate of my two asks", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1005);

                    expect(bestRate).toEqual('1005');
                });

                it("max sell rate greater than rate of my two asks with the same price", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestSell(1007.7777);

                    expect(bestRate).toEqual('1009.9999');
                });
            });
        });

        describe("buy rate where", function ()
        {
            let testBid;

            beforeEach(function()
            {
                testBid = new Order(
                    {exchangeId: 'ASDFGHJKL',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'buy',
                        state: 'pending',
                        amount: '0.05',        // the fixed currency amount before fess are deducted
                        amountTraded: '0.02',
                        amountRemaining: '0.03',
                        price: '995'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testBid);
            });

            describe("no max buy rate", function ()
            {
                it("and 0 min bid amount to ignore", function ()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy();

                    expect(bestRate).toEqual('999.0001');
                });

                it("and min amount to ignore > first ask amount", function ()
                {
                    orderBookWithMyOrders.minAmountToIgnore = '0.2';

                    const bestRate = orderBookWithMyOrders.getBestBuy();

                    expect(bestRate).toEqual('990.0001');
                });
            });

            describe("max buy rate greater than first bid rate and greater than my bid rate", function ()
            {
                describe("and 0 min bid amount to ignore", function ()
                {
                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.1);

                        expect(bestRate).toEqual('999.0001');
                    });

                    it("and min buy rate < first bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.2, 998);

                        expect(bestRate).toEqual('999.0001');
                    });

                    it("and min buy rate > first bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.5, 999.2);

                        expect(bestRate).toEqual('999.2');
                    });
                });

                describe("and min amount to ignore greater than first bid amount", function ()
                {
                    beforeEach(function ()
                    {
                        orderBookWithMyOrders.minAmountToIgnore = '0.2';
                    });

                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.1);

                        expect(bestRate).toEqual('990.0001');
                    });

                    it("and min buy rate < first bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.2, 950);

                        expect(bestRate).toEqual('990.0001');
                    });

                    it("and min buy rate > first bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(999.5, 991.2);

                        expect(bestRate).toEqual('991.2');
                    });
                });
            });

            describe("max buy less than first bid but greater than my bid", function ()
            {
                describe("and 0 min bid amount to ignore", function ()
                {
                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998);

                        expect(bestRate).toEqual('990.0001');
                    });

                    it("and min buy rate < third bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998, 985);

                        expect(bestRate).toEqual('990.0001');
                    });

                    it("and min buy rate > third bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998, 992.9876);

                        expect(bestRate).toEqual('992.9876');
                    });
                });

                describe("and 1.05 min bid amount to ignore", function ()
                {
                    beforeEach(function () {
                        orderBookWithMyOrders.minAmountToIgnore = '1.05';
                    });

                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998);

                        expect(bestRate).toEqual('980.0001');
                    });

                    it("and min buy rate < third bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998, 977);

                        expect(bestRate).toEqual('980.0001');
                    });

                    it("and min buy rate > third bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(998, 987.1234);

                        expect(bestRate).toEqual('987.1234');
                    });
                });
            });

            it("max buy equal my bid", function ()
            {
                const bestRate = orderBookWithMyOrders.getBestBuy(995);

                expect(bestRate).toEqual('990.0001');
            });

            it("max buy less than my bid", function ()
            {
                const bestRate = orderBookWithMyOrders.getBestBuy(993);

                expect(bestRate).toEqual('990.0001');
            });

            describe("max buy equal to third bid", function ()
            {
                describe("and 0 min bid amount to ignore", function ()
                {
                    it("and no min buy rate", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(990);

                        expect(bestRate).toEqual('990');
                    });

                    it("and min buy rate < forth bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(990, 955.55);

                        expect(bestRate).toEqual('990');
                    });

                    it("and min buy rate > forth bid", function () {
                        const bestRate = orderBookWithMyOrders.getBestBuy(990, 987.6543);

                        expect(bestRate).toEqual('990');
                    });
                });
            });

            it("max buy less than third bid", function ()
            {
                const bestRate = orderBookWithMyOrders.getBestBuy(987);

                expect(bestRate).toEqual('980.0001');
            });

            describe("max buy less than last bid", function ()
            {
                it("and no min buy rate", function () {
                    const bestRate = orderBookWithMyOrders.getBestBuy(900);

                    expect(bestRate).toEqual('900');
                });

                it("and min buy rate < max buy rate", function () {
                    const bestRate = orderBookWithMyOrders.getBestBuy(900, 872.12);

                    expect(bestRate).toEqual('872.12');
                });
            });
        });

        describe("buy rate with two pending buy orders at the same price", function()
        {
            let testBid;

            beforeEach(function()
            {
                testBid = new Order(
                    {exchangeId: 'ASDFGHJKL',
                        exchangeName: 'testExchange2',
                        symbol: 'BTCEUR',
                        side: 'buy',
                        state: 'pending',
                        amount: '0.05',        // the fixed currency amount before fess are deducted
                        amountTraded: '0.04',
                        amountRemaining: '0.01',
                        price: '995'
                    });

                testExchange2.markets['BTCEUR'].addOrder(testBid);
            });

            describe("with no other orders at the same price as my orders where", function()
            {
                beforeEach(function()
                {
                    const testBid2 = new Order(
                        {exchangeId: '1234',
                            exchangeName: 'testExchange2',
                            symbol: 'BTCEUR',
                            side: 'buy',
                            state: 'pending',
                            amount: '10',        // the fixed currency amount before fess are deducted
                            amountTraded: '9.98',
                            amountRemaining: '0.02',
                            price: '995',
                            tag: 'secondBuySamePrice'
                        });

                    testExchange2.markets['BTCEUR'].addOrder(testBid2);
                });

                it("max buy rate greater than rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(996);

                    expect(bestRate).toEqual('990.0001');
                });

                it("max buy rate equals rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(995);

                    expect(bestRate).toEqual('990.0001');
                });

                it("max buy rate less than rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(994);

                    expect(bestRate).toEqual('990.0001');
                });
            });

            describe("with another third party order at the same price as my orders where", function()
            {
                beforeEach(function()
                {
                    const testBid2 = new Order(
                        {exchangeId: '1234',
                            exchangeName: 'testExchange2',
                            symbol: 'BTCEUR',
                            side: 'buy',
                            state: 'pending',
                            amount: '10',        // the fixed currency amount before fess are deducted
                            amountTraded: '9.99',
                            amountRemaining: '0.01',
                            price: '995',
                            tag: 'secondBuySamePrice'
                        });

                    testExchange2.markets['BTCEUR'].addOrder(testBid2);
                });

                it("max buy rate greater than rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(996);

                    expect(bestRate).toEqual('995.0001');
                });

                it("max buy rate equals rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(995);

                    expect(bestRate).toEqual('995');
                });

                it("max buy rate less than rate of my two bids", function()
                {
                    const bestRate = orderBookWithMyOrders.getBestBuy(994);

                    expect(bestRate).toEqual('990.0001');
                });
            });
        });
    });

    describe("With no bids or asks in the order book", function ()
    {
        beforeEach(function ()
        {

            // Define test exchange data rather than using live exchange config that can change
            const testExchange3 = new TestExchange({
                name: 'testExchange3',
                fixedCurrencies: ["BTC"],
                variableCurrencies: ["USD"],
                commissions: {USD: 0.001},		//commission of currency being bought
                confirmationTime: 1,	//not a fixed rate exchange so zero confirmation time
                currencyRounding: {BTC: 5, USD: 2},		// decimal places to be used in rounding currency amounts
                defaultPriceRounding: 3,
                defaultMinAmount: 0.001,
                priceRounding: {BTCEUR: 4}
            });

            Exchange.exchanges = {};
            Exchange.exchanges['testExchange3'] = testExchange3;


            testOrderBook = new OrderBook({
                exchangeName: 'testExchange3',
                symbol: 'BTCUSD',
                bids: [],
                asks: []
            });
        });

        describe("sell rate where", function ()
        {
            it("no min sell rate", function ()
            {
                const bestRate = testOrderBook.getBestSell();

                expect(bestRate).toEqual('0');
            });

            it("min sell rate set", function ()
            {
                const bestRate = testOrderBook.getBestSell(1000);

                expect(bestRate).toEqual('1000');
            });
        });

        describe("buy rate where", function ()
        {
            it("no max buy rate", function ()
            {
                const bestRate = testOrderBook.getBestBuy();

                expect(bestRate).toEqual('Infinity');
            });

            it("max buy rate set", function ()
            {
                const bestRate = testOrderBook.getBestBuy(200);

                expect(bestRate).toEqual('200');
            });
        });
    });

    describe("getVwap from", function ()
    {
        let tradeAmountCalculator ;

        beforeEach(function()
        {
            tradeAmountCalculator = new TradeAmountCalculator();

            testOrderBook = new OrderBook({
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                bids: [
                    [999.999, 0.01],
                    [998.765, 0.22],
                    [996.000, 1.1234],
                    [990.001, 123.4321]
                ],
                asks: [
                    [1000, 0.0001],
                    [1000.001, 0.002],
                    [1002, 0.03],
                    [1015, 4.4444],
                    [1020, 100.0001]
                ]
            });
        });

        describe("exchange with buyCurrency fee structure", function()
        {
            let sellAmountTakerCalculator: SellAmountCalculator,
                sellAmountMakerCalculator: SellAmountCalculator,
                buyAmountTakerCalculator: BuyAmountCalculator,
                buyAmountMakerCalculator: BuyAmountCalculator;

            beforeEach(function()
            {
                testExchange1.feeStructure = 'buyCurrency';

                sellAmountTakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'taker');
                sellAmountMakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'maker');
                buyAmountTakerCalculator = new BuyAmountCalculator(testExchange1.markets['BTCUSD'], 'taker');
                buyAmountMakerCalculator = new BuyAmountCalculator(testExchange1.markets['BTCUSD'], 'maker');
            });

            describe("for sell amounts on the asks side", function()
            {
                it("sell amount < first ask amount with no starting amount", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(500), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1000));
                    // selling 500 USD = 500 USD gross as fee is on the buy side
                    // trade amount = 500 / 1000
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(0.5));
                });

                it("sell amount = first ask amount with no starting amount", function()
                {
                    testOrderBook.asks[0] =  {price: 1000, amount: 10.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(10555.5), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1000));
                    // 10555.5 / 1000 = 10.5555
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(10.5555));
                });

                it("sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountTakerCalculator);

                    // last trade amount = (12000 - 1000 * 10.5555) / 1100 = 1.31318181818182
                    // (1000 * 10.5555 + 1100 * 1.31318181818182) / (10.5555 + 1.31318181818182) = 1011.06426002734
                    expect(vwap.priceBN ).toEqual(BigNumber(1011.064));
                    // 10.5555 + 1.31318181818182 = ‌11.86868181818182
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(11.86868182));
                });

                it("maker commission, sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountMakerCalculator);

                    // same as taker commission as sell are on the buy side - not the sell side
                    expect(vwap.priceBN ).toEqual(BigNumber(1011.064));
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(11.86868182));
                });

                it("sell amount > first and second ask amounts and < first, second and third ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};
                    testOrderBook.asks[2] = {price: 5000, amount: 100};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(50000), sellAmountTakerCalculator);

                    // last trade amount = (50000 - 1000 * 10.5555 - 1100 * 5.5555) / 5000 = 6.66669
                    // (1000 * 10.5555 + 1100 * 5.5555 + 5000 * 6.66669) / (10.5555 + 5.5555 + 6.66669) = 2195.13041050256
                    expect(vwap.priceBN ).toEqual(BigNumber(2195.130));
                    // 10.5555 + 5.5555 + 6.66669
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(22.77769));
                });

                it("sell amount > all asks in order book", function()
                {
                    const vwap = testOrderBook.getVwap('asks', BigNumber(1000000), sellAmountMakerCalculator);

                    expect(vwap instanceof Error).toBe(true);
                    expect(vwap.name).toEqual('notEnoughOrders');
                });
            });

            describe("for sell amounts on the bids side", function()
            {
                it("sell amount < first bid amount with no starting amount", function()
                {
                    testOrderBook.bids[0] = {price: 1234.567, amount: 10.1111};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(5), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1234.567));
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(5));
                });

                it("sell amount = first bid amount with no starting amount", function()
                {
                    testOrderBook.bids[0] = {price: 1234.567, amount: 10.1111};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(10.1111), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1234.567));
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(10.1111));
                });

                it("sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountTakerCalculator);

                    // last trade amount = 12.5 - 10.1111 = 2.3889
                    // (1234.5678 * 10.1111 + 900 * 2.3889) / (10.1111 + 2.3889) = 1170.6278786064
                    expect(vwap.priceBN ).toEqual(BigNumber(1170.627));
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(12.5));
                });

                it("maker commissions, sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountMakerCalculator);

                    // last trade amount = 12.5 - 10.1111 = 2.3889
                    // (1234.5678 * 10.1111 + 900 * 2.3889) / (10.1111 + 2.3889) = 1170.6278786064
                    expect(vwap.priceBN ).toEqual(BigNumber(1170.627));
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(12.5));
                });

                it("sell amount > first and second bid amounts and < first, second and third bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};
                    testOrderBook.bids[2] = {price: 333.333, amount: 20};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(30), sellAmountTakerCalculator);

                    // last trade amount = 30 - 10.1111 - 5.2222 = 14.6667
                    // (1234.5678 * 10.1111 + 900 * 5.2222 + 333.333 * 14.6667) / (10.1111 + 5.2222 + 14.6667) = 735.723786456
                    expect(vwap.priceBN ).toEqual(BigNumber(735.723));
                });

                it("sell amount > all bids in order book", function()
                {
                    const vwap = testOrderBook.getVwap('bids', BigNumber(10000), sellAmountTakerCalculator);

                    expect(vwap instanceof Error).toBe(true);
                    expect(vwap.name).toEqual('notEnoughOrders');
                });
            });

            // TODO implement test for BuyAmountCalculator
            describe("for buy amounts on the asks side",function()
            {
                it("buy amount < first ask amount with no starting amount", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(0.4), buyAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1000));
                    // buying 0.4 BTC = 0.4 / (1 - 0.02) = 0.40816326530612246
                    // trade amount = 500 / 1000
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(0.40816327));
                });
            });

            describe("for trade amounts on the asks side", function()
            {
                it("trade amount = first order amount", function()
                {
                    const vwap = testOrderBook.getVwap('asks', BigNumber(0.0001), tradeAmountCalculator);

                    expect(vwap.priceBN).toEqual(BigNumber(1000));
                    expect(vwap.tradeAmountBN).toEqual(BigNumber(0.0001));
                });

                it("trade amount > first order amount and < first and second order amounts", function()
                {
                    const vwap = testOrderBook.getVwap('asks', BigNumber(0.001), tradeAmountCalculator);

                    // (1000 * 0.0001 + 1000.001 * 0.0009) / (0.0001 + 0.0009) = 1000.0009
                    expect(vwap.priceBN).toEqual(BigNumber(1000) );
                    expect(vwap.tradeAmountBN).toEqual(BigNumber(0.001));
                });

                it("trade amount > all order amounts", function()
                {
                    const vwap = testOrderBook.getVwap('asks', BigNumber(200), tradeAmountCalculator);

                     expect(vwap instanceof Error).toBe(true);
                     expect(vwap.name).toEqual('notEnoughOrders');
                });
            });

            describe("for trade amounts on the bids side", function()
            {
                it("trade amount < first order amount", function()
                {
                    const vwap = testOrderBook.getVwap('bids', BigNumber(0.0001), tradeAmountCalculator);

                    expect(vwap.priceBN).toEqual(BigNumber(999.999) );
                    expect(vwap.tradeAmountBN).toEqual(BigNumber(0.0001));
                });

                it("trade amount = first and second order amounts", function()
                {
                    const vwap = testOrderBook.getVwap('bids', BigNumber(0.23), tradeAmountCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(998.818) );
                    expect(vwap.tradeAmountBN).toEqual(BigNumber(0.23));
                });
            });
        });

        describe("full book of orders and sellCurrency fee structure", function()
        {
            let sellAmountTakerCalculator,
                sellAmountMakerCalculator;

            beforeEach(function()
            {
                testExchange1.feeStructure = 'sellCurrency';

                sellAmountTakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'taker');
                sellAmountMakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'maker');
            });

            describe("for sell amounts on the asks side", function()
            {
                it("sell amount < first ask amount with no starting amount", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(500), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1000));
                });

                it("sell amount = first ask amount with no starting amount", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(10555.5), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1000));
                });

                it("sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountTakerCalculator);

                    // gross sell amount = 12000 / (1 + 0.06) = 11320.7547169811
                    // first order gross sell amount = 1000 * 10.5555 = 10555.5
                    // second order gross sell amount = 1100 * 5.5555 = 6111.05
                    // sum of first and second net sell amounts = 10555.5 + 6111.05 = 16666.55
                    // last trade amount = 5.5555 * (11320.7547169811 - 10555.5) / 6111.05 = ‌0.695686106346455
                    // (1000 * 10.5555 + 1100 * 0.695686106346455) / (10.5555 + 0.695686106346455) = 1006.18322459313
                    expect(vwap.priceBN ).toEqual(BigNumber(1006.183));
                });

                it("maker commissions, sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountMakerCalculator);

                    // gross sell amount = 12000 / (1 + 0.006) = ‌11928.429423459245
                    // first order gross sell amount = 1000 * 10.5555 = 10555.5
                    // second order gross sell amount = 1100 * 5.5555 = 6111.05
                    // sum of first and second net sell amounts = 10555.5 + 6111.05 = 16666.55
                    // last trade amount = 5.5555 * (11928.42 - 10555.5) / 6111.05 = 1.248109090909091
                    // (1000 * 10.5555 + 1100 * 1.248109090909091) / (10.5555 + 1.248109090909091) = 1010.573961584939
                    expect(vwap.priceBN ).toEqual(BigNumber(1010.574));
                });

                it("sell amount > first and second ask amounts and < first, second and third ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};
                    testOrderBook.asks[2] = {price: 5000, amount: 100};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(50000), sellAmountTakerCalculator);

                    // gross sell amount = 50000 / (1 + 0.06) = 47169.811320754714
                    // first order gross sell amount = 1000 * 10.5555 = 10555.5
                    // second order gross sell amount = 1100 * 5.5555 = 6111.05
                    // third order gross sell amount = 5000 * 100 = 500000
                    // sum of first and second net sell amounts = 10555.5 + 6111.05 = 16666.55
                    // sum of first, second and third net sell amounts = 10555.5 + 6111.05 + 500000 = 516666.55
                    // last trade amount = 100 * (47169.811320754714 - 16666.55) / 500000 = 6.100652264150943
                    // (1000 * 10.5555 + 1100 * 5.5555 + 5000 * 6.100652264150943) / (10.5555 + 5.5555 + 6.100652264150943) = 2123.6516203202777
                    expect(vwap.priceBN ).toEqual(BigNumber(2123.651));
                });

                it("sell amount > all asks in order book", function()
                {
                    const vwap = testOrderBook.getVwap('asks', BigNumber(1000000), sellAmountTakerCalculator);

                    expect(vwap instanceof Error).toBe(true);
                    expect(vwap.name).toEqual('notEnoughOrders');
                });
            });

            describe("for sell amounts on the bids side", function()
            {
                it("sell amount < first bid amount with no starting amount", function()
                {
                    testOrderBook.bids[0] = {price: 1234.567, amount: 10.1111};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(5), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1234.567));
                });

                it("sell amount = first bid amount with no starting amount", function()
                {
                    testOrderBook.bids[0] = {price: 1234.567, amount: 10.1111};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(10.1111), sellAmountTakerCalculator);

                    expect(vwap.priceBN ).toEqual(BigNumber(1234.567));
                });

                it("sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountTakerCalculator);

                    // gross sell amount = 12.5 / (1 + 0.02) = 12.254901960784313
                    // last trade amount = 12.254901960784313 - 10.1111 = 2.1438019607843124
                    // (1234.5678 * 10.1111 + 900 * 2.1438019607843124) / (10.1111 + 2.1438019607843124) = 1176.04043617853
                    expect(vwap.priceBN ).toEqual(BigNumber(1176.040));
                });

                it("maker commission, sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountMakerCalculator);

                    // gross sell amount = 12.5 / (1 + 0.002) = 12.4750499001996
                    // last trade amount = 12.4750499001996 - 10.1111 = 2.3639499001996
                    // (1234.5678 * 10.1111 + 900 * 2.3639499001996) / (10.1111 + 2.3639499001996) = 1171.1691343636128
                    expect(vwap.priceBN ).toEqual(BigNumber(1171.169));
                });

                it("sell amount > first and second bid amounts and < first, second and third bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};
                    testOrderBook.bids[2] = {price: 333.333, amount: 20};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(30), sellAmountTakerCalculator);

                    // gross sell amount = 30 / (1 + 0.02) = ‌29.41176470588235
                    // last trade amount = ‌29.41176470588235 - 15.3333 = 14.078464705882352
                    // (1234.5678 * 10.1111 + 900 * 5.2222 + 333.333 * 14.078464705882352) / (10.1111 + 5.2222 + 14.078464705882352) = 735.723786456
                    expect(vwap.priceBN ).toEqual(BigNumber(743.771));
                });

                it("sell amount > all bids in order book", function()
                {
                    const vwap = testOrderBook.getVwap('bids', BigNumber(10000), sellAmountTakerCalculator);

                    expect(vwap instanceof Error).toBe(true);
                    expect(vwap.name).toEqual('notEnoughOrders');
                });
            });
        });

        describe("full book of orders and variableCurrency fee structure", function()
        {
            let sellAmountTakerCalculator,
                sellAmountMakerCalculator;

            beforeEach(function()
            {
                testExchange1.feeStructure = 'variableCurrency';

                sellAmountTakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'taker');
                sellAmountMakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'maker');
            });

            describe("for sell amounts on the asks side", function()
            {
                it("sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountTakerCalculator);

                    // same as sellCurrency
                    expect(vwap.priceBN ).toEqual(BigNumber(1006.183));
                });

                it("maker commission, sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountMakerCalculator);

                    // same as sellCurrency
                    expect(vwap.priceBN ).toEqual(BigNumber(1010.574));
                });
            });

            describe("for sell amounts on the bids side", function()
            {
                it("sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountTakerCalculator);

                    // same as buy trade
                    expect(vwap.priceBN ).toEqual(BigNumber(1170.627));
                });

                it("maker commission, sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountMakerCalculator);

                    // same as buy trade
                    expect(vwap.priceBN ).toEqual(BigNumber(1170.627));
                });
            });
        });

        describe("full book of orders and fixedCurrency fee structure", function()
        {
            let sellAmountTakerCalculator,
                sellAmountMakerCalculator;

            beforeEach(function()
            {
                testExchange1.feeStructure = 'fixedCurrency';

                sellAmountTakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'taker');
                sellAmountMakerCalculator = new SellAmountCalculator(testExchange1.markets['BTCUSD'], 'maker');
            });

            describe("for sell amounts on the asks side", function()
            {
                it("sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountTakerCalculator);

                    // same as buyCurrency
                    expect(vwap.priceBN ).toEqual(BigNumber(1011.064));
                });

                it("maker commission, sell amount > first ask amount and < first and second ask amounts", function()
                {
                    testOrderBook.asks[0] = {price: 1000, amount: 10.5555};
                    testOrderBook.asks[1] = {price: 1100, amount: 5.5555};

                    const vwap = testOrderBook.getVwap('asks', BigNumber(12000), sellAmountMakerCalculator);

                    // same as taker commission as sell are on the buy side - not the sell side
                    expect(vwap.priceBN ).toEqual(BigNumber(1011.064));
                });
            });

            describe("for sell amounts on the bids side", function()
            {
                it("sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountTakerCalculator);

                    // gross sell amount = 12.5 / (1 + 0.02) = 12.254901960784313
                    // last trade amount = 12.254901960784313 - 10.1111 = 2.1438019607843124
                    // (1234.5678 * 10.1111 + 900 * 2.1438019607843124) / (10.1111 + 2.1438019607843124) = 1176.04043617853
                    expect(vwap.priceBN ).toEqual(BigNumber(1176.040));
                    // 10.1111 + 2.1438019607843124 = ‌12.254901960784313
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(12.25490196));
                });

                it("maker commission, sell amount > first bid amount and < first and second bid amounts", function()
                {
                    testOrderBook.bids[0] = {price: 1234.5678, amount: 10.1111};
                    testOrderBook.bids[1] = {price: 900, amount: 5.2222};

                    const vwap = testOrderBook.getVwap('bids', BigNumber(12.5), sellAmountMakerCalculator);

                    // gross sell amount = 12.5 / (1 + 0.002) = 12.4750499001996
                    // last trade amount = 12.4750499001996 - 10.1111 = 2.3639499001996
                    // (1234.5678 * 10.1111 + 900 * 2.3639499001996) / (10.1111 + 2.3639499001996) = 1171.1691343636128
                    expect(vwap.priceBN ).toEqual(BigNumber(1171.169));
                    // 10.1111 + 2.3639499001996 = ‌12.4750499001996
                    expect(vwap.tradeAmountBN ).toEqual(BigNumber(12.4750499));
                });
            });
        });
    });
});