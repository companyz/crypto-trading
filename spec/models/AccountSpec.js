/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
var balance_1 = require('../../models/balance');
var account_1 = require('../../models/account');
var order_1 = require('../../models/order');
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
describe("Account: ", function () {
    // Define test exchange data rather than using live exchange config that can change
    var testExchange1 = new TestExchange_1.default({
        name: 'testExchange1',
        fixedCurrencies: ["LTC", "BTC"],
        variableCurrencies: ["AUD", "EUR", "USD"],
        commissions: { "LTC": 0.1, "BTC": 0.02, "AUD": 0.01, "USD": 0.06, "EUR": 0.03 },
        confirmationTime: 1,
        defaultPriceRounding: 2,
        defaultMinAmount: 0.1,
        currencyRounding: { "BTC": 8, "LTC": 8, "USD": 8, "AUD": 8, "EUR": 2 }
    });
    var testExchange2 = new TestExchange_1.default({
        name: 'testExchange2',
        fixedCurrencies: ["BTC"],
        variableCurrencies: ["EUR", "USD"],
        commissions: { "BTC": 0.01, "AUD": 0.03, "USD": 0.04 },
        confirmationTime: 20,
        defaultPriceRounding: 3,
        defaultMinAmount: 0.001,
        currencyRounding: { "BTC": 4, "USD": 3, "EUR": 2 }
    });
    // override the global exchanges variable for testing purposes
    exchange_1.default.exchanges = {
        'testExchange1': testExchange1,
        'testExchange2': testExchange2 };
    var testBTCBalance, testUSDBalance, testAUDBalance, testAccount;
    beforeEach(function () {
        testBTCBalance = new balance_1.default({
            exchangeName: 'testExchange1',
            currency: 'BTC',
            totalBalance: '9.87654321',
            availableBalance: '8.12345678'
        });
        testUSDBalance = new balance_1.default({
            exchangeName: 'testExchange1',
            currency: 'USD',
            totalBalance: '222.22',
            availableBalance: '40.99'
        });
        testAUDBalance = new balance_1.default({
            exchangeName: 'testExchange1',
            currency: 'AUD',
            totalBalance: '333.33',
            availableBalance: '300.66'
        });
        testAccount = new account_1.default([testBTCBalance, testUSDBalance, testAUDBalance], 'testExchange1', testExchange1.currencyRounding);
    });
    it("Initialising", function () {
        expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
        expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
        expect(testAccount.balances['USD'].totalBalance).toEqual('222.22');
        expect(testAccount.balances['USD'].availableBalance).toEqual('40.99');
    });
    describe("setBalance()", function () {
        it("of new currency", function () {
            testAccount.setBalance(testAUDBalance);
            expect(testAccount.balances['AUD'].totalBalance).toEqual('333.33');
            expect(testAccount.balances['AUD'].availableBalance).toEqual('300.66');
        });
        it("of existing currency", function () {
            var updatedBTCBalance = new balance_1.default({
                exchangeName: 'testExchange1',
                currency: 'BTC',
                totalBalance: '11.87654321',
                availableBalance: '11.12345678'
            });
            testAccount.setBalance(updatedBTCBalance);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('11.87654321');
            expect(testAccount.balances['BTC'].availableBalance).toEqual('11.12345678');
            //check the clone in the setBalance worked
            expect(testBTCBalance.totalBalance).toEqual('9.87654321');
            expect(testBTCBalance.availableBalance).toEqual('8.12345678');
        });
    });
    describe("add buy limit order", function () {
        it("of existing currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '1',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.addOrder(newOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
            expect(testAccount.balances['USD'].totalBalance).toEqual('222.22');
            //40.99 - (0.1 * 1000) = 30.99
            expect(testAccount.balances['USD'].availableBalance).toEqual('30.99');
        });
        it("of new currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '2',
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'buy',
                state: 'quoted',
                amount: '1',
                amountTraded: '0',
                amountRemaining: '1',
                price: '1000'
            });
            testAccount.addOrder(newOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
            expect(testAccount.balances['EUR'].totalBalance).toEqual('0');
            //0 - (1 * 1000) = -1000
            expect(testAccount.balances['EUR'].availableBalance).toEqual('-1000');
        });
    });
    describe("add sell limit order", function () {
        it("of existing currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '1',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.addOrder(newOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            //8.12345678 - 0.01 = 8.11345678
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.11345678');
            expect(testAccount.balances['USD'].totalBalance).toEqual('222.22');
            expect(testAccount.balances['USD'].availableBalance).toEqual('40.99');
        });
        it("of new currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '2',
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'sell',
                state: 'quoted',
                amount: '1',
                amountTraded: '0',
                amountRemaining: '1',
                price: '1000'
            });
            testAccount.addOrder(newOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            //8.12345678 - 1 = 7.12345678
            expect(testAccount.balances['BTC'].availableBalance).toEqual('7.12345678');
            expect(testAccount.balances['EUR'].totalBalance).toEqual('0');
            expect(testAccount.balances['EUR'].availableBalance).toEqual('0');
        });
    });
    describe("add buy market order", function () {
        it("of existing currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '1',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'filled',
                amount: '0.5',
                amountTraded: '0.5',
                amountRemaining: '0',
                amountLastPartial: '0.5',
                numberPartialFills: 1,
                price: '200'
            });
            testAccount.addMarketTrade(newOrder);
            // 9.87654321 + 0.5 * (1 - 0.02)
            expect(testAccount.balances['BTC'].totalBalance).toEqual('10.36654321');
            // 8.12345678 + 0.5 * (1 - 0.02)
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.61345678');
            // 222.22 - (0.5 * 200)
            expect(testAccount.balances['USD'].totalBalance).toEqual('122.22');
            // 40.99 - (0.5 * 200)
            expect(testAccount.balances['USD'].availableBalance).toEqual('-59.01');
        });
        it("of new currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '2',
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'buy',
                state: 'filled',
                amount: '2',
                amountTraded: '2',
                amountRemaining: '0',
                numberPartialFills: 1,
                price: '200'
            });
            testAccount.addMarketTrade(newOrder);
            // 9.87654321 + 2 * (1 - 0.02)
            expect(testAccount.balances['BTC'].totalBalance).toEqual('11.83654321');
            // 8.12345678 + 2 * (1 - 0.02)
            expect(testAccount.balances['BTC'].availableBalance).toEqual('10.08345678');
            // 222.22 - (0.5 * 200)
            expect(testAccount.balances['EUR'].totalBalance).toEqual('-400');
            // 40.99 - (0.5 * 200)
            expect(testAccount.balances['EUR'].availableBalance).toEqual('-400');
        });
    });
    describe("cancel buy order", function () {
        it("of existing currency", function () {
            var cancelledOrder = new order_1.default({
                exchangeId: '3',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.cancelOrder(cancelledOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
            expect(testAccount.balances['USD'].totalBalance).toEqual('222.22');
            //40.99 + (0.01 * 1000) = 50.99
            expect(testAccount.balances['USD'].availableBalance).toEqual('50.99');
        });
        it("of new currency", function () {
            var cancelledOrder = new order_1.default({
                exchangeId: '4',
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'buy',
                state: 'quoted',
                amount: '1',
                amountTraded: '0',
                amountRemaining: '1',
                price: '1000'
            });
            testAccount.cancelOrder(cancelledOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
            expect(testAccount.balances['EUR'].totalBalance).toEqual('0');
            //0 + (1 * 1000) = 1000
            expect(testAccount.balances['EUR'].availableBalance).toEqual('1000');
        });
    });
    describe("cancelled sell order", function () {
        it("of existing currency", function () {
            var cancelledOrder = new order_1.default({
                exchangeId: '5',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.cancelOrder(cancelledOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            //8.12345678 + 0.01 = 8.13345678
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.13345678');
            expect(testAccount.balances['USD'].totalBalance).toEqual('222.22');
            expect(testAccount.balances['USD'].availableBalance).toEqual('40.99');
        });
        it("of new currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '6',
                exchangeName: 'testExchange1',
                symbol: 'BTCEUR',
                side: 'sell',
                state: 'quoted',
                amount: '1',
                amountTraded: '0',
                amountRemaining: '1',
                price: '1000'
            });
            testAccount.cancelOrder(newOrder);
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.87654321');
            //8.12345678 + 1 = 9.12345678
            expect(testAccount.balances['BTC'].availableBalance).toEqual('9.12345678');
            expect(testAccount.balances['EUR'].totalBalance).toEqual('0');
            expect(testAccount.balances['EUR'].availableBalance).toEqual('0');
        });
    });
    describe("confirm buy order", function () {
        it("of existing currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '7',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.fillOrder(newOrder);
            // 9.98654321 + 0.01 * (1 - 0.02)
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.88634321');
            // 8.13345678 + 0.01 * (1 - 0.02)
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.13325678');
            // 212.22 - (0.01 * 1000) = 202.22
            expect(testAccount.balances['USD'].totalBalance).toEqual('212.22');
            // add order would have already reduce the available balance so no change on fill
            expect(testAccount.balances['USD'].availableBalance).toEqual('40.99');
        });
    });
    describe("fill sell order", function () {
        it("of existing currency", function () {
            var newOrder = new order_1.default({
                exchangeId: '8',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'quoted',
                amount: '0.01',
                amountTraded: '0',
                amountRemaining: '0.01',
                price: '1000'
            });
            testAccount.fillOrder(newOrder);
            // 9.87654321 - 0.01
            expect(testAccount.balances['BTC'].totalBalance).toEqual('9.86654321');
            // add order would have already reduce the available balance so no change on fill
            expect(testAccount.balances['BTC'].availableBalance).toEqual('8.12345678');
            // 222.22 + (0.01 * 1000) * (1 - 0.06) = 231.62
            expect(testAccount.balances['USD'].totalBalance).toEqual('231.62');
            // 40.99 + (0.01 * 1000) * (1 - 0.06) = 50.39
            expect(testAccount.balances['USD'].availableBalance).toEqual('50.39');
        });
    });
});
