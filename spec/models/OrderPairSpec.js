/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
// My packages
var TestExchange_1 = require('../../exchanges/TestExchange');
var exchange_1 = require('../../models/exchange');
var order_1 = require('../../models/order');
var orderPair_1 = require('../../models/orderPair');
var BigNumber = require('bignumber.js');
describe("OrderPair: ", function () {
    var leadingOrder, trailingOrder, trailingTrade1, trailingTrade2, trailingTrade3;
    // Define test exchanges data rather than using live config that can change
    var testExchange1 = new TestExchange_1.default({
        name: 'testExchange1',
        fixedCurrencies: ["LTC", "BTC"],
        variableCurrencies: ["AUD", "EUR", "USD"],
        commissions: { LTC: 0.01, BTC: 0.001, AUD: 0.002, USD: 0.003, EUR: 0.004 },
        defaultPriceRounding: 4,
        defaultMinAmount: 0.001,
        confirmationTime: 1,
        currencyRounding: { BTC: 5, LTC: 8, USD: 4, AUD: 3, EUR: 4 }
    });
    var testExchange2 = new TestExchange_1.default({
        name: 'testExchange2',
        fixedCurrencies: ["BTC"],
        variableCurrencies: ["EUR", "USD"],
        commissions: { BTC: 0.01, AUD: 0.003, USD: 0.001 },
        defaultPriceRounding: 5,
        defaultMinAmount: 0.01,
        confirmationTime: 20,
        currencyRounding: { BTC: 8, USD: 8, EUR: 2 }
    });
    // override the global exchanges object
    exchange_1.default.exchanges = {
        'testExchange1': testExchange1,
        'testExchange2': testExchange2
    };
    describe("Negative tests", function () {
        beforeEach(function () {
            leadingOrder = new order_1.default({
                exchangeId: '1001',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'filled',
                amount: '1.1',
                amountTraded: '1.0',
                amountRemaining: '0.1',
                amountLastPartial: '0.8',
                numberPartialFills: 3,
                price: '1000'
            });
            trailingOrder = new order_1.default({
                exchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'partiallyFilled',
                amount: '0.8',
                amountTraded: '0.8',
                amountRemaining: '0',
                amountLastPartial: '0.8',
                numberPartialFills: 1,
                price: '996'
            });
        });
        //it("no symbol parameter passed on initiation", function()
        //{
        //    const testFunction = function()
        //    {
        //        const testOrderPair = new OrderPair();
        //    };
        //
        //    expect(testFunction).toThrow();
        //});
        it("adding a trailing trade before a leading trade has been set", function () {
            var testFunction = function () {
                var testOrderPair = new orderPair_1.default('BTCUSD');
                testOrderPair.addTrailingOrder(leadingOrder);
            };
            expect(testFunction).toThrow();
        });
        it("get return before adding the leading trade", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            var varReturn = testOrderPair.getNetVariableReturn();
            expect(varReturn instanceof Error).toBe(true);
        });
    });
    describe("leading sell, trailing buy", function () {
        beforeEach(function () {
            leadingOrder = new order_1.default({
                exchangeId: '1001',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'filled',
                amount: '1.1',
                amountTraded: '1.0',
                amountRemaining: '0.1',
                amountLastPartial: '0.8',
                numberPartialFills: 3,
                price: '1000'
            });
            // 0.8 / (1 - 0.001) = 0.800800800800801
            trailingOrder = new order_1.default({
                exchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'filled',
                amount: '0.8008',
                amountTraded: '0.8008',
                amountRemaining: '0',
                amountLastPartial: '0.8008',
                numberPartialFills: 1,
                price: '996'
            });
            trailingTrade1 = new order_1.default({
                exchangeId: '1003',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'partiallyFilled',
                amount: '0.8',
                amountTraded: '0.3',
                amountRemaining: '0.5',
                amountLastPartial: '0.3',
                numberPartialFills: 1,
                price: '990'
            });
            trailingTrade2 = new order_1.default({
                exchangeId: '1004',
                originalExchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'partiallyFilled',
                amount: '0.8',
                amountTraded: '0.5',
                amountRemaining: '0.3',
                amountLastPartial: '0.2',
                numberPartialFills: 2,
                price: '992'
            });
            trailingTrade3 = new order_1.default({
                exchangeId: '1005',
                originalExchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'filled',
                amount: '0.8',
                amountTraded: '0.8',
                amountRemaining: '0',
                amountLastPartial: '0.3',
                numberPartialFills: 3,
                price: '995'
            });
        });
        it("adding a leading sell trade", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            expect(testOrderPair.leadingOrder).toBe(leadingOrder);
            // buy amount
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(800));
            // 800 * (1 - 0.003)
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(797.6));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.8));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.8));
            // 800 / 0.8
            expect(testOrderPair.aggregatedSellOrders.priceGrossBN).toEqual(BigNumber(1000));
            // 797.6 / 0.8
            expect(testOrderPair.aggregatedSellOrders.priceNetBN).toEqual(BigNumber(997));
            // 0 / 0.8 = 0
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(0));
        });
        it("adding one trailing buy trade with last partial trade amount = leading sell trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingOrder);
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.8008));
            // 0.80808 * (1 - 0.001) = 0.7999992
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.79999));
            // 0.8008 * 996 = 797.5968
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(797.5968));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(797.5968));
            // 796.8 / 0.8
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(996));
            // 796.8 / 0.7992
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(996));
            // aggregated sell trade values should not have changed from when the leading trade was added
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(800));
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(797.6));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.8));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.8));
            // 0.79999 / 0.8 = ‌0.9999874999999999
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN().toString()).toEqual(BigNumber(1).toString());
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(100));
        });
        it("adding one trailing buy trade with last partial trade amount < leading sell trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingTrade1);
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.3));
            // 0.3 * (1 - 0.001)
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.2997));
            // 0.3 * 990
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(297));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(297));
            // 297 / 0.3
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(990));
            // 297 / 0.2997 = ‌990.9909909909909
            expect(testOrderPair.aggregatedBuyOrders.priceNetBN).toEqual(BigNumber(990.991));
        });
        it("adding two trailing buy trades with total last partial trade amount < leading sell trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingTrade1);
            testOrderPair.addTrailingOrder(trailingTrade2);
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.5));
            // 0.5 * (1 - 0.001)
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.4995));
            // 0.3 * 990 + 0.2 * 992
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(495.4));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(495.4));
            // 495.4 / 0.5
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(990.8));
            // 495.4 / 0.4995
            expect(testOrderPair.aggregatedBuyOrders.priceNetBN).toEqual(BigNumber(991.7918));
        });
        it("adding three trailing buy trades with total last partial trade amount = leading sell trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingTrade1);
            testOrderPair.addTrailingOrder(trailingTrade2);
            testOrderPair.addTrailingOrder(trailingTrade3);
            // 0.3 + 0.2 + 0.3
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.8));
            // 0.8 * (1 - 0.001)
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.7992));
            // 0.3 * 990 + 0.2 * 992 + 0.3 * 995
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(793.9));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(793.9));
            // 793.9 / 0.8
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(992.375));
            // 793.9 / 0.7992
            expect(testOrderPair.aggregatedBuyOrders.priceNetBN).toEqual(BigNumber(993.3684));
            // gross variable return
            // 800 - 793.9 = 6.1
            expect(testOrderPair.getGrossVariableReturn().amountBN()).toEqual(BigNumber(6.1));
            // (800 - 793.9) / 793.9 = 0.00768358735357101
            expect(testOrderPair.getGrossVariableReturn().decimalBN()).toEqual(BigNumber(0.007684));
            // (800 - 793.9) / 793.9 * 100 = 0.77
            expect(testOrderPair.getGrossVariableReturn().decimalBN()).toEqual(BigNumber(0.007684));
            // net variable return
            // 797.6 - 793.9 = 3.7
            expect(testOrderPair.getNetVariableReturn().amountBN()).toEqual(BigNumber(3.7));
            // (797.6 - 793.9) / 793.9 = 0.00466053659151032
            expect(testOrderPair.getNetVariableReturn().decimalBN()).toEqual(BigNumber(0.004661));
            // (797.6 - 793.9) / 793.9 * 100 = 0.466053659151032
            expect(testOrderPair.getNetVariableReturn().percentageBN()).toEqual(BigNumber(0.47));
            // gross fixed return
            // 0.8 - 0.8 = 0
            expect(testOrderPair.getGrossFixedReturn().amountBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getGrossFixedReturn().decimalBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getGrossFixedReturn().percentageBN()).toEqual(BigNumber(0));
            // gross fixed return
            // 0.8 - 0.7992 = 0.0008
            expect(testOrderPair.getNetFixedReturn().amountBN()).toEqual(BigNumber(0.0008));
            // (0.8 - 0.7992) / 0.7992 = 0.00100100100100103
            expect(testOrderPair.getNetFixedReturn().decimalBN()).toEqual(BigNumber(0.001001));
            // (0.8 - 0.7992) / 0.7992 * 100 = 0.100100100100103
            expect(testOrderPair.getNetFixedReturn().percentageBN()).toEqual(BigNumber(0.1));
            // gross price return
            // 1000 - 992.375 = 7.625
            expect(testOrderPair.getGrossPriceReturn().amountBN()).toEqual(BigNumber(7.625));
            // (1000 - 992.375) / 992.375 = 0.00768358735357098
            expect(testOrderPair.getGrossPriceReturn().decimalBN()).toEqual(BigNumber(0.007684));
            // (1000 - 992.375) / 992.375 * 100 = 0.768358735357098
            expect(testOrderPair.getGrossPriceReturn().percentageBN()).toEqual(BigNumber(0.77));
            // net price return
            // 997 - 993.3684 = 3.63160000000005
            expect(testOrderPair.getNetPriceReturn().amountBN()).toEqual(BigNumber(3.6316));
            // (997 - 993.3684) / 993.3684 = 0.00365584409570513
            expect(testOrderPair.getNetPriceReturn().decimalBN()).toEqual(BigNumber(0.003656));
            // (997 - 993.3684) / 993.3684 * 100 = 0.365584409570513
            expect(testOrderPair.getNetPriceReturn().percentageBN()).toEqual(BigNumber(0.37));
        });
    });
    describe("leading buy, trailing sell", function () {
        beforeEach(function () {
            leadingOrder = new order_1.default({
                exchangeId: '1001',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'filled',
                amount: '0.1',
                amountTraded: '0.8',
                amountRemaining: '0.2',
                amountLastPartial: '0.5',
                numberPartialFills: 2,
                price: '1000'
            });
            trailingOrder = new order_1.default({
                exchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'filled',
                amount: '0.5',
                amountTraded: '0.5',
                amountRemaining: '0',
                amountLastPartial: '0.5',
                numberPartialFills: 1,
                price: '1010'
            });
            trailingTrade1 = new order_1.default({
                exchangeId: '1003',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'partiallyFilled',
                amount: '0.5',
                amountTraded: '0.1',
                amountRemaining: '0.4',
                amountLastPartial: '0.1',
                numberPartialFills: 1,
                price: '1005'
            });
            trailingTrade2 = new order_1.default({
                exchangeId: '1004',
                originalExchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'partiallyFilled',
                amount: '0.5',
                amountTraded: '0.3',
                amountRemaining: '0.2',
                amountLastPartial: '0.2',
                numberPartialFills: 2,
                price: '1007.5'
            });
            trailingTrade3 = new order_1.default({
                exchangeId: '1005',
                originalExchangeId: '1002',
                exchangeName: 'testExchange1',
                symbol: 'BTCUSD',
                side: 'sell',
                state: 'filled',
                amount: '0.8',
                amountTraded: '0.8',
                amountRemaining: '0',
                amountLastPartial: '0.2',
                numberPartialFills: 3,
                price: '1015.05'
            });
        });
        it("adding a leading buy trade", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            expect(testOrderPair.leadingOrder).toBe(leadingOrder);
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.5));
            // 0.5 * (1 - 0.001)
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.4995));
            // 0.5 * 1000
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(500));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(500));
            // 500 / 0.5
            expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(1000));
            // 500 / 0.4995
            expect(testOrderPair.aggregatedBuyOrders.priceNetBN).toEqual(BigNumber(1001.001));
            // 0 / 0.4995 = 0
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(0));
        });
        it("adding one trailing sell trade with last partial trade amount = leading buy trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingOrder);
            // 1010 * 0.5
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(505));
            // 1010 * 0.5 * (1 - 0.003) = 503.485
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(503.485));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.5));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.5));
            // 505 / 0.5
            expect(testOrderPair.aggregatedSellOrders.priceGrossBN).toEqual(BigNumber(1010));
            // 503.485 / 0.5 = 1006.97
            expect(testOrderPair.aggregatedSellOrders.priceNetBN).toEqual(BigNumber(1006.97));
            // aggregated buy trade values should not have changed from when the leading trade was added
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(500));
            expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(500));
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.5));
            expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.4995));
            // 0.5 / 0.4995 = 1.001001001001 = 1 rounded
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN()).toEqual(BigNumber(1.001));
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(100.1));
        });
        it("adding one trailing sell trade with last partial trade amount < leading buy trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingTrade1);
            // 1005 * 0.1
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(100.5));
            // 1005 * 0.1 * (1 - 0.003) = 100.1985
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(100.1985));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.1));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.1));
            // 100.5 / 0.1
            expect(testOrderPair.aggregatedSellOrders.priceGrossBN).toEqual(BigNumber(1005));
            // 100.1985 / 0.1 = 1001.985
            expect(testOrderPair.aggregatedSellOrders.priceNetBN).toEqual(BigNumber(1001.985));
            // 0.1 / 0.4995 = 0.2002002002002 = 0.2002 rounded
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN()).toEqual(BigNumber(0.2002));
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(20.02));
        });
        it("adding three trailing sell trades with total last partial trade amount = leading buy trade last partial amount", function () {
            var testOrderPair = new orderPair_1.default('BTCUSD');
            testOrderPair.addLeadingOrder(leadingOrder);
            testOrderPair.addTrailingOrder(trailingTrade1);
            testOrderPair.addTrailingOrder(trailingTrade2);
            testOrderPair.addTrailingOrder(trailingTrade3);
            // 0.1 * 1005 + 0.2 * 1007.5 + 0.2 * 1015.05
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(505.01));
            // 0.1 * 1005 * (1 - 0.003) = 100.1985
            // 0.2 * 1007.5 * (1 - 0.003) = 200.8955
            // 0.2 * 1015.05 * (1 - 0.003) = 202.40097
            // Total 100.1985 + 200.8955 + 202.40097 = 503.49497
            expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(503.4949));
            // 0.1 + 0.2 + 0.2
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(0.5));
            expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.5));
            // 505.01 / 0.5
            expect(testOrderPair.aggregatedSellOrders.priceGrossBN).toEqual(BigNumber(1010.02));
            // 503.4949 / 0.5 = 1006.9898
            expect(testOrderPair.aggregatedSellOrders.priceNetBN).toEqual(BigNumber(1006.9898));
            // 0.5 / 0.4995 = 1.001001001001
            expect(testOrderPair.getFixedCurrencyReturnedDecimalBN()).toEqual(BigNumber(1.001));
            expect(testOrderPair.getFixedCurrencyReturnedPercentageBN()).toEqual(BigNumber(100.1));
            // gross variable return
            // 505.01 - 500 = 5.01
            expect(testOrderPair.getGrossVariableReturn().amountBN()).toEqual(BigNumber(5.01));
            // (505.01 - 500) / 500 = 0.01002
            expect(testOrderPair.getGrossVariableReturn().decimalBN()).toEqual(BigNumber(0.01002));
            // (505.01 - 500) / 500 * 100 = 1.002
            expect(testOrderPair.getGrossVariableReturn().percentageBN()).toEqual(BigNumber(1));
            // net variable return
            // 503.4949 - 500 = 3.49489999999997
            expect(testOrderPair.getNetVariableReturn().amountBN()).toEqual(BigNumber(3.4949));
            // (503.4949 - 500) / 500 = 0.00698979999999995
            expect(testOrderPair.getNetVariableReturn().decimalBN()).toEqual(BigNumber(0.00699));
            // (503.4949 - 500) / 500 * 100 = 0.696000000000004
            expect(testOrderPair.getNetVariableReturn().percentageBN()).toEqual(BigNumber(0.7));
            // gross fixed return
            // 0.5 - 0.5 = 0
            expect(testOrderPair.getGrossFixedReturn().amountBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getGrossFixedReturn().decimalBN()).toEqual(BigNumber(0));
            expect(testOrderPair.getGrossFixedReturn().percentageBN()).toEqual(BigNumber(0));
            // net fixed return
            // 0.5 - 0.4995 = 0.0005
            expect(testOrderPair.getNetFixedReturn().amountBN()).toEqual(BigNumber(0.0005));
            // (0.5 - 0.4995) / 0.4995 = 0.001001001001001
            expect(testOrderPair.getNetFixedReturn().decimalBN()).toEqual(BigNumber(0.001001));
            // (0.5 - 0.4995) / 0.4995 * 100 = 0.1001001001001
            expect(testOrderPair.getNetFixedReturn().percentageBN()).toEqual(BigNumber(0.1));
            // gross price return
            // 1010.02 - 1000
            expect(testOrderPair.getGrossPriceReturn().amountBN()).toEqual(BigNumber(10.02));
            // 1010.02 - 1000 / 1000 = 0.01002
            expect(testOrderPair.getGrossPriceReturn().decimalBN()).toEqual(BigNumber(0.01002));
            // 1010.02 - 1000 / 1000 * 100 = 0.01002
            expect(testOrderPair.getGrossPriceReturn().percentageBN()).toEqual(BigNumber(1));
            // net price return
            // 1006.9898 - 1001.001 = 5.98879999999997
            expect(testOrderPair.getNetPriceReturn().amountBN()).toEqual(BigNumber(5.9888));
            // (1006.9898 - 1001.001) / 1001.001 = 0.00598281120598278
            expect(testOrderPair.getNetPriceReturn().decimalBN()).toEqual(BigNumber(0.005983));
            expect(testOrderPair.getNetPriceReturn().percentageBN()).toEqual(BigNumber(0.6));
        });
    });
    describe("cross currency (leading sell in BTCAUD and trailing in buy BTCUSD)", function () {
        beforeEach(function () {
            leadingOrder = new order_1.default({
                exchangeId: '9001',
                exchangeName: 'testExchange1',
                symbol: 'BTCAUD',
                side: 'sell',
                state: 'filled',
                price: '500',
                amount: '1.9999',
                amountTraded: '1',
                amountRemaining: '0.9999',
                amountLastPartial: '1',
                numberPartialFills: 1,
                tag: 'leadingBTCAUD'
            });
            trailingOrder = new order_1.default({
                exchangeId: '9002',
                exchangeName: 'testExchange2',
                symbol: 'BTCUSD',
                side: 'buy',
                state: 'filled',
                price: '400',
                amount: '1.001',
                amountTraded: '1.001',
                amountRemaining: '0',
                amountLastPartial: '1.001',
                numberPartialFills: 1,
                tag: 'trailingBTCUSD'
            });
        });
        describe("leading sell exchange has buyCurrency fee structure", function () {
            var testOrderPair;
            beforeEach(function () {
                testExchange1.feeStructure = 'buyCurrency';
                testExchange2.feeStructure = 'buyCurrency';
                testOrderPair = new orderPair_1.default('BTCAUD', 'BTCUSD', 1.2);
                testOrderPair.addLeadingOrder(leadingOrder);
                testOrderPair.addTrailingOrder(trailingOrder);
            });
            it("properties", function () {
                expect(testOrderPair.leadingSymbol).toBe('BTCAUD');
                expect(testOrderPair.trailingSymbol).toBe('BTCUSD');
                expect(testOrderPair.leadingVariableCurrency).toBe('AUD');
                expect(testOrderPair.trailingVariableCurrency).toBe('USD');
                expect(testOrderPair.trailing2LeadingExchangeRate).toBe(1.2);
                expect(testOrderPair.leadingOrder).toBe(leadingOrder);
                expect(testOrderPair.trailingOrders.length).toBe(1);
                expect(testOrderPair.trailingOrders[0]).toBe(trailingOrder);
            });
            it("aggregated buy trades", function () {
                // aggregatedBuyTrades values which is the trailing BTCUSD trade
                expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(1.001));
                // 1.001 * (1 - 0.01) = 0.99099
                expect(testOrderPair.aggregatedBuyOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(0.99099));
                // 1.001 * 400 = 500.5
                expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(400.4));
                expect(testOrderPair.aggregatedBuyOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(400.4));
                // 400.4 / 1.001 = 400
                expect(testOrderPair.aggregatedBuyOrders.priceGrossBN).toEqual(BigNumber(400));
                // 400.4 / 0.99099 = 404.040404040404
                expect(testOrderPair.aggregatedBuyOrders.priceNetBN).toEqual(BigNumber(404.0404));
            });
            it("aggregated sell trades", function () {
                // aggregatedSellTrades values which is the leading BTCAUD trade
                expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialGrossBN).toEqual(BigNumber(500));
                // 500 * (1 - 0.002) = 499
                expect(testOrderPair.aggregatedSellOrders.sumVariableAmountLastPartialBN).toEqual(BigNumber(499));
                //
                expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialGrossBN).toEqual(BigNumber(1));
                expect(testOrderPair.aggregatedSellOrders.sumFixedAmountLastPartialBN).toEqual(BigNumber(1));
                // 500 / 1 = 400
                expect(testOrderPair.aggregatedSellOrders.priceGrossBN).toEqual(BigNumber(500));
                // 499 / 1 = 499
                expect(testOrderPair.aggregatedSellOrders.priceNetBN).toEqual(BigNumber(499));
            });
            it("aggregated variable currency returns", function () {
                // gross variable return in USD
                // 500 * 1 / 1.2 - 400.4 = 16.2666666666667 USD
                expect(testOrderPair.getGrossVariableReturn().amountBN('USD')).toEqual(BigNumber(16.26666667));
                // (500 * 1 / 1.2 - 400.4) / 400.4 = 0.0406260406260407
                expect(testOrderPair.getGrossVariableReturn().decimalBN('USD')).toEqual(BigNumber(0.040626));
                // (500 * 1 / 1.2 - 400.4) / 400.4 * 100 = 4.06260406260407
                expect(testOrderPair.getGrossVariableReturn().percentageBN('USD')).toEqual(BigNumber(4.06));
                // gross variable return in AUD
                // 500 - 400.4 * 1.2 = 19.52 AUD
                expect(testOrderPair.getGrossVariableReturn().amountBN('AUD')).toEqual(BigNumber(19.52));
                // (500 - 400.4 * 1.2) / (400.4 * 1.2) = 0.0406260406260407
                expect(testOrderPair.getGrossVariableReturn().decimalBN('AUD')).toEqual(BigNumber(0.040626));
                // (500 - 400.4 * 1.2) / (400.4 * 1.2) * 100 = 4.06260406260407
                expect(testOrderPair.getGrossVariableReturn().percentageBN('AUD')).toEqual(BigNumber(4.06));
                // net variable return in USD
                // 499 * 1 / 1.2 - 400.4 = 15.4333333333334 USD
                expect(testOrderPair.getNetVariableReturn().amountBN('USD')).toEqual(BigNumber(15.43333333));
                // (499 * 1 / 1.2 - 400.4) / 400.4 = 0.0385447885447887
                expect(testOrderPair.getNetVariableReturn().decimalBN('USD')).toEqual(BigNumber(0.038545));
                expect(testOrderPair.getNetVariableReturn().percentageBN('USD')).toEqual(BigNumber(3.85));
                // net variable return in AUD
                // 499 - 400.4 * 1.2 = 18.52 AUD
                expect(testOrderPair.getNetVariableReturn().amountBN('AUD')).toEqual(BigNumber(18.52));
                expect(testOrderPair.getNetVariableReturn().decimalBN('AUD')).toEqual(BigNumber(0.038545));
                expect(testOrderPair.getNetVariableReturn().percentageBN('AUD')).toEqual(BigNumber(3.85));
            });
            it("aggregated fixed currency returns", function () {
                // gross fixed return
                // 1 - 1.001 = -0.00099999999999989
                expect(testOrderPair.getGrossFixedReturn().amountBN()).toEqual(BigNumber(-0.001));
                // (1 - 1.001) / 1.001 = -0.000999000999000889
                expect(testOrderPair.getGrossFixedReturn().decimalBN()).toEqual(BigNumber(-0.000999));
                // TODO for some reason matching BigNumber(-0.1) to BigNumber(-0.1) does not work. Work around is to convert to string before matching
                expect(testOrderPair.getGrossFixedReturn().percentageBN().toString()).toEqual('-0.1');
                // net fixed return
                // 1 - 0.99099 = 0.00900999999999996
                expect(testOrderPair.getNetFixedReturn().amountBN()).toEqual(BigNumber(0.00901));
                // (1 - 0.99099) / 0.99099 = 0.00909191818282724
                expect(testOrderPair.getNetFixedReturn().decimalBN()).toEqual(BigNumber(0.009092));
                expect(testOrderPair.getNetFixedReturn().percentageBN()).toEqual(BigNumber(0.91));
            });
            it("aggregated price returns", function () {
                // gross price return in USD
                // 500 / 1.2 - 400 = 16.6666666666667
                expect(testOrderPair.getGrossPriceReturn().amountBN('USD')).toEqual(BigNumber(16.66666667));
                // (500 / 1.2 - 400) / 400 = 0.0416666666666667
                expect(testOrderPair.getGrossPriceReturn().decimalBN('USD')).toEqual(BigNumber(0.041667));
                // (500 / 1.2 - 400) / 400 * 100 = 4.16666666666667
                expect(testOrderPair.getGrossPriceReturn().percentageBN('USD')).toEqual(BigNumber(4.17));
                // gross price return in AUD
                // 500 - 400 * 1.2 = 20
                expect(testOrderPair.getGrossPriceReturn().amountBN('AUD')).toEqual(BigNumber(20));
                // (500 - 400 * 1.2) / (400 * 1.2) = 0.0416666666666667
                expect(testOrderPair.getGrossPriceReturn().decimalBN('AUD')).toEqual(BigNumber(0.041667));
                // (500 - 400 * 1.2) / (400 * 1.2) * 100 = 4.16666666666667
                expect(testOrderPair.getGrossPriceReturn().percentageBN('AUD')).toEqual(BigNumber(4.17));
                // net price return in USD
                // 499 / 1.2 - 404.0404 = 11.7929333333334
                expect(testOrderPair.getNetPriceReturn().amountBN('USD')).toEqual(BigNumber(11.79293333));
                // (499 / 1.2 - 404.0404) / 404.0404 = 0.0291875102918753
                expect(testOrderPair.getNetPriceReturn().decimalBN('USD')).toEqual(BigNumber(0.029188));
                // (499 / 1.2 - 404.04042) / 404.0404 * 100 = 2.91874607918747
                expect(testOrderPair.getNetPriceReturn().percentageBN('USD')).toEqual(BigNumber(2.92));
                // net price return in AUD
                // 499 - 404.0404 * 1.2 = 14.1515200000001
                expect(testOrderPair.getNetPriceReturn().amountBN('AUD')).toEqual(BigNumber(14.15152));
                // (499 - 404.0404 * 1.2) / (404.0404 * 1.2) = 0.0291875102918752
                expect(testOrderPair.getNetPriceReturn().decimalBN('AUD')).toEqual(BigNumber(0.029188));
                // (499 - 404.0404 * 1.2) / (404.0404 * 1.2) * 100 = 2.91875102918752
                expect(testOrderPair.getNetPriceReturn().percentageBN('AUD')).toEqual(BigNumber(2.92));
            });
            it("summary description with no leading or trailing trades", function () {
                var testOrderPair = new orderPair_1.default('BTCAUD', 'BTCUSD', 1.2);
                expect(testOrderPair.summaryDescription()).toEqual("return 0% as no leading or trailing orders");
            });
            it("summary description with only leading trade", function () {
                var testOrderPair = new orderPair_1.default('BTCAUD', 'BTCUSD', 1.2);
                testOrderPair.addLeadingOrder(leadingOrder);
                expect(testOrderPair.summaryDescription()).toEqual("return Infinity% (499AUD 415.83USD), testExchange1 leading sell filled 50.003% 499AUD 1.0000 @ 500.00, no trailing orders, AUDUSD 1.2");
            });
            it("summary description with leading and trailing trades", function () {
                expect(testOrderPair.summaryDescription()).toEqual("return 3.85% (18.52AUD 15.43USD), testExchange1 leading sell filled 50.003% 499AUD 1.0000 @ 500.00, testExchange2 trailing buy 400.4USD 1.001 @ 400, AUDUSD 1.2");
            });
        });
    });
});
