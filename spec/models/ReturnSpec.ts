/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/jasmine/jasmine.d.ts" />

var BigNumber = require('bignumber.js');
type BigNumber = bignumber.BigNumber;

import Return from '../../models/return';

// Third party libraries
var	VError = require('verror');

describe("Return: ", function()
{
    describe("initiation", function()
    {
        it("no defaults", function()
        {
            var testReturn = new Return({
                sellCurrency: 'USD',
                sellAmount: '120',
                buyCurrency: 'AUD',
                buyAmount: '150',

                amountRounding: 6,
                decimalRounding: 5,
                percentageRounding: 3,

                sell2buyExchangeRate: '1.2'
            });

            expect(testReturn.sellCurrency).toEqual('USD');
            expect(testReturn.sellAmount).toEqual('120');

            expect(testReturn.buyCurrency).toEqual('AUD');
            expect(testReturn.buyAmount).toEqual('150');

            expect(testReturn.amountRounding).toEqual(6);
            expect(testReturn.decimalRounding).toEqual(5);
            expect(testReturn.percentageRounding).toEqual(3);

            expect(testReturn.sell2buyExchangeRate).toEqual('1.2');
        });

        it("with defaults", function()
        {
            var testReturn = new Return({
                sellCurrency: 'EUR',
                sellAmount: '1000',
                buyCurrency: 'EUR',
                buyAmount: '990'
            });

            expect(testReturn.sellCurrency).toEqual('EUR');
            expect(testReturn.sellAmount).toEqual('1000');

            expect(testReturn.buyCurrency).toEqual('EUR');
            expect(testReturn.buyAmount).toEqual('990');

            expect(testReturn.amountRounding).toEqual(8);
            expect(testReturn.decimalRounding).toEqual(6);
            expect(testReturn.percentageRounding).toEqual(2);

            expect(testReturn.sell2buyExchangeRate).toEqual('1');
        });
    });

    describe("same buy and sell currency", function()
    {
        it("sell amount > buy amount", function()
        {
            var testReturn = new Return({
                sellCurrency: 'USD',
                amountRounding: 4,
                sellAmount: '120',
                buyCurrency: 'USD',
                buyAmount: '100'
            });

            // 120 - 100
            expect(testReturn.amountBN('USD')).toEqual(BigNumber(20));
            // (120 - 100) / 100
            expect(testReturn.decimalBN('USD')).toEqual(BigNumber(0.2));
            // (120 - 100) / 100 * 100
            expect(testReturn.percentageBN('USD')).toEqual(BigNumber(20));
        });

        it("sell amount = buy amount", function()
        {
            var testReturn = new Return({
                sellCurrency: 'USD',
                sellAmount: '100',
                buyCurrency: 'USD',
                buyAmount: '100'
            });

            // 100 - 100
            expect(testReturn.amountBN('USD')).toEqual(BigNumber(0));
            // (100 - 100) / 100
            expect(testReturn.decimalBN('USD')).toEqual(BigNumber(0));
            // (100 - 100) / 100 * 100
            expect(testReturn.percentageBN('USD')).toEqual(BigNumber(0));
        });

        it("sell amount < buy amount", function()
        {
            var testReturn = new Return({
                sellCurrency: 'USD',
                sellAmount: '200',
                buyCurrency: 'USD',
                buyAmount: '300',
                decimalRounding: 3,
                percentageRounding: 1
            });

            // 200 - 300 = -100
            expect(testReturn.amountBN() ).toEqual(BigNumber(-100));
            // (200 - 300) / 300 = 0.33333333333
            expect(testReturn.decimalBN() ).toEqual(BigNumber(-0.333));
            // (200 - 300) / 300 * 100
            expect(testReturn.percentageBN() ).toEqual(BigNumber(-33.3));
        });
    });

    describe("different buy and sell currency", function()
    {
        var testReturn;

        beforeEach(function()
        {
            testReturn = new Return({
                sellCurrency: 'USD',
                sellAmount: '250',
                buyCurrency: 'AUD',
                buyAmount: '300',
                sell2buyExchangeRate: '1.2951'
            });
        });

        it("return values", function()
        {
            // 250 - 300 / 1.2951 = 18.3576557794765
            expect(testReturn.amountBN('USD')).toEqual(BigNumber(18.35765578));
            // (250 * 1.2951 - 300) / 300 = 0.0792499999999999
            expect(testReturn.decimalBN('USD')).toEqual(BigNumber(0.07925));
            // (250 * 1.2951 - 300) / 300 * 100 = 7.92499999999999
            expect(testReturn.percentageBN('USD')).toEqual(BigNumber(7.93));

            // 250 * 1.2951 - 300 = 23.775
            expect(testReturn.amountBN('AUD')).toEqual(BigNumber(23.775));
            // (250 * 1.2951 - 300) / 300 = 0.0792499999999999
            expect(testReturn.decimalBN('AUD')).toEqual(BigNumber(0.07925));
            // (250 * 1.2951 - 300) / 300 * 100 = 7.92499999999999 = 7.925 after rounding
            expect(testReturn.percentageBN('AUD')).toEqual(BigNumber(7.93));
        });

        it("no currency parameter", function()
        {
            expect(testReturn.amountBN()).toEqual(jasmine.any(VError));
            expect(testReturn.decimalBN()).toEqual(jasmine.any(VError));
            expect(testReturn.percentageBN()).toEqual(jasmine.any(VError));
        });

        it("invalid currency parameter", function()
        {
            expect(testReturn.amountBN('EUR')).toEqual(jasmine.any(VError));
            expect(testReturn.decimalBN('EUR')).toEqual(jasmine.any(VError));
            expect(testReturn.percentageBN('EUR')).toEqual(jasmine.any(VError));
        });
    });
});

