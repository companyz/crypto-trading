/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
/// <reference path="../typings/async/async.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/moment/moment.d.ts" />
// load third party packages
var _ = require('underscore');
var async = require('async');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
var market_1 = require("./market");
var balance_1 = require("./balance");
var account_1 = require("./account");
// My packages
var logger = require('config-logger'), VError = require('verror');
/**
 * an abstract implementation of an exchange which is made up of different currencies and markets.
 *
 * A currency can be a crypto currency, eg BTC and LTC, or fiat, eg AUD, USD...
 * Markets are made up from a fixed and variable currencies. eg BTCUSD, LTCBTC or AUDUSD
 * For BTCUSD, the fixed currency is BTC and the variable currency is USD
 * Buying BTCUSD is buying BTC and selling USD
 * Selling BTCUSD is selling BTC and buying BTC
 *
 * An exchange also has a account with a number of currency balances that are used in market trading.
 */
var Exchange = (function () {
    function Exchange(settings) {
        this.markets = {};
        this.currencyRounding = {};
        this.defaultCurrencyRounding = 4;
        this.priceRounding = {};
        this.defaultPriceRounding = 2;
        this.minAmount = {};
        this.defaultMinAmount = 0.01;
        this.minOrderBookAmountToIgnore = 0;
        this.maxFailures = 3;
        this.publicPollingInterval = 2000; //polling interval of public methods
        this.privatePollingInterval = 2000; //polling interval of private methods
        this.confirmationTime = 0;
        /*
         buyCurrency the fee is always charged in the buy currency of the order. eg buy 1 BTC at 500 USD will be 1 * (1 - commission) BTC. eg Bitfinex, BTCe
         variableCurrency the fee is always charged in the variable currency of the order. eg buy 1 BTC at 500 USD will be 1000 * (1 - commission) USD. eg Independent Reserve
    
         the following are still to be implemented
         sellCurrency the fee is always charged in the sell currency of the order. eg buy 1 BTC at 500 USD will be 1000 * (1 - commission) USD
         fixedCurrency the fee is always charged in the fixed currency of the order. eg sell 1 BTC at 500 USD will be 1 * (1 - commission) BTC
         */
        this.feeStructure = "buyCurrency"; // valid structures
        this.failures = 0;
        this.requestNumber = 0;
        var functionName = 'Exchange.constructor()';
        // override the default Order options with what's passed to the constructor
        _.extend(this, settings);
        //TODO is this still needed?
        this.exchangeSettings = settings;
        if (!this.exchangeSettings || !this.exchangeSettings.APIkey || !this.exchangeSettings.APIsecret) {
            var error = new VError('%s Config %s.key or %s.secret has not been set in the configuration. Check that environment variable NODE_CONFIG={"%s":{"APIkey":"xxxx"},{"APIsecret:"yyyy"}} has been set', functionName, this.name, this.name, this.name);
            logger.error(error.stack);
            throw error;
        }
        this.initCommissions(settings.commissions);
        this.initAccount(settings.balances);
    }
    Object.defineProperty(Exchange, "NOT_ENOUGH_FUNDS", {
        // set exchange constants
        get: function () { return "notEnoughFunds"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "AMOUNT_TOO_SMALL", {
        get: function () { return "amountTooSmall"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "ALREADY_FILLED", {
        // could not cancel an order as it is already filled
        get: function () { return "alreadyFilled"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "ALREADY_CANCELLED", {
        get: function () { return "alreadyCancelled"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "ID_NOT_FOUND", {
        get: function () { return "exchangeIdNotFound"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "PARTIALLY_FILLED", {
        // used when an order was partially filled just before it was cancelled as part of a replace order
        get: function () { return "partiallyFilled"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exchange, "NOT_FILLED", {
        get: function () { return "orderNotFilled"; },
        enumerable: true,
        configurable: true
    });
    Exchange.prototype.initCommissions = function (commissionConfig) {
        var functionName = "Exchange.initCommissions", self = this;
        var defaultCommission = {
            maker: 0,
            taker: 0
        };
        // initialise the commissions to an empty object. ie no currency commissions are set set
        // this is needed as the constructor does _.extend(this, settings); which can set the commissions to a number
        this.commissions = {};
        // If commissions were configured with type number
        if (typeof commissionConfig == 'number') {
            defaultCommission = {
                maker: commissionConfig,
                taker: commissionConfig
            };
        }
        else if (commissionConfig.maker || commissionConfig.taker) {
            defaultCommission = _.extend(defaultCommission, commissionConfig);
            if (!commissionConfig.maker) {
                defaultCommission.maker = commissionConfig.taker;
            }
            else if (!commissionConfig.taker) {
                defaultCommission.taker = commissionConfig.maker;
            }
        }
        else if (_.isObject(commissionConfig) &&
            typeof _.values(commissionConfig)[0] == 'number') {
            _.keys(commissionConfig).forEach(function (currency) {
                self.commissions[currency] = {
                    maker: commissionConfig[currency],
                    taker: commissionConfig[currency]
                };
            });
        }
        else if (_.isObject(commissionConfig)) {
            this.commissions = commissionConfig;
        }
        // for each unique currency in the fixed and variable currencies
        _.union(this.fixedCurrencies, this.variableCurrencies).
            forEach(function (currency) {
            // if no commission for this currency
            if (!self.commissions[currency]) {
                self.commissions[currency] = defaultCommission;
            }
            // if no rounding for this currency
            if (!self.currencyRounding[currency]) {
                self.currencyRounding[currency] = self.defaultCurrencyRounding;
            }
        });
        // TODO move the below into a separate function
        // initialise each market and commission if not set in the above constructor
        // the fixed and variable currencies are known
        if (this.fixedCurrencies.length > 0 && this.variableCurrencies.length > 0) {
            // loop through each of the exchange's fixed currencies
            self.fixedCurrencies.forEach(function (fixedCurrency) {
                // loop through each of the exchange's variable currencies
                self.variableCurrencies.forEach(function (variableCurrency) {
                    // Fixed and variable currencies can't be the same. eg BTCBTC
                    if (fixedCurrency == variableCurrency)
                        return;
                    var symbol = fixedCurrency + variableCurrency;
                    // if market wasn't instantiated above for this fixed and variable currency
                    if (!self.markets[symbol]) {
                        //add an empty market for this fixed and variable currency
                        self.addMarket(fixedCurrency, variableCurrency);
                    }
                    // if no price rounding for this market
                    if (!self.priceRounding[symbol]) {
                        self.priceRounding[symbol] = self.defaultPriceRounding;
                    }
                    // if no minAmount for this market
                    if (!self.minAmount[symbol]) {
                        self.minAmount[symbol] = self.defaultMinAmount;
                    }
                });
            });
        }
    };
    /**
     * Initialises an accounts balances from a configuration array
     * any fixed or variable currencies not in the config will be initialised to 0.
     *
     * Configuration is usually for simulation or testing rather than live exchanges
     *
     * @param balancesConfig an array of object is with currency, totalBalance and availableBalance.
     * eg [{currency: 'BTC', totalBalance: '20', availableBalance: '18'}, {currency: 'USD', totalBalance: '1000', availableBalance: '900'}]
     */
    Exchange.prototype.initAccount = function (balancesConfig) {
        var functionName = 'Exchange.initAccount()', self = this;
        var balances = [];
        // if a configuration array was passed in
        if (_.isArray(balancesConfig)) {
            logger.info('%s %s currencies are in the configuration', functionName, balancesConfig.length);
            // loop through each balance in the config
            balancesConfig.forEach(function (balance) {
                balances.push(new balance_1.default({
                    exchangeName: self.name,
                    currency: balance.currency,
                    totalBalance: balance.totalBalance,
                    availableBalance: balance.availableBalance
                }));
                logger.info('%s initialising currency %s with total %s and available %s', functionName, balance.currency, balance.totalBalance, balance.availableBalance);
            });
        }
        // now initialise any currency not in config to 0
        var allCurrencies = _.union(self.fixedCurrencies, self.variableCurrencies);
        allCurrencies.forEach(function (currency) {
            // find balance with currency
            var balance = _.find(balances, function (balance) { return balance.currency === currency; });
            // if balance was not set from config
            if (_.isUndefined(balance)) {
                // initialise zero balances for currency
                balances.push(new balance_1.default({
                    exchangeName: self.name,
                    currency: currency,
                    totalBalance: '0',
                    availableBalance: '0'
                }));
            }
        });
        // instantiate account from config or default to 0
        self.account = new account_1.default(balances, this.name, this.currencyRounding);
    };
    /**
     * Filters the results from getPendingOrders.
     * Used when an exchange only supports getting pending orders across all markets.
     */
    Exchange.prototype.getPendingOrdersForSymbol = function (symbol, callback) {
        var functionName = 'AnxPro.getPendingOrdersForSymbol()';
        logger.trace('%s about to get pending orders on the %s exchange for market %s', functionName, this.name, symbol);
        this.getPendingOrders(function (err, pendingOrders) {
            if (err) {
                return callback(err);
            }
            var pendingOrdersForSymbol = _.where(pendingOrders, { symbol: symbol });
            callback(null, pendingOrdersForSymbol);
        });
    };
    /**
     * Loops through all markets and gets the pending orders.
     * Used when an exchange only supports getting pending orders for a particular market.
     * @param callback
     */
    Exchange.prototype.getPendingOrders = function (callback) {
        var functionName = 'Exchange.getPendingOrders()', self = this;
        logger.trace('%s about to get pending orders across all markets for the %s exchange', functionName, this.name);
        var orders = [];
        // for each market symbol
        async.eachSeries(_.keys(self.markets), function (symbol, callback) {
            self.getPendingOrdersForSymbol(symbol, function (err, exchangeOrders) {
                orders = orders.concat(exchangeOrders);
                callback(err);
            });
        }, function (err) {
            callback(err, orders);
        });
    };
    /**
     * Add empty market object to markets object
     * @Param {String} fixedCurrency . eg BTC for a BTC/AUD exchange
     * @Param {String} variableCurrency. eg AUD for a BTC/AUD exhange
     */
    Exchange.prototype.addMarket = function (fixedCurrency, variableCurrency) {
        var symbol = fixedCurrency + variableCurrency;
        this.markets[symbol] = new market_1.default({
            exchangeName: this.name,
            symbol: symbol
        });
    };
    // TODO remove symbol parameter as it can be sourced from the localOrders
    // TODO need to ensure only the unique markets in localOrders is queries though. This avoid unnecessary calls to the exchange for markets not in localOrders
    /**
     * Returns the orders that have been filled since the last call. These can be fully or partially filled orders
     */
    Exchange.prototype.getFilledOrders = function (localOrders, symbol, callback) {
        var functionName = 'Exchange.getFilledOrders()', self = this;
        var fullyFilledOrders = [], returnedExchangeOrders = [];
        // increment the request number for this getFilledOrders request
        self.requestNumber++;
        // find pending local orders where state is pending
        var pendingLocalOrders = _.where(localOrders, { state: 'pending' });
        logger.debug('%s getting pending exchange orders from %s to see if any of the %s pending local orders are missing.', functionName, self.name, pendingLocalOrders.length);
        //get pending orders from the exchange
        self.getPendingOrdersForSymbol(symbol, function (err, pendingExchangeOrders) {
            if (err) {
                var error = new VError(err, '%s could not get pending orders from the %s exchange to see if any orders have been filled', functionName, self.name);
                logger.error(error.stack);
                return callback(error);
            }
            logger.debug('%s got %s pending exchange orders to check against %s pending local orders', functionName, pendingExchangeOrders.length, pendingLocalOrders.length);
            // find orders that have been fully filled on the exchange by
            // finding pending local orders not in pending exchange orders
            fullyFilledOrders = _.reject(pendingLocalOrders, function (pendingLocalOrder) {
                return _.find(pendingExchangeOrders, function (pendingExchangeOrder) { return pendingExchangeOrder.exchangeId === pendingLocalOrder.exchangeId; });
            });
            logger.debug('%s found %s fully filled orders on the %s exchange', functionName, fullyFilledOrders.length, self.name);
            //for each fully filled order clone, fill and add to array of filled orders to be returned
            fullyFilledOrders.forEach(function (filledOrder) {
                // remove filled order from the list of pending orders
                // this array is used in the next section to detect any partially filled orders
                pendingLocalOrders = _.reject(pendingLocalOrders, function (pendingExchangeOrder) { return pendingExchangeOrder.exchangeId === filledOrder.exchangeId; });
                var exchangeOrder = filledOrder.clone();
                exchangeOrder.fill();
                logger.info('%s filled order with id %s, tag %s, amount %s, amount remaining %s, amount last partial %s and price %s', functionName, exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.amount, exchangeOrder.amountRemaining, exchangeOrder.amountLastPartial, exchangeOrder.price);
                // add cloned order to list of orders to be returned
                returnedExchangeOrders.push(exchangeOrder);
                // update exchange balances
                self.account.filledOrder(exchangeOrder);
                // update market orders, emit a new event for listeners
                self.markets[exchangeOrder.symbol].fillOrder(exchangeOrder);
                // if there is a latest order book for the filled order's market
                if (self.markets[exchangeOrder.symbol].latestOrderBook &&
                    _.isFunction(self.markets[exchangeOrder.symbol].latestOrderBook.removeOrder)) {
                    // remove my order I've just cancelled from the latest market order book
                    self.markets[exchangeOrder.symbol].latestOrderBook.removeOrder(exchangeOrder);
                }
            });
            // check for partial fills and return in the filledOrders array
            // for each of the pending orders, check that their amounts ordered are more than the pending order amounts on the exchange
            pendingLocalOrders.forEach(function (pendingLocalOrder) {
                // for each pending order on the exchange
                pendingExchangeOrders.forEach(function (pendingExchangeOrder) {
                    // if pending exchange order has a remaining amount < the pending local order remaining amount
                    if (pendingLocalOrder.exchangeId === pendingExchangeOrder.exchangeId &&
                        BigNumber(pendingExchangeOrder.amountRemaining).lessThan(pendingLocalOrder.amountRemaining)) {
                        // calculate the amount that was partially filled since the last getFilledOrders call
                        var partialFillAmount = pendingLocalOrder.amountRemainingBN.
                            minus(pendingExchangeOrder.amountRemaining).
                            round(self.currencyRounding[pendingExchangeOrder.fixedCurrency]).
                            toString();
                        logger.info('%s found partially filled order with partial fill amount %s, id %s, tag %s, amount %s and amount remaining %s', functionName, partialFillAmount, pendingExchangeOrder.exchangeId, pendingExchangeOrder.tag, pendingExchangeOrder.amount, pendingExchangeOrder.amountRemaining);
                        var exchangeOrder = pendingLocalOrder.clone();
                        // mark the pending order as being partially filled
                        exchangeOrder.partialFill(partialFillAmount);
                        // add partially filled order to the list of orders to be returned
                        returnedExchangeOrders.push(exchangeOrder);
                        // update exchange balances
                        self.account.filledOrder(exchangeOrder);
                        // update market orders, emit a new event for listeners
                        self.markets[exchangeOrder.symbol].partialFillOrder(exchangeOrder);
                        // if there is a latest order book for the filled order's market
                        if (self.markets[exchangeOrder.symbol].latestOrderBook &&
                            _.isFunction(self.markets[exchangeOrder.symbol].latestOrderBook.removeOrder)) {
                            // remove my order I've just cancelled from the latest market order book
                            self.markets[exchangeOrder.symbol].latestOrderBook.removeOrder(exchangeOrder);
                        }
                    }
                });
            });
            callback(null, returnedExchangeOrders);
        });
    };
    ;
    /**
     * polls for a ticker for each specified symbol
     * @param symbols array of instrument symbols. eg BTCUSD, LTCAUD...
     * @param callback
     */
    Exchange.prototype.getTickers = function (symbols, callback) {
        var functionName = 'Exchange.getTickers()', self = this;
        // if no symbols passed then get tickers for all markets on the exchange
        if (!symbols) {
            symbols = _.keys(this.markets);
        }
        var pollingInterval = symbols.length * self.publicPollingInterval;
        logger.debug('%s pollingInterval = %s * %s = %s', functionName, symbols.length, self.publicPollingInterval, pollingInterval);
        var intervalIds = [];
        // for each symbol start a poller to get the ticker
        symbols.forEach(function (symbol, i) {
            var startDelay = i * self.publicPollingInterval;
            logger.debug('%s %s[%s] ticker poller starting in %dms polling every %dms', functionName, self.name, symbol, startDelay, pollingInterval);
            setTimeout(function () {
                // run initially
                self.getTicker.bind(self, symbol);
                //then run at an interval
                intervalIds.push(setInterval(self.getTicker.bind(self, symbol), pollingInterval));
            }, startDelay);
        });
        // call the callback after all the pollers have started
        setTimeout(function () {
            if (callback)
                callback(intervalIds);
        }, pollingInterval + 1000); // start 1 second after the pollers should have finished
    };
    // starts poller for each symbol to retrieve the exchange order book
    Exchange.prototype.getOrderBooks = function (symbols, callback) {
        var functionName = 'Exchange.getOrderBooks()', self = this;
        // if no symbols passed then get order books for all markets on the exchange
        if (!symbols) {
            symbols = _.keys(this.markets);
        }
        var pollingInterval = symbols.length * self.publicPollingInterval;
        var intervalIds = [];
        // for each market start a poller to get the order books
        symbols.forEach(function (symbol, i) {
            var startDelay = i * self.publicPollingInterval;
            logger.debug('%s %s[%s] order book poller starting in %dms polling every %dms', functionName, self.name, symbol, startDelay, pollingInterval);
            setTimeout(function () {
                // do an initial run
                self.getOrderBook.bind(self, symbol);
                // then run at a regular interval
                intervalIds.push(setInterval(self.getOrderBook.bind(self, symbol), pollingInterval));
            }, startDelay);
        });
        // call the callback after all the pollers have started
        setTimeout(function () {
            if (callback)
                callback(intervalIds);
        }, pollingInterval + 1000); // start 1 second after the pollers should have finished
    };
    Exchange.prototype.addOrderSuccess = function (newOrder, callback, data) {
        var functionName = 'Exchange.addOrderSuccess()', self = this;
        this.failures = 0;
        logger.debug('%s successfully added %s %s order with tag %s, price %s and remaining amount %s to the %s exchange', functionName, newOrder.type, newOrder.side, newOrder.tag, newOrder.price, newOrder.amountRemaining, newOrder.exchangeName);
        var exchangeOrder = newOrder.clone();
        exchangeOrder.exchangeId = data.exchangeId;
        exchangeOrder.timestamp = data.timestamp;
        // if original exchange id is not set then set it
        if (!newOrder.originalExchangeId) {
            exchangeOrder.originalExchangeId = exchangeOrder.exchangeId;
        }
        if (newOrder.type === 'limit') {
            exchangeOrder.state = 'pending';
            // update exchange balances
            self.account.addOrder(exchangeOrder);
            // update market orders and emit a new event for listeners of new order events
            self.markets[newOrder.symbol].addOrder(exchangeOrder);
            // if there is a latest order book for the market the new order was just added to
            if (self.markets[newOrder.symbol].latestOrderBook &&
                _.isFunction(self.markets[newOrder.symbol].latestOrderBook.addOrder)) {
                // add my new order to the latest market order book
                self.markets[newOrder.symbol].latestOrderBook.addOrder(exchangeOrder);
            }
            return callback(null, exchangeOrder);
        }
        else if (newOrder.type === 'market') {
            exchangeOrder.fill();
            self.addOrderGetVwap(exchangeOrder, callback);
        }
    };
    Exchange.prototype.addOrderGetVwap = function (exchangeOrder, callback) {
        var functionName = 'Exchange.addOrderGetVwap()', self = this;
        self.getOrder(exchangeOrder.exchangeId, function (err, gotOrder) {
            if (err) {
                return self.errorHandler(err, true, 'addOrderGetVwap', self.addOrderGetVwap, callback, exchangeOrder);
            }
            else if (!gotOrder.price || new BigNumber(gotOrder.price).equals(0)) {
                var error = new VError('%s could not get vwap (%s) for market %s order with id %s, tag %s and remaining amount %s on the %s exchange.', functionName, gotOrder.price, exchangeOrder.side, exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.amountRemaining, self.name);
                error.name = Exchange.NOT_FILLED;
                logger.error(error.stack);
                return self.errorHandler(error, true, 'addOrderGetVwap', self.addOrderGetVwap, callback, exchangeOrder);
            }
            // set the Volume Weighted Average Price (VWAP) of the Order
            exchangeOrder.price = gotOrder.price;
            logger.info('%s successfully got vwap of %s for market %s order with exchange id %s, tag %s and remaining amount %s', functionName, gotOrder.price, gotOrder.side, gotOrder.exchangeId, gotOrder.tag, gotOrder.amountRemaining);
            // update exchange balances
            self.account.addMarketTrade(exchangeOrder);
            // update market orders and emit a new event for listeners of new order events
            self.markets[exchangeOrder.symbol].fillOrder(exchangeOrder);
            // if there is a latest order book for the market the new order was just added to
            if (self.markets[exchangeOrder.symbol].latestOrderBook &&
                _.isFunction(self.markets[exchangeOrder.symbol].latestOrderBook.addOrder)) {
                // add my new order to the latest market order book
                self.markets[exchangeOrder.symbol].latestOrderBook.addOrder(exchangeOrder);
            }
            return callback(null, exchangeOrder);
        });
    };
    Exchange.prototype.addOrderTimeout = function (err, newOrder, addOrderCallback) {
        var functionName = "Exchange.addOrderTimeout()", self = this;
        this.failures++;
        var orderDesc = util_1.format('%s order with symbol %s, price %s, remaining amount %s and tag %s', newOrder.side, newOrder.symbol, newOrder.price, newOrder.amountRemaining, newOrder.tag);
        var error = new VError(err, '%s failed to add %s as the request timed out. Failure number %d', functionName, orderDesc, this.failures);
        logger.warn(error.stack);
        // check that the order was added to the exchange or not
        this.getPendingOrders(function (getPendingOrdersError, pendingOrders) {
            if (getPendingOrdersError) {
                var error_1 = new VError(getPendingOrdersError, '%s could not get pending orders to see if timed out add %s worked or not.', functionName, orderDesc);
                logger.error(error_1.stack);
                return self.errorHandler(error_1, true, 'addOrderTimeout', self.addOrderTimeout, addOrderCallback, newOrder);
            }
            // if order found on the exchange with matching symbol, price and remaining amount
            var addedOrder = _.findWhere(pendingOrders, {
                "symbol": newOrder.symbol,
                "price": newOrder.price,
                "amountRemaining": newOrder.amountRemaining
            });
            // if a matching order on the exchange was found
            if (addedOrder) {
                logger.warn('%s matching order was found on the %s exchange so previous add %s must have worked', functionName, self.name, orderDesc);
                var data = {
                    exchangeId: addedOrder.exchangeId,
                    timestamp: addedOrder.timestamp
                };
                self.addOrderSuccess(newOrder, addOrderCallback, data);
            }
            else {
                var error_2 = new VError(err, "%s no matching order was found on the %s exchange so previous add %s must not have worked.", functionName, self.name, orderDesc);
                logger.error(error_2.stack);
                self.errorHandler(error_2, true, 'addOrder', self.addOrder, addOrderCallback, newOrder);
            }
        });
    };
    /**
     * cancels an order without calling out to the exchange. Used when you know the order has already been cancelled or filled.
     */
    Exchange.prototype.cancelOrderSuccess = function (cancelOrder, callback) {
        var functionName = 'Exchange.cancelOrderSuccess()', self = this;
        this.failures = 0;
        logger.info('%s successfully cancelled order with id %s', functionName, cancelOrder.exchangeId);
        var exchangeOrder = cancelOrder.clone();
        exchangeOrder.cancel();
        self.account.cancelOrder(exchangeOrder);
        // update market orders, emit a new event for listeners order events
        self.markets[exchangeOrder.symbol].cancelOrder(exchangeOrder);
        // if there is a latest order book for the market the order was just removed from
        if (self.markets[cancelOrder.symbol].latestOrderBook &&
            _.isFunction(self.markets[cancelOrder.symbol].latestOrderBook.removeOrder)) {
            // remove my order I've just cancelled from the latest market order book
            self.markets[cancelOrder.symbol].latestOrderBook.removeOrder(exchangeOrder);
        }
        // pass back updated cancelled order
        callback(null, exchangeOrder);
    };
    /**
     * check the state of the order that could not be cancelled and then call the callback with the appropriate error. eg ALREADY_FILLED or ALREADY_CANCELLED
     */
    Exchange.prototype.cancelOrderInvalidState = function (err, cancelOrder, cancelOrderCallback) {
        var functionName = 'Exchange.cancelOrderInvalidState()', self = this;
        this.failures = 0;
        var orderDesc = util_1.format('%s order with id %s and tag %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag);
        var error = new VError(err, '%s failed to cancel %s. Will try and get order state from the %s exchange to see why.', functionName, orderDesc, this.failures, this.name);
        logger.error(error.stack);
        // check that the order was cancelled on the exchange or not
        this.getOrder(cancelOrder.exchangeId, function (getOrdersError, order) {
            if (getOrdersError) {
                var error_3 = new VError(getOrdersError, '%s could not get order to see if timed out cancel %s worked or not.', functionName, orderDesc);
                logger.error(error_3.stack);
                return self.errorHandler(error_3, true, 'cancelOrderTimeout', self.cancelOrderTimeout, cancelOrderCallback, cancelOrder);
            }
            // if a cancelled order on the exchange was found
            if (order.state == 'cancelled') {
                var error_4 = new VError(err, '%s order with id %s has already been cancelled.', functionName, cancelOrder.exchangeId);
                error_4.name = Exchange.ALREADY_CANCELLED;
                logger.error(error_4.stack);
                cancelOrderCallback(error_4);
            }
            else if (order.state === 'filled') {
                var error_5 = new VError(err, '%s order has already been filled.', functionName);
                error_5.name = Exchange.ALREADY_FILLED;
                logger.error(error_5.stack);
                cancelOrderCallback(error_5);
            }
            else if (order.state == 'pending' || order.state == 'partiallyFilled') {
                var error_6 = new VError(err, '%s %s has state %s on the %s exchange so previous cancelOrder call must not have worked.', functionName, orderDesc, order.state, self.name);
                logger.error(error_6.stack);
                self.errorHandler(error_6, true, 'cancelOrder', self.cancelOrder, cancelOrderCallback, cancelOrder);
            }
        });
    };
    Exchange.prototype.cancelOrderTimeout = function (err, cancelOrder, cancelOrderCallback) {
        var functionName = "Exchange.cancelOrderTimeout()", self = this;
        this.failures++;
        var orderDesc = util_1.format('%s order with id %s and tag %s', cancelOrder.side, cancelOrder.exchangeId, cancelOrder.tag);
        var error = new VError(err, '%s failed to cancel %s. Failures %s. Will try and get order state from the %s exchange to see if it was successfully cancelled or not.', functionName, orderDesc, this.failures, this.name);
        logger.error(error.stack);
        // check that the order was cancelled on the exchange or not
        this.getOrder(cancelOrder.exchangeId, function (getOrdersError, order) {
            if (getOrdersError) {
                var error_7 = new VError(getOrdersError, '%s could not get order to see if timed out cancel %s worked or not.', functionName, orderDesc);
                logger.error(error_7.stack);
                return self.errorHandler(error_7, true, 'cancelOrderTimeout', self.cancelOrderTimeout, cancelOrderCallback, cancelOrder);
            }
            // if a cancelled order on the exchange was found
            if (order.state == 'cancelled') {
                logger.warn('%s %s is cancelled on the %s exchange so previous cancelOrder call must have worked.', functionName, orderDesc, self.name);
                self.cancelOrderSuccess(cancelOrder, cancelOrderCallback);
            }
            else if (order.state == 'pending' || order.state == 'partiallyFilled') {
                var error_8 = new VError('%s %s is %s on the %s exchange so previous cancelOrder call must NOT have worked.', functionName, orderDesc, order.state, self.name);
                logger.error(error_8.stack);
                self.errorHandler(error_8, true, 'cancelOrder', self.cancelOrder, cancelOrderCallback, cancelOrder);
            }
            else if (order.state === 'filled') {
                var error_9 = new VError('%s %s has already been filled on the %s exchange.', functionName, orderDesc, self.name);
                error_9.name = Exchange.ALREADY_FILLED;
                logger.error(error_9.stack);
                cancelOrderCallback(error_9);
            }
        });
    };
    // Abstract function the needs to be implemented for each exchange
    // cancels all pending orders
    Exchange.prototype.cancelAllOrders = function (callback) {
        var functionName = 'Exchange.cancelAllOrders()', self = this;
        logger.trace('%s about to get all pending orders across all markets on the %s exchange so they can be cancelled', functionName, self.name);
        // TODO need to pass in the symbol parameter to getPendingOrders
        self.getPendingOrders(function (err, pendingOrders) {
            if (err) {
                var error = new VError('%s could not get pending orders on the %s exchange', functionName, self.name);
                logger.error(error.stack);
                return callback(error);
            }
            logger.debug('%s %s pending orders were on the %s exchange before they were cancelled', functionName, pendingOrders.length, self.name);
            self.cancelOrders(pendingOrders, function (err) {
                if (err) {
                    var error = new VError(err, '%s not all the %s pending orders could not be cancelled', functionName, pendingOrders.length);
                    logger.error(error.stack);
                    return callback(error);
                }
                logger.info('%s successfully cancelled %s orders on the %s exchange', functionName, pendingOrders.length, self.name);
                callback(null, pendingOrders.length);
            });
        });
    };
    // cancels an array of orders to the exchange
    // this is a generic implementation using the abstract cancelOrder.
    // This can be overridden if the exchange supports it. eg Bitfinex
    Exchange.prototype.cancelOrders = function (orders, callback) {
        var functionName = 'Exchange.cancelOrders()', self = this;
        var count = 0;
        if (!orders || !_.isArray(orders)) {
            var error = VError('%s the first parameter %s needs to be an array of orders', functionName, orders);
            logger.error(error.stack);
            return callback(error);
        }
        else if (orders.length === 0) {
            var error = VError('%s the first parameter with an array of orders to cancel has %s orders', functionName, orders.length);
            error.name = 'NoTradesToCancel';
            // no need to fail the call. Just a warning
            logger.warn(error.stack);
            return callback(null);
        }
        logger.trace('%s cancelling %s orders on the exchange in series', functionName, orders.length);
        async.eachSeries(orders, function (order, callback) {
            logger.trace('%s about to cancel %s order on the exchange with id %s, tag %s, price %s and amount %s', functionName, ++count, order.exchangeId, order.tag, order.price, order.amount);
            self.cancelOrder(order, function (err, exchangeOrder) {
                if (err) {
                    var error = new VError(err, '%s failed to cancel %s order with id %s and tag %s', functionName, order.side, order.exchangeId, order.tag);
                    logger.error(error.stack);
                    // will not fail async.eachSeries as other orders may be able to be cancelled
                    return callback(null);
                }
                logger.info('%s order with id %s, tag %s, price %s and remaining amount %s cancelled on the %s exchange', functionName, exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.price, exchangeOrder.amountRemaining, self.name);
                callback(null);
            });
        }, function (err) {
            if (err) {
                var error = VError(err, '%s failed to cancel all %s orders', functionName, orders.length);
                logger.error(error.stack);
                return callback(error);
            }
            callback(null);
        });
    };
    // adds an array of orders to the exchange
    // this is a generic implementation using the abstract addOrder.
    // This can be overridden if the exchange supports it. eg Bitfinex
    Exchange.prototype.addOrders = function (orders, callback) {
        var functionName = 'Exchange.addOrders()', self = this, exchangeOrders = [];
        var count = 0;
        logger.trace('%s adding %s orders onto the exchange in series', functionName, orders.length);
        async.eachSeries(orders, function (order, callback) {
            logger.trace('%s adding %s order to the exchange with tag %s, price %s and amount %s', functionName, ++count, order.tag, order.price, order.amount);
            self.addOrder(order, function (err, exchangeOrder) {
                if (err) {
                    var error = new VError(err, '%s failed to add order with tag %s to the %s exchange.', functionName, order.tag, order.exchangeName);
                    logger.error(error.stack);
                    return callback(error);
                }
                logger.debug('%s order with id %s, tag %s, price %s and amount %s added to the exchange', functionName, exchangeOrder.exchangeId, exchangeOrder.tag, exchangeOrder.price, exchangeOrder.amount);
                exchangeOrders.push(exchangeOrder);
                callback(null);
            });
        }, function (err) {
            if (err) {
                var error = new VError(err, '%s unable to add all %s orders. %s were successfully added', functionName, orders.length, exchangeOrders.length);
                logger.error(error.stack);
                return callback(error, exchangeOrders);
            }
            callback(null, exchangeOrders);
        });
    };
    ;
    // replace an old order with a new order
    // this is a generic implementation using the abstract addOrder and cancelOrder.
    // This can be overridden if the exchange supports it. eg Bitfinex
    Exchange.prototype.replaceOrder = function (oldOrder, newOrder, callback) {
        var functionName = 'Exchange.replaceOrder()', self = this;
        var oldOrderDescription = util_1.format('%s order with id %s, tag %s, price %s and amount %s', oldOrder.side, oldOrder.exchangeId, oldOrder.tag, oldOrder.price, oldOrder.amount);
        var newOrderDescription = util_1.format('%s order with id %s, tag %s, price %s and amount %s', newOrder.side, newOrder.exchangeId, newOrder.tag, newOrder.price, newOrder.amount);
        logger.trace('%s replacing %s with %s', functionName, oldOrderDescription, newOrderDescription);
        async.series([
            function cancelOldOrder(callback) {
                logger.trace('%s about to cancel %s on the %s exchange', functionName, oldOrderDescription, oldOrder.exchangeName);
                self.cancelOrder(oldOrder, function (err, cancelledOrder) {
                    if (err) {
                        var error = new VError(err, '%s could not replace %s on the %s exchange', functionName, oldOrderDescription, oldOrder.exchangeName);
                        error.name = err.name;
                        logger.error(error.stack);
                        return callback(error, null);
                    }
                    logger.debug('%s cancelled %s on the %s exchange', functionName, oldOrderDescription, oldOrder.exchangeName);
                    callback(null, cancelledOrder);
                });
            },
            // check that cancelled order was not partially or fully filled
            // check that cancelled order was not partially or fully filled
            function checkNotPartiallyFilled(callback) {
                logger.trace('%s about to check if the cancelled %s was partially filled on the %s exchange', functionName, oldOrderDescription, oldOrder.exchangeName);
                self.getOrder(oldOrder.exchangeId, function (err, exchangeOrder) {
                    if (err) {
                        var error = new VError(err, '%s could not get order with id %s from exchange %s', functionName, oldOrder.exchangeId, self.name);
                        error.name = err.name;
                        logger.error(error.stack);
                        return callback(error, null);
                    }
                    if (exchangeOrder.amountRemaining !== oldOrder.amountRemaining) {
                        var error = new VError('%s order with id %s has been partially filled on exchange %s as old remaining amount %s does not equal to new remaining amount %s', functionName, oldOrder.exchangeId, self.name, oldOrder.amountRemaining, exchangeOrder.amountRemaining);
                        error.name = Exchange.PARTIALLY_FILLED;
                        logger.error(error.stack);
                        return callback(error, null);
                    }
                    logger.debug('%s order with id %s has not been partially or fully filled', functionName, oldOrder.exchangeId);
                    callback(null, exchangeOrder);
                });
            },
            function addNewOrder(callback) {
                logger.trace('%s about to add %s order with tag %s, price %s and amount %s to the exchange', functionName, newOrder.side, newOrder.tag, newOrder.price, newOrder.amount);
                self.addOrder(newOrder, function (err, exchangeOrder) {
                    if (err) {
                        var error = new VError(err, '%s could not add order with id %s', functionName, newOrder.exchangeId);
                        error.name = err.name;
                        logger.error(error.stack);
                        callback(error, null);
                    }
                    else {
                        logger.trace('%s %s order with id %s added to the %s exchange', functionName, exchangeOrder.side, exchangeOrder.exchangeId, exchangeOrder.exchangeName);
                        callback(null, exchangeOrder);
                    }
                });
            }], function (err, results) {
            if (err) {
                var error = new VError(err, '%s async.series failed', functionName);
                error.name = err.name;
                logger.error(error.stack);
                return callback(error);
            }
            //returning the new order added to the exchange in the third function of the series
            callback(null, results[2]);
        });
    };
    ;
    Exchange.prototype.errorHandler = function (err, callbackLast, actionDesc, actionFunction, callback) {
        if (callbackLast === void 0) { callbackLast = true; }
        var params = [];
        for (var _i = 5; _i < arguments.length; _i++) {
            params[_i - 5] = arguments[_i];
        }
        var functionName = util_1.format('Exchange.errorHandler.%s()', actionDesc), self = this;
        self.failures++;
        var error = new VError(err, '%s could not %s on exchange %s. Failure number %d', functionName, actionDesc, self.name, self.failures);
        if (err.name) {
            error.name = err.name;
        }
        logger.error(error.stack);
        // log each of the parameters used in the failed call
        logger.error('%s failed %s parameters:', functionName, actionDesc);
        params.forEach(function (param, index) {
            if (!param) {
                logger.error('param %s: undefined', index);
            }
            else if (param.log) {
                logger.error('param %s:', index);
                param.log('error');
            }
            else if (param.toString) {
                logger.error('param %s: %s', index, param.toString());
            }
            else {
                logger.error('param %s: can not be logged', index);
            }
        });
        if (self.failures >= self.maxFailures) {
            var err_1 = new VError(error, '%s exchange %s failed %d times in a row so returning error in original callback', functionName, self.name, self.maxFailures);
            if (err_1.name) {
                error.name = err_1.name;
            }
            logger.error(err_1.stack);
            return callback(err_1);
        }
        // try again until maxFailures is hit
        setTimeout(function () {
            if (callbackLast) {
                // no parameters passed
                if (params.length == 0) {
                    actionFunction.call(self, callback);
                }
                else if (params.length == 1) {
                    actionFunction.call(self, params[0], callback);
                }
                else {
                    actionFunction.call(self, params[0], params[1], callback);
                }
            }
            else {
                // no parameters passed
                if (params.length == 0) {
                    actionFunction.call(self, callback);
                }
                else if (params.length == 1) {
                    actionFunction.call(self, callback, params[0]);
                }
                else {
                    actionFunction.call(self, callback, params[0], params[1]);
                }
            }
        }, self.failures * self.publicPollingInterval);
    };
    return Exchange;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Exchange;
