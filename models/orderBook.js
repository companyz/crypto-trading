/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
// load third party packages
var moment = require('moment');
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
var exchange_1 = require("./exchange");
// Third party packages
var logger = require('config-logger'), VError = require('verror');
var OrderBook = (function () {
    function OrderBook(settings) {
        var functionName = 'OrderBook.constructor()';
        this.exchangeName = settings.exchangeName;
        this.symbol = settings.symbol;
        this.bids = OrderBook.convertOrders(settings.bids);
        this.asks = OrderBook.convertOrders(settings.asks);
        this.timestamp = settings.timestamp || new Date();
        this.minAmountToIgnore = settings.minAmountToIgnore || 0;
    }
    Object.defineProperty(OrderBook.prototype, "exchange", {
        get: function () { return exchange_1.default.exchanges[this.exchangeName]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OrderBook.prototype, "market", {
        get: function () { return this.exchange.markets[this.symbol]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OrderBook.prototype, "priceRounding", {
        get: function () { return this.exchange.priceRounding[this.symbol]; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(OrderBook.prototype, "smallestPrice", {
        get: function () { return new BigNumber(1).div(BigNumber(10).toPower(this.priceRounding)).toNumber(); },
        enumerable: true,
        configurable: true
    });
    OrderBook.convertOrders = function (orders) {
        var convertedOrders = [];
        orders.forEach(function (order) {
            convertedOrders.push({
                price: order[0],
                amount: order[1]
            });
        });
        return convertedOrders;
    };
    OrderBook.prototype.midpoint = function () {
        var functionName = 'OrderBook.midpoint()';
        var midpoint = new BigNumber(this.bids[0].price).plus(this.asks[0].price).div(2).toFixed(this.priceRounding);
        logger.debug('%s midpoint of inside bid and ask of order book = %s', functionName, midpoint);
        return midpoint;
    };
    /**
     * adds a new order to an existing order book
     * @param newOrder
     * @returns {VError}
     */
    OrderBook.prototype.addOrder = function (newOrder) {
        var functionName = 'OrderBook.addOrder()', self = this;
        var oldOrders = [], newOrders = [];
        var isPassedOldOrderFN, addedNewOrder = false;
        if (newOrder.side === 'sell') {
            oldOrders = self.asks;
            isPassedOldOrderFN = function (existingOrder) {
                // if new order price > existing order price
                return newOrder.priceBN.greaterThan(existingOrder);
            };
        }
        else if (newOrder.side === 'buy') {
            oldOrders = self.bids;
            isPassedOldOrderFN = function (existingOrderPrice) {
                // if new order price < existing order price
                return newOrder.priceBN.lessThan(existingOrderPrice);
            };
        }
        else {
            var error = new VError("%s new order side %s is not sell or buy", functionName, newOrder.side);
            logger.error(error.stack);
            return error;
        }
        // loop through the old bid or ask orders
        for (var i = 0; i < oldOrders.length; i++) {
            var currentOrder = oldOrders[i], nextOrder = oldOrders[i + 1];
            // if new order price matches existing order price
            if (newOrder.priceBN.equals(currentOrder.price)) {
                var newAmount = new BigNumber(currentOrder.amount).
                    plus(newOrder.amountRemaining).
                    round(self.priceRounding).
                    toString();
                logger.trace('%s new amount %s = existing order amount %s + new order remaining amount %s', functionName, newAmount, currentOrder.amount, newOrder.amountRemaining);
                newOrders.push({
                    price: Number(newOrder.price),
                    amount: Number(newAmount)
                });
                addedNewOrder = true;
            }
            else if (!addedNewOrder && isPassedOldOrderFN(currentOrder.price) &&
                (!nextOrder || !isPassedOldOrderFN(nextOrder.price))) {
                // add existing order
                newOrders.push({
                    price: currentOrder.price,
                    amount: currentOrder.amount
                });
                // add new order to the new orders list
                newOrders.push({
                    price: Number(newOrder.price),
                    amount: Number(newOrder.amountRemaining)
                });
                addedNewOrder = true;
            }
            else {
                // add existing order
                newOrders.push({
                    price: currentOrder.price,
                    amount: currentOrder.amount
                });
            }
        }
        // if new order is past all the existing orders
        if (!addedNewOrder) {
            // add new order to the new orders list
            newOrders.unshift({
                price: Number(newOrder.price),
                amount: Number(newOrder.amountRemaining)
            });
        }
        // set the asks or bids to the new order list
        if (newOrder.side === 'sell') {
            self.asks = newOrders;
        }
        else if (newOrder.side === 'buy') {
            self.bids = newOrders;
        }
    };
    OrderBook.prototype.removeOrder = function (oldOrder) {
        var functionName = 'OrderBook.removeOrder()', self = this;
        var oldOrders = [], newOrders = [];
        if (oldOrder.side === 'sell') {
            oldOrders = self.asks;
        }
        else if (oldOrder.side === 'buy') {
            oldOrders = self.bids;
        }
        else {
            var error = new VError("%s old order side %s is not sell or buy", functionName, oldOrder.side);
            logger.error(error.stack);
            return error;
        }
        // for each order
        oldOrders.forEach(function (currentOrder) {
            // if order prices match
            if (oldOrder.priceBN.equals(currentOrder.price)) {
                // if order amount match
                if (oldOrder.amountRemainingBN.equals(currentOrder.amount)) {
                    logger.debug('%s current order with price %s and amount %s matches order being removed with price %s and remaining amount %s', functionName, currentOrder.price, currentOrder.amount, oldOrder.price, oldOrder.amountRemaining);
                }
                else {
                    // current order amount - old order amount cast to a Number
                    var newOrderAmount = new BigNumber(currentOrder.amount).
                        minus(oldOrder.amountRemaining).
                        toString();
                    newOrders.push({
                        price: currentOrder.price,
                        amount: Number(newOrderAmount)
                    });
                    logger.debug('%s current order with price %s matches order being removed. New order amount %s = current order amount %s - amount of old order being removed %s', functionName, currentOrder.price, newOrderAmount, currentOrder.amount, oldOrder.amountRemaining);
                }
            }
            else {
                // add current order to new orders list
                newOrders.push(currentOrder);
            }
        });
        if (oldOrder.side === 'sell') {
            self.asks = newOrders;
        }
        else if (oldOrder.side === 'buy') {
            self.bids = newOrders;
        }
    };
    ;
    // TODO add startingCalcAmountBN parameter which was in the old getVwapFromTradeAmountBN function
    OrderBook.prototype.getVwap = function (side, requiredCalcAmountBN, amountCalculator) {
        var functionName = 'OrderBook.getVwap()', self = this;
        var index = 0, sumTradeAmountsBN = new BigNumber(0), sumTradeAmountTimesPriceBN = new BigNumber(0), sumCalcAmountsBN = new BigNumber(0);
        // if side parameter not asks or bids
        if (!(side === 'asks' || side === 'bids')) {
            var error = new VError('%s fisrt parameter side %s must be either "asks" or "bids"', functionName, side);
            logger.error(error.stack);
            return error;
        }
        else if (!_.isObject(amountCalculator) || !_.isFunction(amountCalculator.calcAmountBN)) {
            var error = new VError('%s third parameter amountCalculator %s must be passed and be an object with a calcAmountBN function', functionName, amountCalculator);
            logger.error(error.stack);
            return error;
        }
        logger.trace('%s about to get %s vwap for required amount %s using amount calculator', functionName, side, requiredCalcAmountBN.toString());
        var orders = self[side];
        // while sum of calculated amounts < required calculated amount
        while (sumCalcAmountsBN.lessThan(requiredCalcAmountBN)) {
            // if all the orders have been read then return an Error as the total order book of calculated amounts is < required calc amount
            if (index === orders.length) {
                var error = new VError('%s sum of calculated amounts %s of all %s %s in the %s[%s] order book is less than the required calculated amount %s.', functionName, sumCalcAmountsBN.toString(), index, side, self.exchangeName, self.market.symbol, requiredCalcAmountBN.toString());
                error.name = 'notEnoughOrders';
                logger.warn(error.stack);
                return error;
            }
            var oldSumTradeAmountsBN = sumTradeAmountsBN, oldSumCalcAmountsBN = sumCalcAmountsBN;
            // calculate the amount for the current order
            var orderCalcAmountBN = amountCalculator.calcAmountBN(orders[index], side, self);
            sumCalcAmountsBN = oldSumCalcAmountsBN.
                plus(orderCalcAmountBN);
            logger.debug('%s sum calc amounts %s = old sum calc amounts %s + current order calc amount %s', functionName, sumCalcAmountsBN.toString(), oldSumCalcAmountsBN.toString(), orderCalcAmountBN.toString());
            // if sum calculated amounts > required calculated amount
            if (sumCalcAmountsBN.greaterThan(requiredCalcAmountBN)) {
                // calculate the last trade amount so the sum of the trade amounts equals the required calculated amounts
                var lastTradeAmountBN = requiredCalcAmountBN.
                    minus(oldSumCalcAmountsBN).
                    div(orderCalcAmountBN).
                    times(orders[index].amount).
                    round(self.exchange.currencyRounding[this.market.fixedCurrency]);
                logger.debug('%s last trade amount %s = (required calculated amount %s - sum calculated amounts before the current order %s) / current order calculated amount %s * current order size %s', functionName, lastTradeAmountBN.toString(), requiredCalcAmountBN.toString(), oldSumCalcAmountsBN.toString(), orderCalcAmountBN.toString(), orders[index].amount);
                // sum trade amounts = old sum trade amounts + current order trade amount
                sumTradeAmountsBN = sumTradeAmountsBN.
                    plus(lastTradeAmountBN);
                logger.debug('%s sum trade amounts %s = old sum trade amounts %s + last trade amount %s', functionName, sumTradeAmountsBN.toString(), oldSumTradeAmountsBN.toString(), lastTradeAmountBN.toString());
                var oldSumAmountTimesPriceBN = sumTradeAmountTimesPriceBN;
                // sum trade amount * price
                sumTradeAmountTimesPriceBN = sumTradeAmountTimesPriceBN.
                    plus(lastTradeAmountBN.
                    times(orders[index].price));
                logger.debug('%s sum of trade amounts * prices %s = old sum of trade amounts * price %s + (last trade amount %s * order price %s at %s index %s)', functionName, sumTradeAmountTimesPriceBN.toString(), oldSumAmountTimesPriceBN.toString(), lastTradeAmountBN.toString(), orders[index].price, side, index);
            }
            else {
                // sum trade amounts = old sum trade amounts + current trade amount
                sumTradeAmountsBN = sumTradeAmountsBN.
                    plus(orders[index].amount);
                logger.trace('%s sum trade amounts %s = old sum trade amounts %s + trade amount %s at %s index %s', functionName, sumTradeAmountsBN.toString(), oldSumTradeAmountsBN.toString(), orders[index].amount, side, index);
                var oldSumTradeAmountTimesPriceBN = sumTradeAmountTimesPriceBN;
                // sum of order quantity * price
                sumTradeAmountTimesPriceBN = sumTradeAmountTimesPriceBN.
                    plus(BigNumber(orders[index].amount).
                    times(orders[index].price));
                logger.trace('%s sum of trade amount * price %s = old sum of trade amount * price %s + (current order amount %s * current order price %s)', functionName, sumTradeAmountTimesPriceBN.toString(), oldSumTradeAmountTimesPriceBN, orders[index].amount, orders[index].price);
            }
            // increment the index for the next loop
            index++;
        }
        var vwapBN = sumTradeAmountTimesPriceBN.
            div(sumTradeAmountsBN).
            round(this.priceRounding, BigNumber.ROUND_DOWN);
        logger.debug('%s %s vwap %s = sum of (trade amount * price) %s / sum of trade amounts %s', functionName, side, vwapBN.toString(), sumTradeAmountTimesPriceBN.toString(), sumTradeAmountsBN.toString());
        var returnObject = {
            priceBN: vwapBN,
            tradeAmountBN: sumTradeAmountsBN.
                round(self.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN)
        };
        return returnObject;
    };
    // get best buy price under or equal to a max price
    // TODO change to return a BigNumber rather than a string. eg getBestBuyBN
    OrderBook.prototype.getBestBuy = function (maxPrice, minPrice) {
        var functionName = 'OrderBook.getBestBuy()', self = this;
        var maxBuyPriceBN, minBuyPriceBN, bestPrice;
        if (!maxPrice)
            maxBuyPriceBN = new BigNumber(Infinity);
        else
            maxBuyPriceBN = new BigNumber(maxPrice);
        if (!minPrice)
            minBuyPriceBN = new BigNumber(0);
        else
            minBuyPriceBN = new BigNumber(minPrice);
        // if there are asks and max buy price > inside ask
        if (this.asks[0] && !maxBuyPriceBN.equals(Infinity) && maxBuyPriceBN.greaterThanOrEqualTo(this.asks[0].price)) {
            return new BigNumber(this.asks[0].price).
                round(this.priceRounding, BigNumber.ROUND_DOWN).
                toString();
        }
        var myBids = _.where(this.market.orders, { state: 'pending', side: 'buy' });
        logger.debug('%s found %s pending buy trades in %s my trades', functionName, myBids.length, this.market.orders.length);
        var bestBidPrice;
        // calculate best bid price which is the first bid with price <= max buy price
        // and bid amount > min order amount after removing any of my order amounts
        // for each bid in the exchange order book
        for (var i = 0; i < this.bids.length; i++) {
            var exchangeBid = this.bids[i];
            // initialise bid amount
            var exchangeBidAmountLessMyOrders = exchangeBid.amount;
            logger.trace('%s index %s, price %s, amount %s', functionName, i, exchangeBid.price, exchangeBid.amount);
            myBids.forEach(function (myOrder) {
                // if public order price matches my order price
                if (exchangeBid.price.toString() == myOrder.price) {
                    var oldExchangeBidAmountLessMyOrders = exchangeBidAmountLessMyOrders;
                    exchangeBidAmountLessMyOrders -= Number(myOrder.amountRemaining);
                    logger.trace('%s for price %s, new exchange bid amount less my orders %s = old exchange bid amount less my orders %s - remaining amount %s', functionName, exchangeBid.price, exchangeBidAmountLessMyOrders, oldExchangeBidAmountLessMyOrders, myOrder.amountRemaining);
                }
            });
            // best price is the first bid with price <= max buy price
            // and bid amount > min order amount
            if (BigNumber(exchangeBid.price).lessThanOrEqualTo(maxBuyPriceBN) &&
                exchangeBidAmountLessMyOrders > self.minAmountToIgnore) {
                bestBidPrice = exchangeBid.price;
                logger.debug('%s best buy price %s at order index %s in the order book', functionName, exchangeBid.price, i);
                break;
            }
        }
        // if no best bids from the previous search
        if (!bestBidPrice) {
            if (minBuyPriceBN.greaterThan(0)) {
                bestPrice = minBuyPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = min buy price %s as there are no bids <= max buy price %s and the min buy price > 0', functionName, bestPrice, minBuyPriceBN.toString(), maxBuyPriceBN.toString());
            }
            else {
                bestPrice = maxBuyPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = max buy price %s as there are no bids <= max buy price %s and the min buy price = 0', functionName, bestPrice, maxBuyPriceBN.toString(), maxBuyPriceBN.toString());
            }
            return bestPrice;
        }
        // bestBid can't be undefined due to the previous check
        var bestBidPriceBN = new BigNumber(bestBidPrice);
        // if best bid price < min buy price
        if (bestBidPriceBN.lessThan(minBuyPriceBN)) {
            // make sure the new buy order doesn't cross with the inside ask
            if (minBuyPriceBN.greaterThan(this.asks[0].price)) {
                bestPrice = new BigNumber(this.asks[0].price).
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = inside ask as the best bid price %s < min buy price %s which is > inside ask %s', functionName, bestPrice, bestBidPrice, minPrice, this.asks[0].price);
            }
            else {
                bestPrice = minBuyPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = min buy price %s as the best bid price %s < min buy price %s which is < inside ask %s', functionName, bestPrice, minBuyPriceBN, bestBidPrice, minPrice, this.asks[0].price);
            }
        }
        else if (bestBidPriceBN.lessThan(maxBuyPriceBN)) {
            var priceJustInsideBestBidBN = bestBidPriceBN.plus(this.smallestPrice);
            // make sure the new bid doesn't cross with the inside ask
            if (priceJustInsideBestBidBN.lessThan(this.asks[0].price)) {
                bestPrice = priceJustInsideBestBidBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = best bid %s + smallest price %s which is < inside ask %s', functionName, bestPrice, bestBidPrice, this.smallestPrice, this.asks[0].price);
            }
            else {
                bestPrice = bestBidPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best buy price %s = inside bid %s as best bid %s + smallest price %s >= inside ask %s', functionName, bestPrice, bestBidPrice, this.asks[0].price);
            }
        }
        else {
            bestPrice = bestBidPriceBN.
                round(this.priceRounding, BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s best buy price %s = best bid price %s as best bid === max buy price %s', functionName, bestPrice, bestBidPrice, maxPrice);
        }
        return bestPrice;
    };
    // get best buy price under or equal to a max price
    OrderBook.prototype.getBestSell = function (minPrice, maxPrice) {
        var functionName = 'OrderBook.getBestSell()', self = this;
        var maxSellPriceBN, minSellPriceBN, bestPrice;
        if (!minPrice)
            minSellPriceBN = new BigNumber(0);
        else
            minSellPriceBN = new BigNumber(minPrice);
        if (!maxPrice)
            maxSellPriceBN = new BigNumber(Infinity);
        else
            maxSellPriceBN = new BigNumber(maxPrice);
        // if there are bids and max sell price > inside bid
        if (this.bids[0] && !minSellPriceBN.equals(0) && minSellPriceBN.lessThanOrEqualTo(this.bids[0].price)) {
            return new BigNumber(this.bids[0].price).
                round(this.priceRounding, BigNumber.ROUND_DOWN).
                toString();
        }
        // get my pending sell trades
        var myAsks = _.where(this.market.orders, { state: 'pending', side: 'sell' });
        logger.debug('%s found %s pending sell trades in %s my trades', functionName, myAsks.length, this.market.orders.length);
        var bestAskPrice;
        // for each ask in the exchange order book
        for (var i = 0; i < this.asks.length; i++) {
            var exchangeAsk = this.asks[i];
            // initialise bid amount
            var exchangeAskAmountLessMyOrders = exchangeAsk.amount;
            logger.trace('%s index %s, price %s, amount %s', functionName, i, exchangeAsk.price, exchangeAsk.amount);
            myAsks.forEach(function (myOrder) {
                // if public order price matches my order price
                if (exchangeAsk.price.toString() == myOrder.price) {
                    var oldExchangeAskAmountLessMyOrders = exchangeAskAmountLessMyOrders;
                    exchangeAskAmountLessMyOrders -= Number(myOrder.amountRemaining);
                    logger.trace('%s for price %s, new exchange ask amount less my orders %s = old exchange ask amount less my orders %s - remaining amount %s', functionName, exchangeAsk.price, exchangeAskAmountLessMyOrders, oldExchangeAskAmountLessMyOrders, myOrder.amountRemaining);
                }
            });
            // best price is the first ask with price >= min sell price
            // and ask amount > min order amount
            if (BigNumber(exchangeAsk.price).greaterThanOrEqualTo(minSellPriceBN) && exchangeAskAmountLessMyOrders > self.minAmountToIgnore) {
                bestAskPrice = exchangeAsk.price;
                logger.debug('%s best sell price %s at order index %s in the order book', functionName, exchangeAsk.price, i);
                break;
            }
        }
        // if no best asks from the previous search
        if (!bestAskPrice) {
            if (maxSellPriceBN.lessThan(Infinity)) {
                bestPrice = maxSellPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = max sell price %s as there are no asks >= min sell price %s and the max sell price < Infinity', functionName, bestPrice, maxSellPriceBN.toString(), minSellPriceBN.toString());
            }
            else {
                bestPrice = minSellPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = min sell price %s as there are no asks >= min sell price %s and the max sell price is Infinity', functionName, bestPrice, minSellPriceBN.toString(), minSellPriceBN.toString());
            }
            return bestPrice;
        }
        // bestAsk can't be undefined due to the previous check
        var bestAskPriceBN = new BigNumber(bestAskPrice);
        // if best ask price > max sell price
        if (bestAskPriceBN.greaterThan(maxSellPriceBN)) {
            // make sure the new sell order doesn't cross with the inside bid
            if (maxSellPriceBN.lessThan(this.bids[0].price)) {
                bestPrice = new BigNumber(this.bids[0].price).
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = inside bid as the best ask price %s > max sell price %s which is < inside bid %s', functionName, bestPrice, bestAskPrice, maxPrice, this.bids[0].price);
            }
            else {
                bestPrice = maxSellPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = max sell price %s as the best ask price %s > max sell price %s which is > inside bid %s', functionName, bestPrice, maxPrice, bestAskPrice, maxPrice, this.bids[0].price);
            }
        }
        else if (bestAskPriceBN.greaterThan(minSellPriceBN)) {
            var priceJustInsideBestAskBN = bestAskPriceBN.minus(this.smallestPrice);
            // make sure the new sell order doesn't cross with the inside bid
            if (priceJustInsideBestAskBN.greaterThan(this.bids[0].price)) {
                bestPrice = priceJustInsideBestAskBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = best ask %s - smallest price %s which is > inside bid %s', functionName, bestPrice, bestAskPrice, this.smallestPrice, this.bids[0].price);
            }
            else {
                bestPrice = bestAskPriceBN.
                    round(this.priceRounding, BigNumber.ROUND_DOWN).
                    toString();
                logger.debug('%s best sell price %s = best ask price %s which is <= inside bid %s', functionName, bestPrice, bestAskPrice, this.bids[0].price);
            }
        }
        else {
            bestPrice = bestAskPriceBN.
                round(this.priceRounding, BigNumber.ROUND_DOWN).
                toString();
            logger.debug('%s best sell price %s = best ask price %s as best ask === min sell price %s', functionName, bestPrice, bestAskPrice, minPrice);
        }
        return bestPrice;
    };
    OrderBook.prototype.calcBidAskForSpread = function (desiredSpreadPercentage) {
        var functionName = 'OrderBook.calcBidAskForSpread()', self = this;
        var found = false;
        var bidIndex = 0, askIndex = 0, bidSumBN, askSumBN, spreadPercentageBN = new BigNumber(0), lastIncrement = 'both';
        if (!desiredSpreadPercentage) {
            var errMsg = util_1.format('%s the desired spread percentage is undefined', functionName);
            logger.error(errMsg);
            throw new TypeError(errMsg);
        }
        //TODO remove my orders from the book
        if (self.bids.length === 0 && self.asks.length === 0) {
            var errMsg = util_1.format('%s there are no bids or asks in the order book', functionName);
            logger.error(errMsg);
            throw new TypeError(errMsg);
        }
        else if (self.bids.length === 0) {
            self.bids = [{
                    price: self.asks[0].price,
                    amount: Infinity
                }];
        }
        else if (self.asks.length === 0) {
            self.asks = [{
                    price: self.bids[0].price,
                    amount: Infinity
                }];
        }
        bidSumBN = new BigNumber(self.bids[0].amount);
        askSumBN = new BigNumber(self.asks[0].amount);
        //calc
        while (!found) {
            spreadPercentageBN = new BigNumber(self.asks[askIndex].price).
                minus(self.bids[bidIndex].price).
                div(self.bids[bidIndex].price).
                times(100);
            logger.debug('%s spreadPercentage = (%s - %s) / %s = %s%, bidIndex = %s, askIndex = %s', functionName, self.asks[askIndex].price, self.bids[bidIndex].price, self.bids[bidIndex].price, spreadPercentageBN.toString(), bidIndex, askIndex);
            logger.debug('%s while spread %s < %s desired spread. bidSum = %s, askSum = %s', functionName, spreadPercentageBN.toString(), desiredSpreadPercentage, askSumBN.toString(), bidSumBN.toString());
            if (spreadPercentageBN.lessThan(desiredSpreadPercentage)) {
                if (askSumBN.equals(bidSumBN)) {
                    logger.debug('%s askSumBN %s = bidSumBN %s', functionName, askSumBN.toString(), bidSumBN.toString());
                    if (askSumBN.equals(Infinity)) {
                        logger.debug('%s there are no more bids or asks', functionName);
                        found = true;
                    }
                    else {
                        logger.debug('%s increament both bid and ask', functionName);
                        incrementBid();
                        incrementAsk();
                        lastIncrement = 'both';
                    }
                }
                else if (askSumBN.lessThan(bidSumBN)) {
                    logger.debug('%s askSum %s < bidSum %s so increment the ask', functionName, askSumBN.toString(), bidSumBN.toString());
                    lastIncrement = 'ask';
                    incrementAsk();
                }
                else if (askSumBN.greaterThan(bidSumBN)) {
                    logger.debug('%s askSum %s > bidSum %s so increment the bid', functionName, askSumBN.toString(), bidSumBN.toString());
                    lastIncrement = 'bid';
                    incrementBid();
                }
            }
            else if (spreadPercentageBN.equals(desiredSpreadPercentage)) {
                logger.debug('%s spread matches exactly to the desired spread so exit while loop', functionName);
                found = true;
                lastIncrement = 'both';
            }
            else {
                logger.debug('%s spread > desired spread so exit while loop', functionName);
                found = true;
            }
        }
        logger.debug('%s lastIncrement = %s, bidIndex = %s, askIndex = %s', functionName, lastIncrement, bidIndex, askIndex);
        var spread;
        if (lastIncrement === 'both') {
            // need to find the midpoint then calculate the spread
            // midPoint = (bid + ask) / 2
            var midPoint = new BigNumber(self.bids[bidIndex].price).
                plus(self.asks[askIndex].price).
                div(2);
            logger.debug('%s midPoint = (bid %s + ask %s) / 2 = %s', functionName, self.bids[bidIndex].price.toString(), self.asks[askIndex].price.toString(), midPoint.toString());
            // bid = midPoint / (1 + desired spread / 200)
            var bid = midPoint.div(BigNumber(1).plus(BigNumber(desiredSpreadPercentage).div(200)));
            logger.debug('%s bid = midpoint %s / (1 + desired spread %s / 200 = %s = %s rounded)', functionName, midPoint.toString(), desiredSpreadPercentage, bid.toString(), bid.toFixed(this.priceRounding));
            // ask = bid * (desired spread / 100 + 1)
            var ask = bid.times(BigNumber(desiredSpreadPercentage).div(100).plus(1));
            logger.debug('%s ask = bid %s * (1 + desired spread %s / 100 = %s = %s rounded', functionName, bid.toString(), desiredSpreadPercentage, ask.toString(), ask.toFixed(this.priceRounding));
            spread = {
                bid: bid.plus(this.smallestPrice).toFixed(this.priceRounding),
                ask: ask.minus(this.smallestPrice).toFixed(this.priceRounding) };
        }
        else if (lastIncrement === 'bid') {
            // bid = ask / (1 + desired spread / 100)
            var bid = new BigNumber(self.asks[askIndex].price).
                div(BigNumber(1).
                plus(BigNumber(desiredSpreadPercentage).
                div(100)));
            logger.debug('%s bid = ask %s / (1 + desired spread %s / 100 = %s)', functionName, self.asks[askIndex].price, desiredSpreadPercentage, bid.toFixed(this.priceRounding));
            spread = {
                bid: bid.toFixed(this.priceRounding),
                ask: new BigNumber(self.asks[askIndex].price).minus(this.smallestPrice).toFixed(this.priceRounding) };
        }
        else if (lastIncrement === 'ask') {
            // ask = bid * (1 + desired spread / 100)
            var ask = new BigNumber(self.bids[bidIndex].price).times(BigNumber(1).plus(BigNumber(desiredSpreadPercentage).div(100)));
            logger.debug('%s ask = bid %s * (1 + desired spread %s / 100) = %s', functionName, self.bids[bidIndex].price, desiredSpreadPercentage, ask.toFixed(this.priceRounding));
            spread = {
                bid: new BigNumber(self.bids[bidIndex].price).plus(this.smallestPrice).toFixed(this.priceRounding),
                ask: ask.toFixed(this.priceRounding) };
        }
        logger.debug('%s return spread bid = %s, ask = %s', functionName, spread.bid, spread.ask);
        return spread;
        function incrementBid() {
            if (++bidIndex < self.bids.length) {
                bidSumBN = bidSumBN.plus(self.bids[bidIndex].amount);
                logger.debug('%s incremented bidIndex to %s, bidSumBN = %s', functionName, bidIndex, bidSumBN.toString());
            }
            else {
                logger.warn('%s no more bids so make bidSumBN = Infinity', functionName);
                self.bids.push({
                    price: self.bids[bidIndex - 1].price,
                    amount: Infinity
                });
                bidSumBN = new BigNumber(Infinity);
                lastIncrement = 'ask';
            }
        }
        function incrementAsk() {
            if (++askIndex < self.asks.length) {
                askSumBN = askSumBN.plus(self.asks[askIndex].amount);
                logger.debug('%s incremented askIndex to %s, askSumBN = %s', functionName, askIndex, askSumBN.toString());
            }
            else {
                logger.warn('%s no more asks so make askSumBN = Infinity', functionName);
                self.asks.push({
                    price: self.asks[askIndex - 1].price,
                    amount: Infinity
                });
                askSumBN = new BigNumber(Infinity);
                lastIncrement = 'bid';
            }
        }
    };
    ;
    /**
     * Generic static listener for new depth
     */
    OrderBook.newOrderListener = function (market, callback) {
        var functionName = 'OrderBook.newOrderListener()';
        market.on('newOrderBook', function (orderBook) {
            logger.debug('%s newOrderBook event fired with %d bids and %d asks for the %s market on the %s exchange. ' +
                'Best bid %d %d and ask %d %d', functionName, orderBook.bids.length, orderBook.asks.length, market.symbol, market.exchangeName, orderBook.bids[0].price, orderBook.bids[0].amount, orderBook.asks[0].price, orderBook.asks[0].amount); // price and size of best bid and ask
            // call callback if exists and is a function
            if (callback && typeof (callback) == 'function')
                callback(orderBook);
        });
    };
    /**
     * Static function to log the latest order book
     */
    OrderBook.log = function (orderBook) {
        // Get the previous order book from the market in the exchange
        var previousOrderBook = exchange_1.default.exchanges[orderBook.exchangeName].markets[orderBook.symbol].previousOrderBook;
        // validate there is a previous order book before calculating differences
        if (!previousOrderBook) {
            logger.warn('Other order book for market %s not found. This should only happen once.');
            console.log('%s[%s] %d %d %d %d %s', orderBook.exchangeName, orderBook.symbol, orderBook.bids[0].price, orderBook.bids[0].amount, orderBook.asks[0].price, orderBook.asks[0].amount, moment(orderBook.timestamp).format('hh:mm:ss'));
            return;
        }
        // get rounding from the exchange configuration
        var rounding = exchange_1.default.exchanges[orderBook.exchangeName].currencyRounding[orderBook.market.fixedCurrency];
        var bidChange = new BigNumber(orderBook.bids[0].price).minus(previousOrderBook.bids[0].price), askChange = new BigNumber(orderBook.asks[0].price).minus(previousOrderBook.asks[0].price), bidChangePerc = new BigNumber(bidChange).div(previousOrderBook.bids[0].price).times(100), askChangePerc = new BigNumber(askChange).div(previousOrderBook.asks[0].price).times(100), bidSizeChange = new BigNumber(orderBook.bids[0].amount).minus(previousOrderBook.bids[0].amount), askSizeChange = new BigNumber(orderBook.asks[0].amount).minus(previousOrderBook.asks[0].amount), market = orderBook.exchangeName + '[' + orderBook.symbol + '] ', bidSize = orderBook.bids[0].amount + ' ', askSize = orderBook.asks[0].amount + ' ', spread = new BigNumber(orderBook.asks[0].price).minus(orderBook.bids[0].price).div(orderBook.bids[0].price).times(100).toFixed(3) + '% ';
        var bid = orderBook.bids[0].price + ' ' + bidChangePerc.toFixed(3) + '% ', ask = orderBook.asks[0].price + ' ' + askChangePerc.toFixed(3) + '% ';
        if (bidChange.greaterThan(0))
            bid = "up";
        else if (bidChange.lessThan(0))
            bid = "down";
        if (askChange.greaterThan(0))
            ask = "up";
        else if (askChange.lessThan(0))
            ask = "down";
        if (!bidChange.equals(0) || !bidSizeChange.equals(0) ||
            !askChange.equals(0) || !askSizeChange.equals(0)) {
            console.log(market +
                bid +
                bidSize +
                ask +
                askSize +
                spread +
                moment(orderBook.timestamp).format('hh:mm:ss'));
        }
    };
    // Static function to starts listeners for the specified exchanges and markets
    // if array of markets not passed then listen for all markets on the exchange
    OrderBook.startLogListeners = function (arbExchanges, symbols) {
        // For each exchange name in the exchanges array
        arbExchanges.forEach(function (exchangeName) {
            if (_.isUndefined(symbols)) {
                symbols = Object.keys(exchange_1.default.exchanges[exchangeName].markets);
            }
            // For each market in the symbols
            symbols.forEach(function (symbol) {
                // create a listener for the newOrderBook event
                OrderBook.newOrderListener(exchange_1.default.exchanges[exchangeName].markets[symbol], function (orderBook) {
                    // Log to console the details of the new order book
                    OrderBook.log(orderBook);
                });
            });
        });
    };
    return OrderBook;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = OrderBook;
