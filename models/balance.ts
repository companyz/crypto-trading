/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import * as _ from 'underscore';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import Exchange from "./exchange";

export interface IBalanceSettings {
    exchangeName: string;
    currency: string;
    totalBalance?: string | number | BigNumber;
    availableBalance?: string | number | BigNumber;
}

export default class Balance
{
    exchangeName: string;
    currency: string;

    totalBalanceBN: BigNumber = BigNumber(0);
    availableBalanceBN: BigNumber = BigNumber(0);

    constructor (settings: IBalanceSettings)
    {
        this.exchangeName = settings.exchangeName;
        this.currency = settings.currency;

        this.totalBalanceBN = (settings.totalBalance ? new BigNumber(settings.totalBalance) : new BigNumber(0) );
        this.availableBalanceBN = (settings.availableBalance ? new BigNumber(settings.availableBalance) : new BigNumber(0) );
    }

    get totalBalance(): string { return this.totalBalanceBN.toString(); }
    set totalBalance(totalBalance) { this.totalBalanceBN = new BigNumber(totalBalance); }

    get availableBalance(): string { return this.availableBalanceBN.toString(); }
    set availableBalance(availableBalance) { this.availableBalanceBN = new BigNumber(availableBalance); }

    get exchange(): Exchange { return Exchange.exchanges[this.exchangeName]; }

    get clone(): Balance {
        return new Balance({
            exchangeName:   _.clone(this.exchangeName),
            currency:       _.clone(this.currency),
            totalBalance:       this.totalBalance,
            availableBalance:   this.availableBalance
        });
    }

    log(): string
    {
        const msg = format('%s total %s, available %s',
            this.currency, this.totalBalance, this.availableBalance);
        return msg;
    }
}
