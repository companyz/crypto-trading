/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />

import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import Exchange from "../exchange";
import {default as Market, ICommissionMatrix} from "../market";
import {IAmountCalculator, IOrderBookOrder} from "../orderBook";

// My packages
var logger = require('config-logger');

/**
 * Used by OrderBook.getVwap()
 */
export default class SellAmountCalculator implements IAmountCalculator
{
    exchange: Exchange;
    marketCommissions: ICommissionMatrix;

    constructor(public market: Market, commissionType?: string)
    {
        this.exchange = Exchange.exchanges[market.exchangeName];

        this.marketCommissions = market.getCommissionMatrix(commissionType);
    }

    /**
     * calculate the sell currency amount of an order in an order book.
     * @param order object with rate and amount properties of type number
     * @param side  asks or bids
     */
    calcAmountBN(order: IOrderBookOrder, side: string): BigNumber
    {
        const functionName = 'SellAmountCalculator.calcAmountBN()';

        let sellAmountsBN: BigNumber;

        if (side === 'asks')
        {
            // asks is for a market buy trade so the sell currency is variable
            sellAmountsBN = new BigNumber(order.amount).
            times(order.price).
            times(this.marketCommissions.sell[this.market.variableCurrency].
                plus(1)
            ).
            round(this.exchange.currencyRounding[this.market.variableCurrency], BigNumber.ROUND_DOWN);

            logger.trace('%s sell amounts %s = order amount %s * order rate %s * (variable sell commission %s + 1)', functionName,
                sellAmountsBN.toString(),
                order.amount, order.price,
                this.marketCommissions.sell[this.market.variableCurrency].toString());
        }
        else if (side === 'bids')
        {
            // bids is a market sell trade so the sell currency is fixed
            sellAmountsBN = new BigNumber(order.amount).
            times(this.marketCommissions.sell[this.market.fixedCurrency].
                plus(1)
            ).
            round(this.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN);

            logger.trace('%s sell amounts %s = order amount %s * (fixed sell commission %s + 1)', functionName,
                sellAmountsBN.toString(),
                order.amount,
                this.marketCommissions.sell[this.market.fixedCurrency].toString() );
        }

        return sellAmountsBN;
    }
}

