/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
var BigNumber = require('bignumber.js');
var exchange_1 = require("../exchange");
// My packages
var logger = require('config-logger');
/**
 * Used by OrderBook.getVwap()
 */
var SellAmountCalculator = (function () {
    function SellAmountCalculator(market, commissionType) {
        this.market = market;
        this.exchange = exchange_1.default.exchanges[market.exchangeName];
        this.marketCommissions = market.getCommissionMatrix(commissionType);
    }
    /**
     * calculate the sell currency amount of an order in an order book.
     * @param order object with rate and amount properties of type number
     * @param side  asks or bids
     */
    SellAmountCalculator.prototype.calcAmountBN = function (order, side) {
        var functionName = 'SellAmountCalculator.calcAmountBN()';
        var sellAmountsBN;
        if (side === 'asks') {
            // asks is for a market buy trade so the sell currency is variable
            sellAmountsBN = new BigNumber(order.amount).
                times(order.price).
                times(this.marketCommissions.sell[this.market.variableCurrency].
                plus(1)).
                round(this.exchange.currencyRounding[this.market.variableCurrency], BigNumber.ROUND_DOWN);
            logger.trace('%s sell amounts %s = order amount %s * order rate %s * (variable sell commission %s + 1)', functionName, sellAmountsBN.toString(), order.amount, order.price, this.marketCommissions.sell[this.market.variableCurrency].toString());
        }
        else if (side === 'bids') {
            // bids is a market sell trade so the sell currency is fixed
            sellAmountsBN = new BigNumber(order.amount).
                times(this.marketCommissions.sell[this.market.fixedCurrency].
                plus(1)).
                round(this.exchange.currencyRounding[this.market.fixedCurrency], BigNumber.ROUND_DOWN);
            logger.trace('%s sell amounts %s = order amount %s * (fixed sell commission %s + 1)', functionName, sellAmountsBN.toString(), order.amount, this.marketCommissions.sell[this.market.fixedCurrency].toString());
        }
        return sellAmountsBN;
    };
    return SellAmountCalculator;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SellAmountCalculator;
