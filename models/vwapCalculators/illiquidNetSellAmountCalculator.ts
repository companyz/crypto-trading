/// <reference path="../../typings/node/node-0.10.d.ts" />
/// <reference path="../../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../../typings/underscore/underscore.d.ts" />

import * as _ from 'underscore';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import Exchange from "../exchange";
import Market from "../market";
import Order from "../order";
import OrderBook from "../orderBook";
import {IAmountCalculator, IOrderBookOrder} from "../orderBook";

// My packages
const logger = require('config-logger'),
    VError = require('verror');

export default class BuyAmountCalculator implements IAmountCalculator
{
    liquid2IlliquidRateBN = new BigNumber(1);

    constructor(public marginBN: BigNumber, liquid2IlliquidRateBN?: BigNumber)
    {
        if (liquid2IlliquidRateBN)
        {
            this.liquid2IlliquidRateBN = liquid2IlliquidRateBN;
        }
    }

    /**
     * static function to calculate the sell amount in getVwapFromCustomAmount
     * @param order an array with two items: rate and amount
     * @param side  asks or bids
     * @param orderBook  instance of the orderBook calling the calcAmountBN function
     */
    calcAmountBN(order: IOrderBookOrder, side: string, orderBook: OrderBook): BigNumber
    {
        const functionName = 'IlliquidNetSellAmountCalculator.calcAmountBN()';

        if (!(side === 'asks' || side === 'bids'))
        {
            const error = new VError('%s second parameter side %s must be either "asks" or "bids"', functionName, side);
            logger.error(error.stack);
            return error;
        }

        const liquidSide = (side === 'asks' ? 'buy' : 'sell'),
            illiquidSide = (side === 'asks' ? 'sell' : 'buy');

        // calculate net sell amount of liquid order
        const liquidOrder = new Order({
            exchangeName: orderBook.exchangeName,
            symbol: orderBook.symbol,
            side: liquidSide,
            type: 'market',
            price: order.price.toString(),
            amount: order.amount.toString()
        });

        // illiquid sell amount is variable currency
        if (illiquidSide === 'buy')
        {
            // illiquid net variable amount = liquid variable amount * (1 + marginBN)
            const illiquidNetSellAmountBN = liquidOrder.variableAmountBN.
            times(this.liquid2IlliquidRateBN).
            times(this.marginBN.
                plus(1)
            );

            logger.trace('%s illiquid net sell amount %s = liquid net variable amount * liquid to illiquid exchange rate %s * (1 + margin %s)', functionName,
                illiquidNetSellAmountBN.toString(),
                liquidOrder.variableAmountBN.toString(),
                this.liquid2IlliquidRateBN.toString(),
                this.marginBN.toString() );

            // return as sell amount of illiquid order
            return illiquidNetSellAmountBN;
        }
        // illiquid sell amount is fixed currency
        else if (illiquidSide === 'sell')
        {
            logger.debug('%s illiquid net sell amount %s = liquid net fixed amount', functionName,
                liquidOrder.fixedAmountBN.toString() );

            // illiquid net fixed amount = liquid net fixed amount
            return liquidOrder.fixedAmountBN;
        }
    }
}

