/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
var exchange_1 = require("./exchange");
var logger = require('config-logger'), VError = require('verror');
(function (MovementType) {
    MovementType[MovementType["withdrawal"] = 1] = "withdrawal";
    MovementType[MovementType["deposit"] = 2] = "deposit";
})(exports.MovementType || (exports.MovementType = {}));
var MovementType = exports.MovementType;
var Movement = (function () {
    function Movement(settings) {
        this.exchangeName = settings.exchangeName;
        this.currency = settings.currency;
        this.type = Movement.parse(settings.type);
        this.timestamp = settings.timestamp;
        if (settings.amount) {
            this.amountBN = new BigNumber(settings.amount);
        }
        else {
            var error = new VError('amount in constructor settings is not defined');
            logger.error(error.stack);
        }
    }
    Object.defineProperty(Movement.prototype, "amount", {
        get: function () { return this.amountBN.toString(); },
        set: function (amount) { this.amountBN = new BigNumber(amount); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Movement.prototype, "exchange", {
        get: function () { return exchange_1.default.exchanges[this.exchangeName]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Movement.prototype, "clone", {
        get: function () {
            return new Movement({
                exchangeName: _.clone(this.exchangeName),
                currency: _.clone(this.currency),
                amount: this.amount,
                type: Movement.typeToString(this.type),
                timestamp: _.clone(this.timestamp)
            });
        },
        enumerable: true,
        configurable: true
    });
    Movement.prototype.log = function () {
        return util_1.format('%s %s %s', Movement.typeToString(this.type), this.amount, this.currency);
    };
    Movement.typeToString = function (movementType) {
        if (movementType == MovementType.deposit) {
            return "deposit";
        }
        else if (movementType == MovementType.withdrawal) {
            return "withdrawal";
        }
    };
    Movement.parse = function (movement) {
        if (movement == 'deposit') {
            return MovementType.deposit;
        }
        else if (movement == 'withdrawal') {
            return MovementType.withdrawal;
        }
        else {
            return;
        }
    };
    return Movement;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Movement;
