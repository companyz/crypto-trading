/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />
var _ = require('underscore');
var util_1 = require('util');
var BigNumber = require('bignumber.js');
var exchange_1 = require("./exchange");
var Balance = (function () {
    function Balance(settings) {
        this.totalBalanceBN = BigNumber(0);
        this.availableBalanceBN = BigNumber(0);
        this.exchangeName = settings.exchangeName;
        this.currency = settings.currency;
        this.totalBalanceBN = (settings.totalBalance ? new BigNumber(settings.totalBalance) : new BigNumber(0));
        this.availableBalanceBN = (settings.availableBalance ? new BigNumber(settings.availableBalance) : new BigNumber(0));
    }
    Object.defineProperty(Balance.prototype, "totalBalance", {
        get: function () { return this.totalBalanceBN.toString(); },
        set: function (totalBalance) { this.totalBalanceBN = new BigNumber(totalBalance); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Balance.prototype, "availableBalance", {
        get: function () { return this.availableBalanceBN.toString(); },
        set: function (availableBalance) { this.availableBalanceBN = new BigNumber(availableBalance); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Balance.prototype, "exchange", {
        get: function () { return exchange_1.default.exchanges[this.exchangeName]; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Balance.prototype, "clone", {
        get: function () {
            return new Balance({
                exchangeName: _.clone(this.exchangeName),
                currency: _.clone(this.currency),
                totalBalance: this.totalBalance,
                availableBalance: this.availableBalance
            });
        },
        enumerable: true,
        configurable: true
    });
    Balance.prototype.log = function () {
        var msg = util_1.format('%s total %s, available %s', this.currency, this.totalBalance, this.availableBalance);
        return msg;
    };
    return Balance;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Balance;
