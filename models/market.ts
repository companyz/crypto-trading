/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import * as _ from 'underscore';
import {EventEmitter} from 'events';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import Exchange from "./exchange";
import Order from "./order";
import Ticker from "./ticker";
import OrderBook from "./orderBook";

const logger = require('config-logger'),
    VError = require('verror');

export interface ICommissionMatrix {
    sell:  { [key: string] : BigNumber; },
    buy: { [key: string] : BigNumber; }
}

class Market extends EventEmitter
{
    exchangeName: string;
    symbol: string;

    // Set helper properties
    fixedCurrency: string;
    variableCurrency: string;

    // ask and bids for this market. Also called market depth
    latestOrderBook: OrderBook;
    previousOrderBook: OrderBook;

    tickers=[];
    latestTicker: Ticker;
    previousTicker: Ticker;

    orders: Order[] = [];

    constructor(settings: {exchangeName: string, symbol: string})
    {
        super();

        this.exchangeName = settings.exchangeName;
        this.symbol = settings.symbol;

        // Set helper properties
        this.fixedCurrency = Market.getFixedCurrency(this.symbol);
        this.variableCurrency = Market.getVariableCurrency(this.symbol);
    }

    static getFixedCurrency(symbol: string): string
    {
        return symbol.slice(0,3);
    }

    static getVariableCurrency(symbol: string): string
    {
        return symbol.slice(3);
    }

    // Create reference to the exchange which is not saved into the database
    get exchange(): Exchange
    {
        const functionName = 'Market.exchange()',
            returnedExchange: Exchange = Exchange.exchanges[this.exchangeName];

        if (!returnedExchange)
        {
            const error = new VError('%s exchange %s can not be found in the static Exchange.exchanges', functionName,
                this.exchangeName);

            logger.debug(error);
            throw error;
        }

        return returnedExchange;
    };

    // returns a matrix that can be used to multiple the trade amount to get the buy and sell fees
    getCommissionMatrix(commissionType: string): ICommissionMatrix
    {
        const functionName = 'Market.getCommissionMatrix()';
        let exchangeCommissionType;

        if ( commissionType != 'maker' && commissionType != 'taker' )
        {
            const error = new VError('%s first parameter commissionType %s needs to be either "maker" or "taker"', functionName, commissionType);
            logger.error(error.stack);
            return error;
        }

        const commissionMatrix: ICommissionMatrix = {
            sell: {},
            buy: {}
        };

        if (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'variableCurrency')
        {
            // negative as fee is on the buy currency
            commissionMatrix.sell[this.fixedCurrency] = new BigNumber(0);
            commissionMatrix.buy[this.variableCurrency] = new BigNumber(this.exchange.commissions[this.variableCurrency][commissionType]).
            negated();
        }
        else if (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'fixedCurrency')
        {
            // positive as the fee is on the sell currency
            commissionMatrix.sell[this.fixedCurrency] = new BigNumber(this.exchange.commissions[this.fixedCurrency][commissionType]);
            commissionMatrix.buy[this.variableCurrency] = new BigNumber(0);
        }

        if (this.exchange.feeStructure === 'buyCurrency' || this.exchange.feeStructure === 'fixedCurrency')
        {
            // negative as the fee is on the buy currency
            commissionMatrix.buy[this.fixedCurrency] = new BigNumber(this.exchange.commissions[this.fixedCurrency][commissionType]).
            negated();
            commissionMatrix.sell[this.variableCurrency] = new BigNumber(0);
        }
        else if (this.exchange.feeStructure === 'sellCurrency' || this.exchange.feeStructure === 'variableCurrency')
        {
            // positive as the fee is on the sell currency
            commissionMatrix.buy[this.fixedCurrency] = new BigNumber(0);
            commissionMatrix.sell[this.variableCurrency] = new BigNumber(this.exchange.commissions[this.variableCurrency][commissionType]);
        }

        logger.trace('%s sell %s %s; %s %s; buy %s %s, %s %s', functionName,
            this.fixedCurrency, commissionMatrix.sell[this.fixedCurrency].toString(),
            this.variableCurrency, commissionMatrix.sell[this.variableCurrency].toString(),
            this.fixedCurrency, commissionMatrix.buy[this.fixedCurrency].toString(),
            this.variableCurrency, commissionMatrix.buy[this.variableCurrency].toString() );

        return commissionMatrix;
    }

    /**
     * Add's a new ticker to the market if it's different from the previous ticker
     * @Param {Ticker} new Ticker to be added to the market
     */
    addTicker(newTicker: Ticker, callback?: (Ticker) => void ): void
    {
        // If there has been a change in the bid, ask or last ticker
        if(!this.previousTicker ||
            this.previousTicker.bid != newTicker.bid ||
            this.previousTicker.ask != newTicker.ask ||
            this.previousTicker.last != newTicker.last )
        {
            this.tickers.push(newTicker);
            this.previousTicker = this.latestTicker;
            this.latestTicker = newTicker;

            this.emit('ticker', newTicker);
        }

        if (callback) callback(newTicker);
    };

    /**
     * Add's a new OrderBook to the market
     * @Param {OrderBook} newOrderBook to be added to the market
     */
    addOrderBook(newOrderBook: OrderBook, callback?: (OrderBook) => void ): void
    {
        this.previousOrderBook = this.latestOrderBook;
        this.latestOrderBook = newOrderBook;

        this.emit('newOrderBook', newOrderBook);

        if (callback) callback(newOrderBook);
    }

    /**
     * Add's a new order to a market
     * @Param {Order} newOrder new order to be added to the market
     */
    addOrder(newOrder: Order ): void
    {
        const functionName = 'Market.addOrder()';

        //if trade state is not pending
        if (newOrder.state !== 'pending')
        {
            const error = new VError('%s can not add %s order with id %s and rate %s as its state must be "pending" and not "%s"', functionName,
                newOrder.side, newOrder.exchangeId, newOrder.price, newOrder.state);
            logger.error(error.stack);
            throw error;
        }

        // see if newOrder is already in the orders list matching on the exchangeId
        const storedOrder: Order = _.findWhere(this.orders, {
            exchangeId: newOrder.exchangeId,
            state: 'pending'});

        // If trade is not already in the trades list then add
        if (_.isUndefined(storedOrder))
        {
            logger.debug('%s trade is not already in market.trades list so will add', functionName);

            this.orders.push(newOrder);

            // emit a new event to the listeners
            this.emit('order', newOrder);

        } else // if newTrade is already in the trades list
        {
            logger.debug('%s %s trade with id %s and rate %s already exists in market.trades. Will not add it again',
                functionName, newOrder.side, newOrder.exchangeId, newOrder.price);
        }
    }

    cancelOrder(cancelledOrder: Order, callback?: (Order) => void): void
    {
        const functionName = 'Market.cancelOrder()',
            self = this;

        //if trade state is not cancelled
        if (cancelledOrder.state !== 'cancelled')
        {
            const error = new VError('%s can not cancel %s trade with id %s and rate %s as its state must be "cancelled" and not "%s"', functionName,
                cancelledOrder.side, cancelledOrder.exchangeId, cancelledOrder.price, cancelledOrder.state);
            logger.error(error.stack);
            throw error;
        }

        logger.trace('%s about to replace a pending order with a cancelled trade for id %s, tag %s in list of %s market trades', functionName,
            cancelledOrder.exchangeId, cancelledOrder.tag, cancelledOrder.state, this.orders.length);

        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function(order: Order) {
            return order.exchangeId === cancelledOrder.exchangeId &&
                order.state === 'pending';
        });

        //emit a new filled event to the listeners
        this.emit('order', cancelledOrder);

        if (callback) callback(cancelledOrder);
    }

    fillOrder(filledOrder: Order, callback?: (Order) => void ): void
    {
        const functionName = 'Market.fillOrder()',
            self = this;

        //if trade state is not cancelled
        if (filledOrder.state !== 'filled')
        {
            const error = new VError('%s can not cancel %s trade with id %s and rate %s as its state must be "filled" and not "%s"', functionName,
                filledOrder.side, filledOrder.exchangeId, filledOrder.price, filledOrder.state);
            logger.error(error.stack);
            throw error;
        }

        logger.trace('%s about to replace a pending order with a filled order for id %s, tag %s in market trades list', functionName,
            filledOrder.exchangeId, filledOrder.tag, filledOrder.state);

        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function(order) {
            return order.exchangeId === filledOrder.exchangeId &&
                order.state === 'pending';
        });

        // add new filled order
        this.orders.push(filledOrder);

        //emit a new filled order event to the listeners
        this.emit('order', filledOrder);

        if (callback) callback(filledOrder);
    }

    partialFillOrder(partiallyFilledOrder: Order, callback?: (Order) => void ): void
    {
        const functionName = 'Market.partialFillOrder()',
            self = this;

        if (partiallyFilledOrder.state === 'filled')
        {
            this.fillOrder(partiallyFilledOrder, callback);
            return;
        }
        else if (partiallyFilledOrder.state !== 'partiallyFilled')
        {
            const error = new VError('%s can not partially fill %s trade with id %s and rate %s as its state must be "partiallyFilled" and not "%s"', functionName,
                partiallyFilledOrder.side, partiallyFilledOrder.exchangeId, partiallyFilledOrder.price, partiallyFilledOrder.state);
            logger.error(error.stack);
            throw error;
        }

        logger.trace('%s about to replace a pending order with a partially filled trade for id %s, tag %s in market trades list', functionName,
            partiallyFilledOrder.exchangeId, partiallyFilledOrder.tag, partiallyFilledOrder.state);

        // remove old pending order from market trades
        this.orders = _.reject(this.orders, function(order) {
            return order.exchangeId === partiallyFilledOrder.exchangeId &&
                order.state === 'pending';
        });

        // add new filled order
        this.orders.push(partiallyFilledOrder);

        // add remaining pending order to market trades list
        const remainingPartialOrder = partiallyFilledOrder.remainingPartial();

        // push the remaining pending order of the partially filled trade into the market maker trades
        self.orders.push( remainingPartialOrder );

        //emit a new filled event to the listeners
        this.emit('order', partiallyFilledOrder);

        if (callback) callback(partiallyFilledOrder);
    }
}

export default Market;