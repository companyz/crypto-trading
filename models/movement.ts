/// <reference path="../typings/node/node-0.10.d.ts" />
/// <reference path="../typings/bignumber.js/bignumber.js.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import * as _ from 'underscore';
import {format} from 'util';
import * as BigNumber from 'bignumber.js';
type BigNumber = bignumber.BigNumber;

import Exchange from "./exchange";

const logger = require('config-logger'),
    VError = require('verror');

export enum MovementType {
    withdrawal = 1,
    deposit
}

export interface IMovementSettings {
    exchangeName: string;
    currency: string;
    amount: string | number;
    type: string;
    timestamp: Date;
}

export default class Movement
{
    exchangeName: string;
    currency: string;
    amountBN: BigNumber;
    type: MovementType;
    timestamp: Date;

    constructor (settings: IMovementSettings)
    {
        this.exchangeName = settings.exchangeName;
        this.currency = settings.currency;

        this.type = Movement.parse(settings.type);

        this.timestamp = settings.timestamp;

        if (settings.amount) {
            this.amountBN = new BigNumber(settings.amount);
        }
        else {
            const error = new VError('amount in constructor settings is not defined');
            logger.error(error.stack);
        }
    }

    get amount(): string { return this.amountBN.toString(); }
    set amount(amount) { this.amountBN = new BigNumber(amount); }

    get exchange(): Exchange { return Exchange.exchanges[this.exchangeName]; }

    get clone(): Movement {
        return new Movement({
            exchangeName:   _.clone(this.exchangeName),
            currency:       _.clone(this.currency),
            amount:         this.amount,
            type:           Movement.typeToString(this.type),
            timestamp:      _.clone(this.timestamp)
        });
    }

    log(): string
    {
        return format('%s %s %s',
            Movement.typeToString(this.type), this.amount, this.currency);
    }

    static typeToString(movementType: MovementType): string
    {
        if (movementType == MovementType.deposit)
        {
            return "deposit";
        }
        else if (movementType == MovementType.withdrawal)
        {
            return "withdrawal";
        }
    }

    static parse(movement: string): MovementType
    {
        if (movement == 'deposit')
        {
            return MovementType.deposit
        }
        else if (movement == 'withdrawal')
        {
            return MovementType.withdrawal
        }
        else {
            return;
        }
    }
}
